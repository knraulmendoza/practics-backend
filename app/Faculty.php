<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = ['code','name'];

    public function careers(){
        return $this->hasMany(Career::class);
    }
}

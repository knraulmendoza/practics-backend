<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareersSubjects extends Model
{
    protected $fillable = ['carrer_id', 'subject_id'];
}

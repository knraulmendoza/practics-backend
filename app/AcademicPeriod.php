<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicPeriod extends Model
{
    protected $table = 'academic_periods';
    protected $fillable = ['year', 'period', 'start_date', 'end_date'];
}

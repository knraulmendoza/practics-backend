<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assistence extends Model
{
    protected $table = "assistence";
    protected $fillable = ['student_id','observation','date','academic_period_id', 'assist'];
}

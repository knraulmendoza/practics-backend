<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name','code'];

    public function career()
    {
        return $this->belongsToMany(Career::class, 'careers_subjects'); // una asignatura pertenece a un programa
    }

    public function modules(){
        return $this->belongsToMany(Module::class,'careers_subjects');
    }
}

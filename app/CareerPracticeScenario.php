<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPracticeScenario extends Model
{
    protected $table = 'career_practice_scenario';
    protected $fillable = ['career_id', 'practice_scenario_id'];

    public function scenarioAcademicPeriod()
    {
        return $this->hasOne(CareerPracticeScenarioAcademicPeriod::class);
    }
}

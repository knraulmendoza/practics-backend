<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = ['date', 'module_area_id', 'teacher_id', 'academic_period_id', 'career_id'];

    public function area_module()
    {
        return $this->belongsTo(PracticeScenarioAreaModule::class, 'module_area_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function periodAcademic()
    {
        return $this->belongsTo(AcademicPeriod::class, 'academic_period_id');
    }
    public function groupModule(int $rotationId)
    {
        return $this->belongsToMany(GroupModule::class, 'assign_student_groups')->where('assign_student_groups.rotation_id', '=', $rotationId);
    }
    public function rotations()
    {
        return $this->belongsToMany(Rotation::class, 'assign_student_groups');
    }

    public function rotation()
    {
        return $this->belongsToMany(Rotation::class, 'assign_student_groups')->whereRaw("? BETWEEN rotations.start_date AND rotations.end_date", [date('Y-m-d')]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientPoll extends Model
{
    protected $table = 'patients_polls';
    protected $fillable = ['date', 'attention_id','evaluator'];

    public function answers()
    {
        return $this->morphMany(Answer::class,'answerable');
    }
}

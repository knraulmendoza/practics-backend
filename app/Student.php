<?php

namespace App;

use App\Enums\StateAssistence;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "students";
    protected $fillable = ['person_id',];

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function groups(int $academicPeriodId)
    {
        return $this->belongsToMany(GroupModule::class, 'students_groups')->where('groups_modules.academic_period_id', $academicPeriodId);
    }

    public function studentsCountByModule(int $moduleId)
    {
        return Student::join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->where('groups_modules.module_id', $moduleId)->groupBy('students.person_id')->get();
    }

    public function studentsCountBySubject(int $subjectId)
    {
        return Student::join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->join('modules', 'groups_modules.module_id', 'modules.id')
            ->where('modules.subject_id', $subjectId)->groupBy('students.person_id')->get();
    }

    public function studentsCountByScenario(int $scenarioId)
    {
        return Student::join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
            ->join('assignments', 'asg.assignment_id', 'assignments.id')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
            ->where('psa.practice_scenarios_id', $scenarioId)->groupBy('students.person_id')->get();
    }

    public function stundetGroups($academicPeriodId)
    {
        return $this->hasOne(StudentGroup::class, 'student_id')->where('academic_period_id', $academicPeriodId);
    }

    public function assistences($academicPeriodId)
    {
        return $this->hasMany(Assistence::class, 'student_id')->where('academic_period_id', $academicPeriodId);
    }

    public function inassistences($academicPeriodId)
    {
        return $this->hasMany(Assistence::class, 'student_id')->where('academic_period_id', $academicPeriodId)->where('assist', '!=', StateAssistence::ASISTIO);
    }

    public function assistence($academicPeriodId)
    {
        return $this->hasMany(Assistence::class, 'student_id')->where('academic_period_id', $academicPeriodId)->where('date', date('Y-m-d'))->where('assist', StateAssistence::ASISTIO);
    }

    public function attentions($academicPeriodId)
    {
        return $this->hasMany(Attention::class, 'student_id')->where('academic_period_id', $academicPeriodId);
    }
}

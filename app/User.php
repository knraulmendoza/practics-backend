<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'password', 'user_parent', 'security_question_id', 'security_answer', 'state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class,'roles_users');
    }
    public function hasRole(String $roles)
    {
        $rolesArray = array_map('intval', explode(';', $roles));
        return $this->roles()->whereIn('id', $rolesArray)->count() > 0;
    }

    // public function hasRole(...$role)
    // {


    //     //  $roles = $roleSlug;
    //     //  $rolesArray = explode(';',$roles);  in_array($user->role, $roles
    //      $roles = in_array($this->roles()->roleId, $role);
    //      return $roles;

    // }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function person(){
        return $this->hasOne(Person::class);
    }
}

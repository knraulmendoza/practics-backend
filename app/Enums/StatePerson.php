<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatePerson extends Enum
{
    const ACTIVO = 1;
    const INACTIVO = 0;
}

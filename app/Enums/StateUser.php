<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class StateUser extends Enum
{
    const ACTIVO = 1;
    const INACTIVO = 2;
    const SINPERSON = 3;
    const ACTIVOSINPERSON = 4;
}

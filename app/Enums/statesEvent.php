<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatesEvent extends Enum
{
    const PENDIENTE = 1;
    const CANCELADO = 2;
    const REALIZADO = 3;
}
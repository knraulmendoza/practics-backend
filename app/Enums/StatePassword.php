<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatePassword extends Enum
{
    const CHANGE_PASSWORD = '0';
    const UPDATED_PASSWORD = '1';
}

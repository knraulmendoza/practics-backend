<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class StateAssistence extends Enum
{
    const ASISTIO = 1;
    const NO_ASISTIO = 2;
    const NO_ASISTIO_EXCUSA = 3;
    const COVID_NEG = 4;
    const COVID_POS = 5;
}

<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class TypeRotation extends Enum
{
    const FULL = 1;
    const DOBLE = 2;
    const TRIPLE = 3;
}
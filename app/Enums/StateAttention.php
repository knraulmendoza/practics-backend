<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class StateAttention extends Enum
{
    const AWAIT = 0;
    const ACCEPTED = 1;
    const EVALUATED = 2;
}

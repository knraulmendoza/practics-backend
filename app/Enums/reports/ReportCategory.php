<?php
namespace App\Enums;

use BenSampo\Enum\Enum;

final class Roles extends Enum
{
    const ADMIN = 1;
    const COORDINATOR = 2;
    const TEACHER_PRACTICT = 3;
    const TEACHER = 4;
    const STUDENT = 5;
}
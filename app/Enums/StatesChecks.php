<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatesChecks extends Enum
{
    const CREADA = 1;
    const REALIZADA = 2;
    const CANCELADA = 3;
    const CHEQUEADO = 4;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswersChecks extends Model
{

    protected $fillable = ['id','answer','check_id', 'question_id'];
    public function observation()
    {
        return $this->morphOne(Observation::class,'observationable');
    }

}

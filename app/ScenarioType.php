<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScenarioType extends Model
{
    protected $table = 'scenarios_types';
    protected $fillable = ['name','description'];

    public function practiceScenario(){
        return $this->hasMany(PracticeScenario::class,'scene_type_id');
    }
}

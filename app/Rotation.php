<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rotation extends Model
{
    protected $fillable=['id', 'start_date', 'end_date', 'academic_period_id', 'type', 'career_id'];

    public function groups()
    {
        return $this->hasOne(AssignStudentGroups::class, 'rotation_id');
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'assign_student_groups');
    }

}

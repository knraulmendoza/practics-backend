<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PracticeScenarioArea extends Model
{
    protected $table = 'practice_scenarios_areas';
    protected $fillable = ['practice_scenarios_id', 'area_id'];

    public function scenario()
    {
        return $this->belongsTo(PracticeScenario::class, 'practice_scenarios_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function areas()
    {
        return $this->hasOne(Area::class, 'area_id');
    }
    public function scenarios_area_module()
    {
        return $this->belongsTo(PracticeScenarioAreaModule::class, 'practice_scenarios_areas_id');
    }
}

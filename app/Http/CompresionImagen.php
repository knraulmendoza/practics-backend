<?php
namespace App\Http;
use Illuminate\Support\Facades\File;

class CompresionImagen
{
    public function saveImage($image, $ubicacion)
    {
        $tempDirectory = 'images/temporal/';
        $array = explode('.', $image->getClientOriginalName());
        $uid = uniqid();
        $image->move($tempDirectory, $array[0].$uid.'.'.end($array));
        $filePath = $tempDirectory.$array[0].$uid.'.'.end($array);
        $uid = uniqid();
        $reducedFilename= $array[0].$uid.'.'.end($array);
        $fileReducedPath = 'images/'.$ubicacion.'/'.$reducedFilename;
        $this->reduceImaSizeWithResmush($filePath, $fileReducedPath);
        File::delete($filePath);
        return $reducedFilename;
    }

    public function reduceImaSizeWithResmush($file,$fileDest){
        $mime = mime_content_type($file);

        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new \CURLFile($file, $mime, $name);
        $data = array(
            "files" => $output,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.resmush.it/?qlty=80');
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
           $result = curl_error($ch);
        }
        curl_close ($ch);

        $result = json_decode($result);


        file_put_contents( $fileDest,file_get_contents($result->dest));

    }
}

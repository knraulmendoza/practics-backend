<?php

namespace App\Http\Middleware;

use App\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Closure;

class CheckRole
{

    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next, $role)
    // {
        // if (auth()->check() && auth()->user()->hasRole($role)) {
        //     return $next($request);
        // }

        // return redirect('/');
    // }
    public function handle($request, Closure $next, $role)
    {
        try {
            //Access token from the request
            $token = JWTAuth::parseToken();
            //Try authenticating user
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired
            return $this->response->unauthorized('Your token has expired. Please, login again.');
        } catch (TokenExpiredException $e) {
            //Thrown if token invalid
            return $this->response->unauthorized('Your token is invalid. Please, login again.');
        }catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->response->unauthorized('Please, attach a Bearer Token to your request');
        }
        //If user was authenticated successfully and user is in one of the acceptable roles, send to next request.
        // in_array($user->role, $roles
        if ($user && $user->hasRole($role)) {
            //  return $user->hasRole($role);
           return $next($request);
        }
        return $this->response->unauthorized();
    }
}

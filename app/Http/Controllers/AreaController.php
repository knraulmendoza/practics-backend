<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Response;
use App\Module;
use App\PracticeScenario;
use App\PracticeScenarioArea;
use App\PracticeScenarioAreaModule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group  Areas
 *
 * APIs para areas
 */
class AreaController extends Controller
{
    public $response;

    public function __construct()
    {
        $this->response = new Response();
        // $this->middleware('auth.role:1;2',['only'=>['index', 'store']]);
    }
    /**
     * Lista paginada de áreas
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "areas": [
     *              {
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              },
     *              {
     *                  "name": "Área 2",
     *                  "description": "Descripción área 2"
     *              }
     *          ],
     *          "total": 2,
     *          "per_page": 8
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */
    public function index()
    {
        try {
            $per_page = 8;
            $areas = Area::paginate(8);
            $total = $areas->total();
            $areas = $areas->items();
            return $this->response->ok(compact('areas', 'total', 'per_page'), count($areas) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Búsqueda de áreas por nombre
     *
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam area String required Área a buscar
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "areas": [
     *              {
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              },
     *              {
     *                  "name": "Área 2",
     *                  "description": "Descripción área 2"
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 201 {
     *      "state": false,
     *      "message": "ok",
     *      "data": { "areas": []}
     * }
     */
    public function search(Request $request)
    {
        $area = $request->input('area');
        $query = "MATCH(name) AGAINST('$area' IN BOOLEAN MODE)";
        $areas = Area::whereRaw($query)->get();
        return $this->response->ok(compact('areas'), count($areas) == 0 ? false : true);
    }

    /**
     * Guardar un área en un escenario de práctica
     *
     * 
     * @bodyParam name String required Nombre del área
     * @bodyParam description String optional Descripción del área
     * @bodyParam practiceScenario int required Id escenario de práctica
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "area": {
     *                  "id": 3
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function saveOneArea(Request $request)
    {
        try {
            $area = new Area($request->input('area'));
            if ($area->save()) {
                $practicScenarioArea = new PracticeScenarioArea();
                $practicScenarioArea->practice_scenarios_id = $request->input('practiceScenario');
                $practicScenarioArea->area_id = $area->id;
                $practicScenarioArea->save();
                $practicScenarioAreaModule = new PracticeScenarioAreaModule();
                $practicScenarioAreaModule->practice_scenarios_areas_id = $practicScenarioArea->id;
                $practicScenarioAreaModule->module_id = $request->input('moduleId');
                $practicScenarioAreaModule->save();
                return $this->response->ok(compact('area'));
            }
            return $this->response->badRequest();
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * Guardar area
     *
     * 
     * @bodyParam name String required Nombre del área
     * @bodyParam description String optional Descripción del área
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "area": {
     *                  "id": 3,
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $area = new Area($request->all());
            $area->save();
            DB::commit();
            return $this->response->ok(compact('area'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Asignar area en módulos
     *
     * 
     * @bodyParam scenarioId String required Nombre del área
     * @bodyParam area String required Descripción del área
     * @bodyParam modulesId Array required Id de los módulos que se van a asignar
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "practicScenarioArea": [{
     *                  "id": 3,
     *                  "practice_scenarios_areas_id": 4,
     *                  "module_id": 6
     *              }]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function areaInModules(Request $request)
    {
        try {
            DB::beginTransaction();
            $areaInModules = [];
            $practicScenarioArea = PracticeScenarioArea::where('practice_scenarios_id', $request->input('scenarioId'))->where('area_id', $request->input('area'))->first();
            foreach ($request->input('modulesId') as $module) {
                $practicScenarioAreaModule = new PracticeScenarioAreaModule();
                $practicScenarioAreaModule->practice_scenarios_areas_id = $practicScenarioArea->id;
                $practicScenarioAreaModule->module_id = $module;
                $practicScenarioAreaModule->save();
                array_push($areaInModules, $practicScenarioAreaModule);
            }
            DB::commit();
            return $this->response->ok(compact('practicScenarioArea'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Asignar area en escenarios de prácticas
     *
     * 
     * @bodyParam areas Array<int> required Ids de las áreas a asignar
     * @bodyParam scenarioId Array required Id del escenario al cual se va a asociar al área
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "areas": [2, 3],
     *          "practice_scenario": {"id": 2, "name": "Escenario de práctica", "description": "Descripción del escenario de práctica"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function areaInScenario(Request $request)
    {
        try {
            DB::beginTransaction();
            $areas = $request->input('areas');
            foreach ($areas as $area) {
                $practice_scenario = PracticeScenario::find($request->input('scenarioId'));
                if ($practice_scenario && $practice_scenario->state == 0) {
                    $practice_scenario->state = '1';
                }
                $practicScenarioArea = new PracticeScenarioArea();
                $practicScenarioArea->practice_scenarios_id = $practice_scenario->id;
                $practicScenarioArea->area_id = $area;
                $practicScenarioArea->save();
                $practice_scenario->save();
            }
            DB::commit();
            return $this->response->ok(compact('areas', 'practice_scenario'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Áreas sin escenarios de prácticas
     *
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "areas": [
     *              {
     *                  "id": 2,
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              },
     *              {
     *                  "id": 3,
     *                  "name": "Área 2",
     *                  "description": "Descripción área 2"
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function areasWithoutScenario(int $scenarioId)
    {
        try {
            $areas = DB::table('areas')->whereRaw('areas.id NOT IN (SELECT psa.area_id from practice_scenarios_areas as psa WHERE psa.practice_scenarios_id = ?)', [$scenarioId])->get();
            return $this->response->ok(compact('areas'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Modulos por area
     *
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "modules": [
     *              {
     *                  "id": 2,
     *                  "name": "módulo 1",
     *                  "description": "descripción módulo",
     *                  "career_id": 2,
     *                  "subject_id": 4
     *              },
     *              {
     *                  "id": 3,
     *                  "name": "módulo 2",
     *                  "description": "descripción módulo",
     *                  "career_id": 2,
     *                  "subject_id": 5
     *              }
     *          ]
     *      }
     * }
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function modules(int $areaId)
    {
        $modules = Module::join('practice_scenarios_areas_modules as psam', 'modules.id', 'psam.module_id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
            ->join('areas', 'psa.area_id', 'areas.id')->where('areas.id', $areaId)->groupBy('modules.id')->get();
        return $modules;
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * Actualizar área
     *
     * 
     * @bodyParam name String required Nombre del área
     * @bodyParam description String optional Descripción del área
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "area": {
     *                  "id": 3,
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "No se encontro el área",
     *      "data": null
     * }
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $area = Area::find($id);
            if ($area) {
                $area->update($request->all());
            } else {
                return $this->response->badRequest('No se encontro el área');
            }
            DB::commit();
            return $this->response->ok(compact('area'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Eliminar un área
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "area": {
     *                  "id": 3,
     *                  "name": "Área 1",
     *                  "description": "Descripción área 1"
     *              }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function destroy($id)
    {
        $area = Area::find($id);
        if ($area) {
            $practice_scenario = PracticeScenarioArea::where('area_id', $id)->get()->toArray();
            if (count($practice_scenario) > 0) {
                return $this->response->badRequest('No se puede eliminar el área, está asignada a escenarios de práctica');
            } else {
                try {
                    DB::beginTransaction();
                    $area = Area::destroy($id);
                    DB::commit();
                    return $this->response->ok(compact('area'));
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->response->badRequest($e->getMessage());
                }
            }
        } else {
            return $this->response->badRequest('No se encontro el área de práctica');
        }
    }
}

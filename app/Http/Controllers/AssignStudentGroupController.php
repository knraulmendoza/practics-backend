<?php

namespace App\Http\Controllers;

use App\AssignStudentGroup;
use App\GroupModule;
use App\Http\Response;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group  Asignaciones estudiante a grupos
 *
 * endpoints para asignaciones
 */
class AssignStudentGroupController extends Controller
{

    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Guardar Asignación
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam assignStudentsGroupsArray Array required Lista de grupos a asignar
     * @bodyParam assignStudentsGroupsArray.*.assignStudentsGroups Array required Lista de grupos
     * @bodyParam assignStudentsGroupsArray.*.assignStudentsGroups.*.group_module_id String required Id del grupo del módulo
     * @bodyParam assignStudentsGroupsArray.*.assignStudentsGroups.*.rotation_id int required Id de la rotación
     * @bodyParam assignStudentsGroupsArray.*.assignStudentsGroups.*.assignment_id int required Id de la asignación
     * @bodyParam assignStudentsGroupsArray.*.assignStudentsGroups.*.working_day String required Jornada (mañana, tarde)
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": true
     * }
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request->assignStudentsGroupsArray as $item) {
                foreach ($item['assignStudentsGroups'] as $assignStudentsGroup) {
                    $group = GroupModule::where('code', $assignStudentsGroup['group_module_id'])->first();
                    $assignStudent = new AssignStudentGroup($assignStudentsGroup);
                    $assignStudent->group_module_id = $group->id;
                    $assignStudent->save();
                }
            }
            DB::commit();
            return $this->response->ok(compact('request'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\AssignStudentGroup;
use App\Career;
use App\Enums\StateAttention;
use App\Http\Response;
use App\Module;
use App\Rotation;
use App\Student;
use App\Teacher;
use App\Person;
use App\User;
use App\RolesUsers;
use App\PeopleCareers;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use stdClass;
use App\Enums\StateUser;
use App\Enums\StatePassword;
use App\Enums\Roles;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group Estudiantes
 */
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $response;
    public $table = 'student';
    public $domine = '@unicesar.edu.co';

    public function __construct()
    {
        $this->middleware('auth.role:1;2;3', ['only' => ['index', 'store']]);
        $this->response = new Response();
    }

    public function getAttentions(Request $request, int $studentId)
    {
        $career_id = $request->header('career');
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        return Student::find($studentId)->attentions($academicPeriod->academic_period_id)->get();
    }

    /**
     * Registrar estudiantes masivamente
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"students": [{}]}
     * }
     */
    public function studentsSave(Request $request)
    {
        try {
            $career_id = $request->header('career');
            $students = $request->input("students");
            $user = '';
            $person = null;
            $student = '';
            if (!$userAux = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            DB::beginTransaction();
            foreach ($students as $student) {
                $user_dominio = explode('@', $student["student"]["person"]["address"])[0];
                $user =  $student["student"]["person"]["address"];
                $exist = User::firstOrCreate([
                    'user' => $user 
                ], [
                    'password'          => Hash::make($user_dominio),
                    'user_parent'       => $userAux->id,
                    'state'             => StateUser::SINPERSON,
                    'state_password'    => StatePassword::CHANGE_PASSWORD
                ]);
                if ($exist) {
                    $role_user = RolesUsers::firstOrCreate([ 
                        'user_id' => $exist->id,
                        'role_id' => Roles::STUDENT
                    ], [
                        'user_id' => $exist->id,
                        'role_id' => Roles::STUDENT
                    ]);
                }
                $id_user = $exist->id;
                if (!$exist->state || $exist->state == StateUser::SINPERSON) {
                    $person = Person::firstOrCreate([
                        'document' => $student["student"]["person"]["document"]
                    ], [
                        'document_type'     => $student["student"]["person"]["document_type"],
                        'first_name'        => $student["student"]["person"]["first_name"],
                        'second_name'       => $student["student"]["person"]["second_name"],
                        'first_last_name'   => $student["student"]["person"]["first_last_name"],
                        'second_last_name'  => $student["student"]["person"]["second_last_name"],
                        'address'           => $student["student"]["person"]["address"],
                        'gender'            => $student["student"]["person"]["gender"],
                        'user_id'           => $id_user
                    ]);
                    User::find($id_user)->update(['state' => 1]);
                }
                if ($person && $person->id) {
                    Student::firstOrCreate([
                        'person_id' => $person->id
                    ],[
                        'person_id' => $person->id 
                    ]); 
                    PeopleCareers::firstOrCreate([
                        'person_id' => $person->id,
                        'career_id' => $career_id
                    ],[
                        'person_id' => $person->id,
                        'career_id' => $career_id
                    ]);
                }
            }
            DB::commit();
            return $this->response->ok(compact('user', 'exist', 'person'));
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->response->badRequest('Error de sistema consulte con el administrador ' . $ex->getMessage());
        }
    }

    /**
     * Listar estudiantes por escenario de práctica
     * 
     * El estado pude ser falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"students": [{"id": "int", "person_id": "int", "attentions": "int"}]}
     * }
     */
    public function studentsByScenario(Request $request, int $scenrioId, int $subjectId = null)
    {
        $career_id = $request->header('career');
        try {
            $rotation = (new RotationController())->getRotation($career_id);
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if ($academicPeriod == null) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $students = Student::select('students.*')
                ->join('students_groups', 'students.id', 'students_groups.student_id')
                ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
                ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
                ->join('assignments', 'asg.assignment_id', 'assignments.id')
                ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
                ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
                ->where('psa.practice_scenarios_id', $scenrioId)->where('asg.rotation_id', $rotation->id)->with('person')->get();
            if ($subjectId) {
                $students = Student::select('students.*')
                    ->join('students_groups', 'students.id', 'students_groups.student_id')
                    ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
                    ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
                    ->join('modules', 'groups_modules.module_id', 'modules.id')
                    ->join('assignments', 'asg.assignment_id', 'assignments.id')
                    ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
                    ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
                    ->where('modules.subject_id', $subjectId)
                    ->where('psa.practice_scenarios_id', $scenrioId)->where('asg.rotation_id', $rotation->id)->with('person')->get();
            }
            $students;
            foreach ($students as &$student) {
                $student->attentions = $student->attentions($academicPeriod->academic_period_id)->count();
            }
            return $this->response->ok(compact('students'), count($students) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Listar estudiantes por encuesta de pacientes
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"groups": [{"id": "int", "code": "Sring", "module_id": "int", "students": "Array<Student>"}], "module": "Object<Module>"}
     * }
     * 
     */
    public function studentsByPatientPoll(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $module = Module::find($request->input('module'));
            $groups = $module->groups($request->input('academic_period'))->with('students')->get();
            foreach ($groups as &$group) {
                foreach ($group->students as &$student) {
                    $student->load('person');
                    $attentionsAux = $student->attentions($request->input('academic_period'))->get();
                    $student->attentionsAll = count($attentionsAux);
                    $student->attentionsAccepted = count(array_filter($attentionsAux->toArray(), function ($value) { return in_array($value['state'], [strval(StateAttention::EVALUATED), strval(StateAttention::ACCEPTED)]); }));
                    $student->attentionsEvaluated = count(array_filter($attentionsAux->toArray(), function ($value) { return $value['state'] == strval(StateAttention::EVALUATED); }));
                    $student->person->fullName = $student->person->first_name . ' ' .
                        ($student->person->second_name ? $student->person->second_name . ' ' : '') . $student->person->first_last_name . ' ' . $student->person->second_last_name;
                }
            }
            return $this->response->ok(compact('groups', 'module'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Obtener asignación por periodo academico
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"assignmentClass": {"module": "String", "teacher": "String", "working_day": "String", "rotation": "String", "career": "String", "scenario": "String", "area": "String", "inassistences": "Array<date, observation, scenario>", "attentions": []}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function getAssignment($id, $academicPeriod, Request $request)
    {
        try {
            $career = intval($request->header('career'));
            $student = Student::find($id);
            $group = $student->groups($academicPeriod)->first();
            if ($group) {
                $assignStudentGroup = $group->assignStudentGroup()->first();
                $assignment = $group->assignment()->first();
                if ($assignStudentGroup) {
                    $assignmentClass = new stdClass();
                    $assignmentClass->module = Module::find($group->module_id)->name;
                    $teacherPerson = Teacher::find($assignment->teacher_id)->person()->first();
                    $assignmentClass->teacher = $teacherPerson->first_name . ' ' . $teacherPerson->first_last_name . ' ' . $teacherPerson->second_last_name;
                    $assignmentClass->working_day = $assignStudentGroup->working_day;
                    $rotationAux = $group->rotation()->first();
                    $assignmentClass->rotation = $rotationAux->start_date . ' - ' . $rotationAux->end_date;
                    $assignmentClass->career = Career::find($career)->name;
                    $aux = $assignment->area_module()->first()->scenario_area()->first();
                    $assignmentClass->scenario = $aux->scenario()->first()->name;
                    $assignmentClass->area = $aux->area()->first()->name;
                    $assignmentClass->inassistences = $student->inassistences($academicPeriod)->get(['date', 'observation']);
                    $assignmentClass->attentions = count($student->attentions($academicPeriod)->get());
                    foreach ($assignmentClass->inassistences as &$inassistence) {
                        $rotation = Rotation::whereRaw("? BETWEEN rotations.start_date AND rotations.end_date", [$inassistence->date])->first();
                        $assignStudentGroupAux = AssignStudentGroup::where('rotation_id', $rotation->id)->where('group_module_id', $group->id)->first();
                        $assignmentAux = $assignStudentGroupAux->assignment()->first();
                        $inassistence->scenario =  $assignmentAux->area_module()->first()->scenario_area()->first()->scenario()->first()->name;
                    }

                    return $this->response->ok(compact('assignmentClass'));
                }
            }
            return $this->response->badRequest();
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar estudiante
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = '';
        try {
            DB::beginTransaction();
            $estudiante = Student::find($id);
            $estudiante->update($request->all());
            DB::commit();
            return $this->response->ok(compact($estudiante));
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Eliminar estudiante
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mensaje = '';
        try {
            DB::beginTransaction();
            $student = Student::delete($id);
            DB::commit();
            return $this->response->ok(compact($student));
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

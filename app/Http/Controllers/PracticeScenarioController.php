<?php

namespace App\Http\Controllers;

use App\Area;
use App\CareerPracticeScenario;
use App\CareerPracticeScenarioAcademicPeriod;
use App\Http\Response;
use App\PracticeScenario;
use App\PracticeScenarioArea;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Escenario de práctica
 * 
 */
class PracticeScenarioController extends Controller
{
    public $response;
    public $areas = 'areas';

    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2', ['only' => ['store']]);
    }
    /**
     * Listar escenarios de prácticas
     *
     * 
     * El estado retorna falso si no hay datos
     * 
     * @return \Illuminate\Http\Response
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "scenarios": [{"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int", "areas": []}]}
     * }
     * 
     */
    public function index(Request $request)
    {
        $career_id = intval($request->header('career'));
        $scenarios = PracticeScenario::select('practice_scenarios.*')
            ->join('career_practice_scenario as cps', 'practice_scenarios.id', 'cps.practice_scenario_id')
            ->where('cps.career_id', $career_id)
            ->where('practice_scenarios.state', '1')->get();
        foreach ($scenarios as &$scenario) {
            $scenario->areas = $scenario->areas();
        }
        return $this->response->ok(compact('scenarios'), $scenarios->isEmpty() ? false : true);
    }

    /**
     * Listar escenarios de prácticas paginado
     *
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "scenarios": [{"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int", "areas": []}],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     */
    public function getAllPaginate(Request $request)
    {
        $career_id = $request->header('career');
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (!$academicPeriod) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $career_id = intval($request->header('career'));
        $per_page = 8;
        $scenarios = PracticeScenario::select('practice_scenarios.*', 'cps.id as cpsId')
            ->join('career_practice_scenario as cps', 'practice_scenarios.id', 'cps.practice_scenario_id')
            ->where('cps.career_id', $career_id)->paginate($per_page);
        $total = $scenarios->total();
        $scenarios = $scenarios->items();
        foreach ($scenarios as &$scenario) {
            $scenario->areas = $scenario->areas();
            $careerScenario = CareerPracticeScenarioAcademicPeriod::where('career_practice_scenario_id', $scenario->cpsId)
                ->where('academic_period_id', $academicPeriod->academic_period_id)->first();
            if ($careerScenario) {
                $scenario->quotas = $careerScenario->quotas;
            }
        }
        return $this->response->ok(compact('scenarios', 'total', 'per_page'), count($scenarios) == 0 ? false : true);
    }

    /**
     * Listar escenarios de prácticas por módulo
     * 
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "scenarios": [{"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int", "areas": []}]}
     * }
     * 
     */
    public function scenariosByModule(Request $request, int $moduleId)
    {
        $career_id = intval($request->header('career'));
        $scenarios = (new PracticeScenario())->scenarios($career_id, $moduleId);
        foreach ($scenarios as &$scenario) {
            $scenario->areas = $scenario->areas();
        }
        return $this->response->ok(compact('scenarios'), $scenarios->isEmpty() ? false : true);
    }

    /**
     * Listar escenarios de prácticas tipo de escenario
     * 
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "scenarios": [{"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int", "areas": []}]}
     * }
     */
    public function scenariosByTypeScenario(Request $request, int $typeScenario)
    {
        $career_id = intval($request->header('career'));
        $scenarios = (new PracticeScenario)->scenariosByType($career_id, $typeScenario);
        return $this->response->ok(compact('scenarios'), $scenarios->isEmpty() ? false : true);
    }

    /**
     * guardar escenario en carrera
     * 
     * 
     * @bodyParam scenarioId int required Id del escenario de práctica
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "careerPracticeScenario": {"id": "int", "career_id": "int", "practice_scenario_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function scenarioInCareer(Request $request)
    {
        try {
            $career = intval($request->header('career'));
            $careerPracticeScenario = new CareerPracticeScenario();
            $careerPracticeScenario->career_id = $career;
            $careerPracticeScenario->practice_scenario_id = $request->input('scenarioId');
            $careerPracticeScenario->save();
            return $this->response->ok(compact('careerPracticeScenario'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Buscar escenario de prácticas por nombre
     * 
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "scenarios": [{"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int"}]}
     * }
     * 
     */
    public function search(Request $request, String $scenario)
    {
        $career = intval($request->header('career'));
        $scenario = explode(' ', $scenario);
        $name = '';
        foreach ($scenario as $nameAux) {
            $name = $name . '+' . $nameAux . '-';
        }
        $name = str_replace('-', ' ', $name);
        $query = "MATCH(`name`) AGAINST('$name' IN BOOLEAN MODE)";
        $scenarios = PracticeScenario::whereRaw($query)->get();
        return $this->response->ok(compact('scenarios'), count($scenarios) == 0 ? false : true);
    }

    /**
     * Registrar escenario de práctica
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre del escenario de práctica
     * @bodyParam description String Descripción del escenario de práctica
     * @bodyParam complexity_level int required complejidad escenario de práctica
     * @bodyParam scene_type int required Tipo de escenario de práctica
     * @bodyParam entity int required tipo de entidad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "practiceScenario": {"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int"} }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function store(Request $request)
    {
        try {
            $career = intval($request->header('career'));
            if ($career) {
                DB::beginTransaction();
                $practiceScenario = new PracticeScenario($request->all());
                $practiceScenario->name = strtoupper($request->input('name'));
                if ($practiceScenario->save()) {
                    $careerPracticeScenario = new CareerPracticeScenario();
                    $careerPracticeScenario->career_id = $career;
                    $careerPracticeScenario->practice_scenario_id = $practiceScenario->id;
                    $careerPracticeScenario->save();
                }
                // $text = '';
                // if($practiceScenario){
                //     if($request->has($this->areas)){
                //         for ($i=0; $i < count($request->input($this->areas)); $i++) {
                //             $area = new Area($request->input($this->areas)[$i]);
                //             if ($area->save()) {
                //                 $practicScenarioArea = new PracticeScenarioArea();
                //                 $practicScenarioArea->practice_scenarios_id = $practiceScenario->id;
                //                 $practicScenarioArea->area_id = $area->id;
                //                 $practicScenarioArea->save();
                //             }
                //         }
                //         DB::commit();
                //         return $this->response->ok('Escenario de práctica creado correctamente','Practice_Scenario',$practiceScenario);
                //     }else $text = 'No puede existir un escenario de practicas sin areas';
                // }else $text = 'Hubo un error al registrar el escenario de practica';
                // DB::rollback();
                // return $this->response->badRequest($text);
                // // return $this->response->badRequest($text);
                DB::commit();
                return $this->response->ok(compact('practiceScenario'));
            } else {
                return $this->response->badRequest('Se debe asignar una carrera');
            }
            DB::commit();
            return $this->response->ok(compact('practiceScenario'));
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Listar areas por escenario de práctica y módulo
     * 
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "areas": [{"id": "int", "name": "String", "description": "String"}] }
     * }
     * 
     */
    public function getAreasByModule(int $scenarioId, int $moduleId)
    {
        $areas = Area::select('areas.*')->join('practice_scenarios_areas as psa', 'areas.id', 'psa.area_id')
            ->join('practice_scenarios_areas_modules as psam', 'psa.id', 'psam.practice_scenarios_areas_id')
            ->where('psa.practice_scenarios_id', $scenarioId)
            ->where('psam.module_id', $moduleId)->get();
        return $this->response->ok(compact('areas'), count($areas) == 0 ? false : true);
    }

    public function assignQuotas(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $practiceScenario = CareerPracticeScenario::where('practice_scenario_id', $request->input('practicScenarioId'))->where('career_id', 1)->first();
            $psc_academicPeriod = $practiceScenario->scenarioAcademicPeriod()->first();
            if ($psc_academicPeriod) {
                CareerPracticeScenarioAcademicPeriod::where('career_practice_scenario_id', $psc_academicPeriod->career_practice_scenario_id)
                    ->where('academic_period_id', $academicPeriod->academic_period_id)->update(['quotas' => $request->input('quotas')]);
            } else {
                $save = new CareerPracticeScenarioAcademicPeriod();
                $save->academic_period_id = $academicPeriod->academic_period_id;
                $save->career_practice_scenario_id = $practiceScenario->id;
                $save->quotas = $request->input('quotas');
                $save->save();
            }
            return $this->response->ok(compact('practiceScenario'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar areas por escenario de práctica y módulo
     * 
     * El estado retorna falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "areas": [{"id": "int", "name": "String", "description": "String"}] }
     * }
     * 
     */
    public function getAreas(int $scenarioId)
    {
        $areas = Area::select('areas.*')->join('practice_scenarios_areas as psa', 'areas.id', 'psa.area_id')
            ->where('psa.practice_scenarios_id', $scenarioId)->get();
        return $this->response->ok(compact('areas'), count($areas) == 0 ? false : true);
    }

    /**
     * Actualizar escenario de práctica
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PracticeScenario  $practiceScenario
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre del escenario de práctica
     * @bodyParam description String Descripción del escenario de práctica
     * @bodyParam complexity_level int required complejidad escenario de práctica
     * @bodyParam scene_type int required Tipo de escenario de práctica
     * @bodyParam entity int required tipo de entidad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "practiceScenario": {"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int"} }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function update(Request $request, int $id)
    {
        try {
            DB::beginTransaction();
            $practiceScenario = PracticeScenario::find($id);
            if ($practiceScenario) {
                $practiceScenario->update($request->all());
                if ($request->has($this->areas)) {
                    for ($i = 0; $i < count($request->input($this->areas)); $i++) {
                        $practicScenarioArea = new PracticeScenarioArea();
                        $practicScenarioArea->practice_scenarios_id = $practiceScenario->id;
                        $practicScenarioArea->area_id = $request->input($this->areas)[$i];
                        $practicScenarioArea->save();
                    }
                }
                DB::commit();
                return $this->response->ok(compact('practiceScenario'));
            }
            return $this->response->badRequest('No se encontro el escenario de prácticas');
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Eliminar escenario de práctica
     *
     * @param  \App\PracticeScenario  $practiceScenario
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "practiceScenario": {"id": "int", "name": "String", "description": "String", "complexity_level": "int", "scene_type": "int", "entity": "int"} }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function destroy($id)
    {
        $practiceScenario = PracticeScenario::find($id);
        if ($practiceScenario) {
            $areas = $practiceScenario->areas();
            if (count($areas) > 0) {
                return $this->response->badRequest('No se puede eliminar el escenario, este tiene áreas asignadas');
            } else {
                try {
                    DB::beginTransaction();
                    $careerPracticeScenario = CareerPracticeScenario::where('practice_scenario_id', $id)->get()->toArray();
                    $delCareer = CareerPracticeScenario::destroy($careerPracticeScenario);
                    $escenarioPractica = PracticeScenario::destroy($id);
                    DB::commit();
                    return $this->response->ok(compact('practiceScenario'));
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->response->badRequest($e->getMessage());
                }
            }
        } else {
            return $this->response->badRequest('No se encontro el escenario de práctica');
        }
    }
}

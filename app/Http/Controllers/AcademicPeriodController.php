<?php

namespace App\Http\Controllers;

use App\AcademicPeriod;
use App\AcademicPeriodCareer;
use App\Http\Response;
use App\Enums;
use App\Enums\StateAcademicPeriod;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group  Periodo academico
 *
 * APIs para periodo academico
 */
class AcademicPeriodController extends Controller
{

    public $response;

    public function __construct()
    {
        $this->response = new Response();
    }
    /**
     * Consultar Periodo academicos
     * 
     * @response {
     * "message": "ok",
     * "state": true,
     * "data": {
     * "academicPeriods": []
     * }
     * }
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academicPeriods = AcademicPeriod::orderByDesc('year')->orderBy('period')->get();
        return $this->response->ok(compact('academicPeriods'));
    }

    /**
     * Registrar Periodo Academico
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @authenticated
     * 
     * @bodyParam year string required año Example: 2021
     * @bodyParam period string required periodo Example: 2
     * @bodyParam start_date string required fecha inicial Example: 2021-08-20
     * @bodyParam end_date string required fecha final Example: 2021-12-20
     * @headerParam career number required identificador de la carrera Example: 1
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": {"academicPeriod": {
     *		"end_date": "2019-10-27",
     *		"period": "2",
     *		"start_date": "2019-06-01",
     *		"year": "2019",
     *		"updated_at": "2022-03-03T20:56:04.000000Z",
     *		"created_at": "2022-03-03T20:56:04.000000Z",
     *		"id": 18
     *	},
     *	"academicPeriodCareer": {
     *		"end_date": "2019-10-27",
     *		"start_date": "2019-06-01",
     *		"academic_period_id": 18,
     *		"career_id": 1,
     *		"state": 1,
     *		"updated_at": "2022-03-03T20:56:04.000000Z",
     *		"created_at": "2022-03-03T20:56:04.000000Z",
     *		"id": 1
     *	} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */
    public function store(Request $request)
    {
        try {
            $academicPeriod = AcademicPeriod::where('year', $request->input('year'))->where('period', $request->input('period'))->first();
            if ($academicPeriod) {
                return $this->response->badRequest('Ya existe un periodo académico con los datos enviados del servidor ' .  $request->input('year') . ' - ' . $request->input('period'));
            }
            DB::beginTransaction();
            $academicPeriod = new AcademicPeriod($request->all());
            $academicPeriodCareer = null;
            if ($academicPeriod->save()) {
                $career_id = intval($request->header('career'));
                if ($career_id) {
                    $academicPeriodCareer = new AcademicPeriodCareer($request->all());
                    $academicPeriodCareer->academic_period_id = $academicPeriod->id;
                    $academicPeriodCareer->career_id = $career_id;
                    $academicPeriodCareer->state = StateAcademicPeriod::ACTIVE;
                    $academicPeriodCareer->save();
                    AcademicPeriodCareer::where('id', '!=', $academicPeriodCareer->id)->where('state', '=', StateAcademicPeriod::ACTIVE)->update(['state' => StateAcademicPeriod::INACTIVE]);
                }
                DB::commit();
                return $this->response->ok(compact('academicPeriod', 'academicPeriodCareer'));
            }
            return $this->response->badRequest();
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar Periodo Academico
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @authenticated
     * 
     * @bodyParam year string required año Example: 2021
     * @bodyParam period string required periodo Example: 2
     * @bodyParam start_date string required fecha inicial Example: 2021-08-20
     * @bodyParam end_date string required fecha final Example: 2021-12-20
     * @headerParam career number required identificador de la carrera Example: 1
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": { "academicPeriodCareer": {
     *		"end_date": "2019-10-27",
     *		"start_date": "2019-06-01",
     *		"academic_period_id": 18,
     *		"career_id": 1,
     *		"state": 1,
     *		"updated_at": "2022-03-03T20:56:04.000000Z",
     *		"created_at": "2022-03-03T20:56:04.000000Z",
     *		"id": 1
     *	} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */

    public function saveAcademicPeriodCareer(Request $request)
    {
        try {
            $career_id = intval($request->header('career'));
            if ($career_id) {
                $academicPeriodCareer = new AcademicPeriodCareer($request->all());
                $academicPeriodCareer->academic_period_id = $request->input('academic_period_id');
                $academicPeriodCareer->career_id = $career_id;
                $academicPeriodCareer->save();
                AcademicPeriodCareer::where('id', '!=', $academicPeriodCareer->id)->where('career_id', $career_id)->where('state', '=', StateAcademicPeriod::ACTIVE)->update(['state' => StateAcademicPeriod::INACTIVE]);
                return $this->response->ok(compact('academicPeriodCareer'));
            }
            return $this->response->badRequest('Hace falta el id del programa');
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    public function getAcademicPeriod($career_id)
    {
        // $academicPeriod = AcademicPeriod::whereRaw("? BETWEEN academic_periods.start_date AND academic_periods.end_date", [date('Y-m-d')])->first();
        $academicPeriod = AcademicPeriodCareer::where('state', StateAcademicPeriod::ACTIVE)->where('career_id', $career_id)->first();
        return $academicPeriod;
    }

    public function getAcademicPeriodByDate(Request $request)
    {
        $career_id = intval($request->header('career'));
        $academicPeriod = AcademicPeriod::whereRaw("? BETWEEN academic_periods.start_date AND academic_periods.end_date", [date('Y-m-d')])->first();
        $academicPeriodCareer = null;
        if ($academicPeriod) {
            $academicPeriodCareer = AcademicPeriodCareer::where('academic_period_id', $academicPeriod->id)->where('career_id', $career_id)->first();
            return $this->response->ok(compact('academicPeriod', 'academicPeriodCareer'));
        }
        return $this->response->badRequest('No existe periodo acádemico para está fecha');
    }
    /**
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": {"academicPeriod": {"year": 2021, "period": 2, "start_date": "2021-08-20", "end_date": "2021-12-20"} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */
    public function getObjectAcademicPeriod(Request $request)
    {
        $career_id = intval($request->header('career'));
        $academicPeriodCareer = $this->getAcademicPeriod($career_id);
        $academicPeriod = null;
        if ($academicPeriodCareer) {
            $academicPeriod = AcademicPeriod::find($academicPeriodCareer->academic_period_id);
        }
        return $academicPeriod ? $this->response->ok(compact('academicPeriod', 'academicPeriodCareer')) : $this->response->badRequest('No hay periodo académico activo');
    }

    /**
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": {"academicPeriod": {"year": 2021, "period": 2, "start_date": "2021-08-20", "end_date": "2021-12-20"} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */
    public function activeAcademicPeriod(Request $request)
    {
        try {
            $career_id = intval($request->header('career'));
            if ($career_id) {
                $academicPeriodCareer = AcademicPeriodCareer::where('academic_period_id', $request->input('academic_period_id'))->where('career_id', $career_id)->first();
                $academicPeriod = AcademicPeriod::find($request->input('academic_period_id'));
                DB::beginTransaction();
                if (!$academicPeriodCareer) {
                    $academicPeriodCareer = new AcademicPeriodCareer;
                    $academicPeriodCareer->start_date = $academicPeriod->start_date;
                    $academicPeriodCareer->end_date = $academicPeriod->end_date;
                    $academicPeriodCareer->academic_period_id = $request->input('academic_period_id');
                    $academicPeriodCareer->career_id = $career_id;
                    $academicPeriodCareer->save();
                }
                AcademicPeriodCareer::where('id', '!=', $academicPeriodCareer->id)->where('state', StateAcademicPeriod::ACTIVE)->where('career_id', $career_id)->update(['state' => StateAcademicPeriod::INACTIVE]);
                $academicPeriodCareer->state = StateAcademicPeriod::ACTIVE;
                $academicPeriodCareer->update();
                DB::commit();
                return $this->response->ok(compact('academicPeriodCareer'));
            } else {
                $this->response->badRequest('Debe envíar el código de la carrera');
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar Periodo Academico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @authenticated
     * @bodyParam year string required año Example: 2021
     * @bodyParam period string required periodo Example: 2
     * @bodyParam start_date string required fecha inicial Example: 2021-08-20
     * @bodyParam end_date string required fecha final Example: 2021-12-20
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": {"academicPeriod": {"year": 2021, "period": 2, "start_date": "2021-08-20", "end_date": "2021-12-20"} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $academicPeriod = AcademicPeriod::where('year', $request->input('year'))->where('period', $request->input('period'))->where('id', '!=', $id)->first();
            if ($academicPeriod) {
                return $this->response->badRequest('Ya existe un periodo académico con los datos enviados del servidor ' .  $request->input('year') . ' - ' . $request->input('period'));
            }
            $academicPeriod = AcademicPeriod::find($id);
            if ($academicPeriod) {
                $academicPeriod->update($request->all());
                DB::commit();
                return $this->response->ok(compact('academicPeriod'));
            }
            DB::commit();
            return $this->response->badRequest('No se encontro el periodo académico');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest('No se pudo actualizar el periodo acádemico');
        }
    }


    /**
     * Actualizar Periodo Academico de la carrera
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @authenticated
     * @bodyParam year string required año Example: 2021
     * @bodyParam period string required periodo Example: 2
     * @bodyParam start_date string required fecha inicial Example: 2021-08-20
     * @bodyParam end_date string required fecha final Example: 2021-12-20
     * 
     * @response {
     * "status" : true,
     * "message" : "ok",
     *  "data": {"academicPeriod": {"year": 2021, "period": 2, "start_date": "2021-08-20", "end_date": "2021-12-20"} }
     * }
     * 
     * @response 404 {
     *  "state": false,
     *  "message": "error",
     *  "data": null
     * }
     */
    public function updateAcademicPeriodCareer(Request $request, $period)
    {
        try {
            $career_id = intval($request->header('career'));
            if (!$career_id) {
                return $this->response->badRequest('La sesión no tiene asignado un programa académico');
            }
            DB::beginTransaction();
            $academicPeriodCareer = AcademicPeriodCareer::where('academic_period_id', $period)->where('career_id', $career_id)->first();
            if ($academicPeriodCareer) {
                $academicPeriodCareer->update($request->all());
                $id = $academicPeriodCareer->id;
                if ($academicPeriodCareer['state'] == StateAcademicPeriod::ACTIVE) {
                    AcademicPeriodCareer::where('id', '!=', $id)->where('career_id', $career_id)->where('state', StateAcademicPeriod::ACTIVE)->update(['state' => StateAcademicPeriod::INACTIVE]);
                }
                DB::commit();
                return $this->response->ok(compact('academicPeriodCareer'));
            }
            DB::commit();
            return $this->response->badRequest('No se encontro el periodo académico para este programa académico');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

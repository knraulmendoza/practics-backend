<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidatePatientRequest;
use App\Http\Response;
use App\Patient;
use App\Attention;
use App\Person;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Enums\StateAttention;
/**
 * @group Paciente 
 *
 */
class PatientController extends Controller
{

    public $table = 'patient';
    public $response;

    public function __construct()
    {
        $this->response = new Response;
    }
    /**
     * Listar pacientes
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "patients": [{"id": "int", "document_type": "int", "document": "String", "name": "String"}] }
     * }
     * 
     */
    public function index()
    {
        $patients = Patient::all();
        return $this->response->ok(compact('patients'));
    }

    public function register(ValidatePatientRequest $request)
    {
        try {
            DB::beginTransaction();
            $patient = Patient::firstOrCreate($request->all());
            DB::commit();
            return $patient;
        } catch (Exception $e) {
            DB::rollBack();
            return null;
        }
    }

    /**
     * Obtener paciente por id
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"patient": {"id": "int", "document_type": "int", "document": "String", "name": "String"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function show(int $id)
    {
        try {
            $patient = Patient::find($id);
            if ($patient) {
                return $this->response->ok(compact('patient'));
            }
            return $this->response->badRequest();
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Buscar paciente por documento de identidad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"patients": [{"id": "int", "document_type": "int", "document": "String", "name": "String", "student": "Object<Student> con Object<Person>"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function patientByDocument(String $document)
    {
        try {
            $patient = Patient::where('document', $document)->first();
            $existPatient = $patient != null;
            if ($patient) {
                $patient->attentions = Attention::where('patient_id', $patient->id)->where('state', strval(StateAttention::ACCEPTED))->with('procedures', 'student')->get();
                foreach ($patient->attentions as &$attention) {
                    $attention->student->load('person');
                }
            }
            return $this->response->ok(compact('patient'), $existPatient ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar datos del paciente
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam age int required Edad del paciente
     * @bodyParam gender int required Genero del paciente
     * @bodyParam origin int required Lugar de nacimiento
     * @bodyParam status int required Estado economico del paciente
     * @bodyParam academic_level int required Nivel academico
     * @bodyParam employment_situation int required situación laboral
     * @bodyParam phone int required Número de telefono
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"patients": {"id": "int", "document_type": "int", "document": "String", "name": "String",
     *                          "age": "int", "gender": "int", "origin": "int", "status": "int", "academic_level": "int", "employment_situation": "int", "phone": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function update(Request $request, int $id)
    {
        try {
            DB::beginTransaction();
            $patient = Patient::findOrFail($id);
            if ($patient) {
                $patient->age = $request->input('age');
                $patient->gender = $request->input('gender');
                $patient->origin = $request->input('origin');
                $patient->status = $request->input('status');
                $patient->academic_level = $request->input('academic_level');
                $patient->employment_situation = $request->input('employment_situation');
                $patient->phone = $request->input('phone');
                $patient->update($request->all());
                DB::commit();
                return $this->response->ok(compact('patient'));
            }
            return $this->response->badRequest('No se encontro el paciente');
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

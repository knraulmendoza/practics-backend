<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateModuleRequest;
use App\Http\Response;
use App\Module;
use App\Person;
use App\Subject;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Módulo 
 *
 */
class ModuleController extends Controller
{
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Registrar Módulo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre del módulo
     * @bodyParam code String required Código del módulo
     * @bodyParam description String Descripción del módulo
     * @bodyParam subject_code String required Código de la asignatura
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"module": {"id": "int", "name": "String", "code": "String", "description": "String", "subject_id": "int", "career_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(ValidateModuleRequest $request)
    {
        $career = intval($request->header('career'));
        try {
            DB::beginTransaction();
            $module = new Module($request->all());
            $subject = Subject::where('code', $request->input('subject_code'))->first();
            $module->subject_id = $subject->id;
            $module->career_id = $career;
            $module->save();
            $module->subject = $subject->name;
            $module->subject_code = $subject->code;
            DB::commit();
            return $this->response->ok(compact('module'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Obtener módulo por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"module": {"id": "int", "name": "String", "code": "String", "description": "String", "subject_id": "int", "career_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function show($id)
    {
        try {
            $module = Module::find($id);
            return $this->response->ok(compact('module'));
        } catch (Exception $ex) {
            return $this->response->badRequest('Ocurrio un error al obtener el modulo');
        }
    }
    /**
     * Listar procedimientos por el id del módulo
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"procedures": [{"id": "int", "name": "String", "description": "String", "module_id": "int"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function showProcedures($id)
    {
        try {
            $procedures = Module::find($id)->procedures()->get();
            return $this->response->ok(compact('procedures'));
        } catch (Exception $ex) {
            return $this->response->badRequest('Ocurrio un error al obtener el modulo');
        }
    }

    /**
     * Listar procedimientos paginado por el id del módulo
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "procedures": [{"id": "int", "name": "String", "description": "String", "module_id": "int"}],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Ocurrio un error al obtener el modulo",
     *      "data": null
     * }
     * 
     */
    public function showProceduresPaginate($id)
    {
        try {
            $per_page = 4;
            $procedures = Module::find($id)->procedures()->paginate($per_page);
            $total = $procedures->total();
            $procedures = $procedures->items();
            return $this->response->ok(compact('procedures', 'total', 'per_page'));
        } catch (Exception $ex) {
            return $this->response->badRequest('Ocurrio un error al obtener el modulo');
        }
    }

    /**
     * Listar estudiantes por módulo
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "students": [{"student_id": "int", "people": "Object<Person>"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Ocurrio un error al obtener el modulo",
     *      "data": null
     * }
     * 
     */
    public function getStudents($id)
    {
        $students = Person::select('people.*', 'students.id as student_id')
            // $person = Person::select('persons.*')
            ->join('students', 'students.person_id', '=', 'people.id')
            ->join('students_groups', 'students_groups.student_id', '=', 'students.id')
            ->join('groups_modules', 'students_groups.group_module_id', '=', 'groups_modules.id')
            ->join('modules', 'groups_modules.module_id', '=', 'modules.id')
            ->where('modules.id', '=', $id)->get();
        return $this->response->ok(compact('students'));
    }

    /**
     * Actualizar módulo
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     * @bodyParam name String required Nombre del módulo
     * @bodyParam code String required Código del módulo
     * @bodyParam description String Descripción del módulo
     * @bodyParam subject_code String Código de la asignatura
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"module": {"id": "int", "name": "String", "code": "String", "description": "String", "subject_id": "int", "career_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function update(Request $request, $id)
    {
        $career = intval($request->header('career'));
        try {
            DB::beginTransaction();
            $module = Module::findOrFail($id);
            $module->subject_id = Subject::where('code', $request->input('subject_code'))->first()->id;
            $module->update($request->all());
            $module->career_id = $career;
            DB::commit();
            return $this->response->ok(compact('module'));
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->response->badRequest('Ocurrio un eror');
        }
    }
}

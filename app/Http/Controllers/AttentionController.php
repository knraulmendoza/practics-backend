<?php

namespace App\Http\Controllers;

use App\Attention;
use App\Enums\Roles;
use App\Enums\StateAttention;
use App\GroupModule;
use App\Http\Requests\ValidateAttentionRequest;
use App\Http\Requests\ValidatePatientRequest;
use App\Http\Response;
use App\Module;
use App\Patient;
use App\PatientPoll;
use App\PracticeScenario;
use App\ProceduresAttentions;
use App\Subject;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  Atenciones
 *
 * endpoints para asignaciones
 */
class AttentionController extends Controller
{
    public $table = 'attention';
    public $response;

    public function __construct()
    {
        $this->response = new Response;
        $this->middleware('auth.role:1;3;5', ['only' => ['index']]);
        $this->middleware('auth.role:1;5', ['only' => ['store']]);
        $this->middleware('auth.role:1;3;5', ['only' => ['update']]);
        $this->middleware('auth.role:1;3;5', ['only' => ['attentionByDate']]);
        $this->middleware('auth.role:1;3;5', ['only' => ['getattentionsByStudent']]);
        $this->middleware('auth.role:5', ['only' => ['groupByDateStudent']]);
        $this->middleware('auth.role:1;2', ['only' => ['groupByDate']]);
    }
    /**
     * Listar Atenciones
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentions": [
     *              {
     *                  "id": "int",
     *                  "date": "Date",
     *                  "state": "int",
     *                  "student_id": "int",
     *                  "academic_period_id": "int",
     *                  "patient_id": "int",
     *                  "procedures": [],
     *                  "patient": {}
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function index(Request $request)
    {
        $career_id = $request->header('career');
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            $attentions = [];
            if ($user->hasRole(strval(Roles::STUDENT))) {
                $person = $user->person()->first();
                $student = $person->student()->first();
                $attentions = Attention::where('student_id', $student->id)->where('date', date('Y-m-d'))->with('procedures', 'patient')->get();
            } else {
                $person = $user->person()->first();
                $teacher = $person->teacher()->first();
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if ($academicPeriod == null) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
                $attentions = Attention::where('date', date('Y-m-d'))->where('assignment_id', '=', $assignment->id)->with('procedures', 'patient')->orderBy('date', 'DESC')->get();
            }
            return $this->response->ok(compact('attentions'));
        }
    }
    /**
     * Listar Atenciones por estudiante
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentions": [
     *              {
     *                  "id": "int",
     *                  "date": "Date()",
     *                  "state": "int",
     *                  "student_id": "int",
     *                  "academic_period_id": "int",
     *                  "patient_id": "int",
     *                  "procedures": [],
     *                  "patient": {}
     *              } 
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getattentionsByStudent(int $studentId, string $date = null)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                if(!$date) $date = date('Y-m-d');
                $attentions = Attention::where('student_id', $studentId)
                    ->where('date', $date)
                    ->with('procedures', 'patient')->get();
                return $this->response->ok(compact('attentions'), empty($attentions) ? false : true);
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Listar Atenciones por estudiante
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentions": [
     *              {
     *                  "id": "int",
     *                  "date": "Date",
     *                  "state": "int",
     *                  "student_id": "int",
     *                  "academic_period_id": "int",
     *                  "patient_id": "int",
     *                  "procedures": [],
     *                  "patient": {}
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getAttentions(Request $request, int $studentId = null)
    {
        $career_id = $request->header('career');
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            $attentions = [];
            $person = $user->person()->first();
            $teacher = $person->teacher()->first();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if ($academicPeriod == null) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
            $attentions = $studentId == null ?
                Attention::where('date', date('Y-m-d'))->where('assignment_id', '=', $assignment->id)->orderBy('date', 'DESC')->get()
                : Attention::where('date', date('Y-m-d'))->where('student_id', '=', $studentId)->where('assignment_id', '=', $assignment->id)->orderBy('date', 'DESC')->get();
            for ($i = 0; $i < count($attentions); $i++) {
                $procedures = $attentions[$i]->procedures()->get();
                $attentions[$i]->procedures = $procedures;
                $attentions[$i]->patient = $attentions[$i]->patient()->first();
            }
            return $this->response->ok(compact('attentions'));
        }
    }

    /**
     * Listar Atenciones
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "patients": [
     *              {
     *                  "id": "int",
     *                  "document_type": "int",
     *                  "document": "String",
     *                  "name": "String"
     *              }
     *          ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getAllAttentions(Request $request)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            $career_id = intval($request->header('career'));
            $per_page = 8;
            $patients = Patient::select('patients.*')
                ->join('attentions', 'patients.id', 'attentions.patient_id')
                ->join('students', 'attentions.student_id', 'students.id')
                ->join('people', 'students.person_id', 'people.id')
                ->join('people_careers as pc', 'people.id', 'pc.person_id')
                ->join('careers', 'pc.career_id', 'careers.id')
                ->where('careers.id', $career_id)
                ->groupBy('patients.id')
                ->orderByDesc('attentions.academic_period_id')
                ->with('attentions')
                ->paginate($per_page);
            foreach ($patients as &$patient) {
                $patient->attentions->load('procedures', 'student');
                foreach ($patient->attentions as &$attention) {
                    $attention->student->load('person');
                    $answersSum = null;
                    $answers = null;
                    if ($attention->state == StateAttention::EVALUATED) {
                        $answersSum = $attention->poll->answers->sum('answer');
                        $answers = $attention->poll->answers->count();
                    }
                    if ($answers > 0) {
                        $result = $answersSum / $answers;
                        if ($result < 2.33) {
                            $attention->encuesta = 'Insatisfacción ';
                        }
                        if ($result > 2.34 && $result < 3.66) {
                            $attention->encuesta = 'Satisfacción Intermedia ';
                        }
                        if ($result > 3.67 && $result < 5) {
                            $attention->encuesta = 'Satisfacción Completa ';
                        }
                    } else {
                        $attention->encuesta = null;
                    }
                }
            }
            $total = $patients->total();
            $patients = $patients->items();
            return $this->response->ok(compact('patients', 'total', 'per_page'));
        }
    }

    /**
     * Listar Atenciones por estudiante
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentionsByDate": {
     *                 "fecha": [
     *                      {"attention": {}}
     *                  ]
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function groupByDateStudent(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person()->first();
                $student = $person->student()->first();
                $attentionsByDate = (array) Attention::all()->where('student_id', $student->id)->where('academic_period_id', $academicPeriod->academic_period_id)->groupBy('date')->toArray();
                foreach ($attentionsByDate as &$attention) {
                    for ($i = 0; $i < count($attention); $i++) {
                        $attention[$i] = new Attention($attention[$i]);
                        $procedures = $attention[$i]->procedures()->get();
                        $attention[$i]->procedures = $procedures;
                        $attention[$i]->patient = $attention[$i]->patient()->first();
                    }
                }
                return $this->response->ok(compact('attentionsByDate'));
            }
        } catch (QueryException $e) {
            return $this->response->badRequest();
        }
    }

    /**
     * Buscar pacientes por escenario 
     * 
     * @bodyParam type String required Tipo de busqueda
     * @bodyParam value String required Valor a buscar
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "patients": [
     *              {"document_type": "int", "document": "String", "name": "string", "attentions": "Array<Attention>"}
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getAttentionSearch(Request $request)
    {
        try {
            $patients = [];
            $table = '';
            switch ($request->input('type')) {
                case 'document':
                    $document = '%' . $request->input('value') . '%';
                    $patients = Patient::select('patients.*')
                        ->join('attentions', 'patients.id', "attentions.patient_id")
                        ->where('document', 'LIKE', "$document")
                        ->groupBy('patients.id')
                        ->with('attentions')->get();
                    break;
                case 'name':
                    $name = explode(' ', $request->input('value'));
                    $full_name = '';
                    foreach ($name as $nameAux) {
                        $full_name = $full_name . '+' . $nameAux . '-';
                    }
                    $full_name = str_replace('-', ' ', $full_name);
                    $query = "MATCH(name) AGAINST('$full_name' IN BOOLEAN MODE)";
                    $patients = Patient::select('patients.*')
                        ->join('attentions', 'patients.id', "attentions.patient_id")->whereRaw($query)
                        ->groupBy('patients.id')
                        ->with('attentions')->get();
                    break;
            }
            foreach ($patients as &$patient) {
                $patient->attentions->load('procedures', 'student', 'poll');
                foreach ($patient->attentions as &$attention) {
                    $attention->student->load('person');
                    $answersSum = null;
                    $answers = null;
                    if ($attention->poll) {
                        $answersSum = $attention->poll->answers->sum('answer');
                        $answers = $attention->poll->answers->count();
                    }
                    if ($answers > 0) {
                        if ($answersSum / $answers < 5) {
                            $attention->encuesta = 'regular';
                        }
                    } else {
                        $attention->encuesta = null;
                    }
                }
            }
            return $this->response->ok(compact('patients'), count($patients) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Atenciones enviado la fecha por parametro
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentionsByDate": [
     *              {
     *                  "fecha": [
     *                      {
     *                          "id": "int", "date": "Date", "state": "int", "student_id": "int", "academic_period_id": "int",
     *                          "patient":{"document_type": "int", "document": "String", "name": "string"},
     *                          "procedures": "Array<Procedure>"
     *                       }
     *                  ]
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function attentionByDate(String $date)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person()->first();
                $student = $person->student()->first();
                $attentionsByDate = (array) Attention::all()->where('student_id', '=', $student->id)->where('date', $date)->groupBy('date')->toArray();
                foreach ($attentionsByDate as &$attention) {
                    for ($i = 0; $i < count($attention); $i++) {
                        $attention[$i] = new Attention($attention[$i]);
                        $procedures = $attention[$i]->procedures()->get();
                        $attention[$i]->procedures = $procedures;
                        $attention[$i]->patient = $attention[$i]->patient()->first();
                    }
                }
                return $this->response->ok(compact('attentionsByDate'), count($attentionsByDate) == 0 ? false : true);
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Atenciones por fecha
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentionsByDate": [
     *              {
     *                  "fecha": [
     *                      {
     *                      "id": "int", "date": "Date", "state": "int", "student_id": "int", "academic_period_id": "int",
     *                      "patient":{"document_type": "int", "document": "String", "name": "string"},
     *                      "procedures": "Array<Procedure>"
     *                       }
     *                  ]
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function groupByDate()
    {
        $attentionsByDate = Attention::all()->groupBy('date')->toArray();
        return $this->response->ok(compact('attentionsByDate'));
    }
    /**
     * Atenciones por fecha
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentionsByDate": [
     *              {
     *                  "fecha": [
     *                      {
     *                      "id": "int", "date": "Date", "state": "int", "student_id": "int", "academic_period_id": "int"
     *                       }
     *                  ]
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function encuesta(int $id)
    {
        try {
            $poll = PatientPoll::where('attention_id', $id)->first();
            $answers = $poll->answers()->with('question')->get();
            return $answers;
        } catch (Exception $e) {
            $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar Atención
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam patient Object required Datos básicos del paciente
     * @bodyParam patient.document_type String required Tipo de documento
     * @bodyParam patient.document String required Número de documento
     * @bodyParam patient.name String required Nombre del paciente
     * @bodyParam date Date required Fecha el cual se registra la atención
     * @bodyParam procedures Array<int> required Lista de procedimientos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attention": {
     *              "student_id": "int",
     *              "academic_period_id": "int",
     *              "patient_id": "int",
     *              "assignment_id": "int"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */


    public function store(ValidateAttentionRequest $request)
    {
        $career_id = $request->header('career');
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            $message = '';
            try {
                DB::beginTransaction();
                $person = $user->person()->first();
                $student = $person->student()->first();
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                if ($student) {
                    $assistence = $student->assistence($academicPeriod->academic_period_id)->first();
                    if ($assistence) {
                        $group = $student->groups($academicPeriod->academic_period_id)->first();
                        $patient = ((new PatientController())->register((new ValidatePatientRequest($request->input('patient')))));
                        if ($patient) {
                            $attention = new Attention();
                            $attention->date = $request->input('date');
                            $attention->student_id = $student->id;
                            $attention->academic_period_id = $academicPeriod->academic_period_id;
                            $attention->patient_id = $patient->id;
                            $attention->assignment_id = $this->assigmentIdByStudent($group->id);
                            if ($attention->save()) {
                                for ($i = 0; $i < count($request->input('procedures')); $i++) {
                                    $procedureAttention = new ProceduresAttentions();
                                    $procedureAttention->procedure_id = $request->input('procedures')[$i];
                                    $procedureAttention->attention_id = $attention->id;
                                    $procedureAttention->save();
                                }
                                DB::commit();
                                return $this->response->ok(compact('attention'));
                            } else {
                                $message = 'No se creo la atención';
                            }
                        } else {
                            $message = 'No se creo el paciente';
                        }
                    } else {
                        $message = 'El profesor no ha subido la asistencia de hoy';
                    }
                } else {
                    $message = 'Tu no eres un estudiante';
                }

                DB::rollBack();
                return $this->response->badRequest($message);
            } catch (QueryException $e) {
                DB::rollBack();
                return $this->response->badRequest($e->getMessage());
            }
        }
    }

    public function assigmentIdByStudent(int $id)
    {
        return GroupModule::select('assign_student_groups.assignment_id as id')
            ->join('assign_student_groups', 'assign_student_groups.group_module_id', 'groups_modules.id')
            ->join('rotations', 'assign_student_groups.rotation_id', 'rotations.id')
            ->whereRaw("groups_modules.id = ? AND ? BETWEEN rotations.start_date AND rotations.end_date", [$id, date('Y-m-d')])->first()->id;
    }
    /**
     * 
     * Atenciones por actividades
     * 
     * @bodyParam subject String required Código de la asignatura
     * @bodyParam academicPeriod int required Id del periodo academico
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentions": [{
     *              "student_id": "int",
     *              "academic_period_id": "int",
     *              "patient_id": "int",
     *              "assignment_id": "int"
     *          }],
     *          "subject": "Object<Subject>"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function attentionByActivities(Request $request)
    {
        $career_id = intval($request->header('career'));
        $subject = Subject::where('code', $request->input('subject'))->first();
        $modules = Module::where('career_id', $career_id)->where('subject_id', $subject->id)->get();
        $attentions = [];
        foreach ($modules as $module) {
            $attentionsAux = Attention::select('procedures.name', DB::raw('count(*) as total'))
                ->join('procedures_attentions as pa', 'attentions.id', 'pa.attention_id')
                ->join('procedures', 'pa.procedure_id', 'procedures.id')
                ->join('modules', 'procedures.module_id', 'modules.id')
                ->where('modules.id', $module->id)
                ->where('attentions.academic_period_id', $request->input('academicPeriod'))
                ->groupBy('procedures.id')->get();
            foreach ($attentionsAux as $attentionAux) {
                array_push($attentions, $attentionAux);
            }
        }
        return $this->response->ok(compact('attentions', 'subject'), count($attentions) > 0 ? true : false);
    }
    /**
     * 
     * Atenciones por actividades
     * 
     * @bodyParam subject String required Código de la asignatura
     * @bodyParam module int required Id del módulo
     * @bodyParam academicPeriod int required Id del periodo academico
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attentions": [{
     *              "student_id": "int",
     *              "academic_period_id": "int",
     *              "patient_id": "int",
     *              "assignment_id": "int"
     *          }],
     *          "subject": "Object<Subject>"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function attentionByModule(Request $request)
    {
        $career_id = intval($request->header('career'));
        $subject = Subject::where('code', $request->input('subject'))->first();
        $attentions = Attention::select('procedures.name', DB::raw('count(*) as total'))
            ->join('procedures_attentions as pa', 'attentions.id', 'pa.attention_id')
            ->join('procedures', 'pa.procedure_id', 'procedures.id')
            ->join('modules', 'procedures.module_id', 'modules.id')
            ->where('modules.id', $request->input('module'))->where('attentions.academic_period_id', $request->input('academicPeriod'))
            ->groupBy('procedures.id')->get();
        return $this->response->ok(compact('attentions', 'subject'), count($attentions) > 0 ? true : false);
    }

    /**
     * Attentión por escenarios
     */
    public function attentionByScenario(Request $request)
    {
        $career_id = intval($request->header('career'));
        $scenarioId = $request->input('scenario');
        $moduleId = $request->input('module');
        $academicPeriod = $request->input('academicPeriod');
        $attentionsAux = Attention::select('areas.name', DB::raw('count(*) as total'), 'psam.*')
            ->join('assignments', 'attentions.assignment_id', 'assignments.id')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
            ->join('areas', 'psa.area_id', 'areas.id')
            ->where('psa.practice_scenarios_id', $scenarioId)
            ->where('attentions.academic_period_id', $academicPeriod)
            ->where('attentions.state', '!=', strval(StateAttention::AWAIT));
        if ($moduleId) {
            $attentionsAux->where('psam.module_id', $moduleId);
        }
        $attentions = $attentionsAux->groupBy('areas.id')->get();
        return $this->response->ok(compact('attentions'), count($attentions) > 0 ? true : false);
    }


    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attention  $attention
     * @return \Illuminate\Http\Response
     * 
     * 
     * actualizar Atención
     * 
     * @bodyParam state int required Estado de la atención
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "attention": {
     *              "student_id": "int",
     *              "academic_period_id": "int",
     *              "patient_id": "int",
     *              "assignment_id": "int"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function update(Request $request, int $id)
    {
        try {
            DB::beginTransaction();
            $attention = Attention::findOrFail($id);
            $attention->state = $request->input('state');
            $attention->update();
            DB::commit();
            return $this->response->ok(compact('attention'));
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Coordinator;
use App\Enums\Roles;
use App\Enums\StateUser;
use App\Http\Requests\ValidatePersonRequest;
use App\Http\Response;
use App\Person;
use App\PeopleCareers;
use App\Student;
use App\Teacher;
use App\TeacherTitle;
use App\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group persona 
 *
 */
class PersonController extends Controller
{
    public $response;
    public $personId = 'person_id';
    public $table = 'person';
    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2;3', ['only' => ['index']]);
    }

    /**
     * Registrar persona y asociar el rol
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam person Object required Objeto de tipo persona
     * @bodyParam person.document_type String required Tipo de documento
     * @bodyParam person.document String required Documento de identidad
     * @bodyParam person.first_name String required Primer nombre
     * @bodyParam person.second_name String Segundo nombre
     * @bodyParam person.first_last_name String required Primer apellido
     * @bodyParam person.second_last_name String required Segundo apellido
     * @bodyParam person.address String Dirección de la persona
     * @bodyParam person.gender int Genero de la persona
     * @bodyParam person.user_id required Id usuario de la persona
     * @bodyParam titles Array<int> Id de los títulos de los profesores
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "person": "Object<Person>", "table_data": "dynamic (Student, teacher o coordinator)"}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function store(ValidatePersonRequest $request)
    {
        $career_id = intval($request->header('career'));
        $message = "";
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            try {
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (is_null($academicPeriod)) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                DB::beginTransaction();
                $person = new Person($request->input('person'));
                $person_career = new PeopleCareers();
                if ($person->save()) {
                    $userPerson = User::where('id', '=', $request->input('person.user_id'))->first();
                    $userPerson->state = StateUser::ACTIVO;
                    $userPerson->update();
                    $role = $userPerson->roles()->first();
                    $person_career->person_id = $person->id;
                    $person_career->career_id = $career_id; //$role->id == Roles::COORDINATOR?$request->input('person.career_id') :$user->person()->first()->careers()->first()->id;
                    if ($person_career->save()) {
                        $table_data = null;
                        switch ($role->id) {
                            case Roles::COORDINATOR:
                                $table_data = new Coordinator();
                                $table_data->person_id = $person->id;
                                $coordinator = Coordinator::select('coordinators.*')->join('people', 'coordinators.person_id', 'people.id')
                                    ->join('people_careers as pc', 'people.id', 'pc.person_id')
                                    ->join('careers', 'pc.career_id', 'careers.id')
                                    ->where('careers.id', $career_id)->where('state', '1')->first();
                                // $coordinator = Coordinator::find($coordinatorAux->id);
                                if ($coordinator) {
                                    $coordinator->state = '0';
                                    $coordinator->update();
                                    $person = $coordinator->person()->first();
                                    $user = $person->user()->first();
                                    $user->state = StateUser::INACTIVO;
                                    $user->update();
                                }
                                break;
                            case Roles::TEACHER:
                                $table_data = new Teacher();
                                $table_data->person_id = $person->id;
                                break;
                            case Roles::TEACHER_PRACTICT:
                                $table_data = new Teacher();
                                $table_data->person_id = $person->id;
                                break;
                            case Roles::STUDENT:
                                $table_data = new Student();
                                $table_data->person_id = $person->id;
                                break;
                            default:
                                $table_data = null;
                                break;
                        }
                        if ($table_data != null) {
                            $table_data->save();
                        }
                        if ($request->input('titles') && $table_data != null) {
                            for ($i = 0; $i < count($request->input('titles')); $i++) {
                                $teacheTitle = new TeacherTitle();
                                $teacheTitle->teacher_id = $table_data->id;
                                $teacheTitle->title_id = $request->input('titles')[$i];
                                $teacheTitle->save();
                            }
                        }
                        DB::commit();
                        $table_data->person = $person;
                        return $this->response->ok(compact('person', 'table_data'));
                    }
                } else {
                    $message = "Error al registrar la persona";
                }
                DB::rollback();
            } catch (QueryException $e) {
                DB::rollBack();
                $message = $e;
                //return $this->response->code($e->getCode(),$this->table);
            }
        }
        return $this->response->badRequest($message);
    }

    /**
     * Obtener datos de la persona por el token
     * 
     * @authenticated
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "person": {"id": "int", "document_type": "String", "document": "String", "first_name": "String", "second_name": "String", "first_last_name": "String", "second_last_name": "String",
     *                  "phone": "String", "birthday": "String", "address": "String", "gender": "int", "user_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function getPerson()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            $person = $user->person()->first();
            return $this->response->ok(compact('person'));
        } catch (JWTException $e) {
            return $this->response->unauthorized($e->getMessage());
        }
    }

    /**
     * 
     * Buscar persona
     * 
     * @bodyParam person_type String required Tipo de persona a buscar
     * @bodyParam type String required Tipo de busqueda (document, name)
     * @bodyParam value String required Valor a buscar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "people": [{"id": "int", "document_type": "String", "document": "String", "first_name": "String", "second_name": "String", "first_last_name": "String", "second_last_name": "String",
     *                  "phone": "String", "birthday": "String", "address": "String", "gender": "int", "user_id": "int"}]}
     * }
     * 
     */
    public function getPersonSearch(Request $request)
    {
        try {
            $people = [];
            $table = '';
            switch ($request->input('person_type')) {
                case Roles::STUDENT:
                    $table = 'students';
                    break;
                case Roles::TEACHER_PRACTICT:
                    $table = 'teachers';
                    break;
                case Roles::TEACHER:
                    $table = 'teachers';
                    break;
                case Roles::COORDINATOR:
                    $table = 'coordinators';
                    break;
                default:
                    return $this->response->ok(compact('people'), count($people) > 0 ? true : false);
            }
            switch ($request->input('type')) {
                case 'document':
                    $document = '%' . $request->input('value') . '%';
                    $people = Person::join($table, 'people.id', "$table.person_id")->where('document', 'LIKE', "$document")->get();
                    break;
                case 'name':
                    $name = explode(' ', $request->input('value'));
                    $full_name = '';
                    foreach ($name as $nameAux) {
                        $full_name = $full_name . '+' . $nameAux . '-';
                    }
                    $full_name = str_replace('-', ' ', $full_name);
                    $query = "MATCH(first_name, second_name, first_last_name, second_last_name) AGAINST('$full_name' IN BOOLEAN MODE)";
                    $people = Person::join($table, 'people.id', "$table.person_id")->whereRaw($query)->get();
                    break;
            }
            foreach ($people as $key => $person) {
                $person_type = new stdClass();
                $person_type->id = $person->id;
                $person_type->person = Person::find($person->person_id);
                if ($request->input('person_type') == Roles::TEACHER_PRACTICT || $request->input('person_type') == Roles::TEACHER) {
                    $roles = $person_type->person->user()->first()->roles()->get()->toArray();
                    $person_type->types = array_map(function ($rol) {
                        return $rol['id'];
                    }, $roles);
                    $person_type->state = $person->state;
                }
                $people[$key] = $person_type;
            }

            return $this->response->ok(compact('people'), count($people) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest();
        }
    }


    /**
     * Actualizar datos de persona
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @bodyParam person.address String Dirección de la persona
     * @bodyParam person.gender int Genero de la persona
     */
    // public function update(Request $request, $id)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $person = Person::findOrFail($id);
    //         if ($person) {
    //             $person->update($request->all());
    //             DB::commit();
    //             return $this->response->ok(compact('person'));
    //         }
    //         return $this->response->badRequest('No se encontro la persona');
    //     } catch (QueryException $ex) {
    //         DB::rollback();
    //         return $this->response->badRequest($ex->getMessage());
    //     }
    // }
}

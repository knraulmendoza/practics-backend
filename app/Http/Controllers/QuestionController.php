<?php

namespace App\Http\Controllers;

use App\Career;
use App\CareersQuestions;
use App\Enums\Roles;
use App\Http\Requests\ValidateQuestionRequest;
use App\Http\Response;
use App\Question;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group Preguntas chequeos
 */
class QuestionController extends Controller
{
    public $response;
    public $nameTable = 'question';
    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2', ['only' => ['index', 'store', 'update']]);
    }

    /**
     * Registrar Pregunta de chequeo o encuesta
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam type int required Tipo de pregunta
     * @bodyParam question String required Pregunta
     * @bodyParam check_type String required Tipo de chequeo
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"question": {"id": "int", "type": "int", "question": "String", "check_type": "String"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function store(ValidateQuestionRequest $request)
    {
        $mensaje = '';
        $career = intval($request->header('career'));
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            DB::beginTransaction();
            $question = new Question($request->all());
            $careers_questions = new CareersQuestions();
            if ($question->save()) {
                $careers_questions->career_id = $career;
                $careers_questions->question_id = $question->id;
                if ($careers_questions->save()) {
                    DB::commit();
                    return $this->response->ok(compact('question'));
                } else {
                    $mensaje = 'Hubo un error en registrar la pregunta en esta carrera';
                }
            } else {
                $mensaje = 'Hubo un error en registrar la pregunta';
            }
            DB::rollBack();
            return $this->response->badRequest($mensaje);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->response->badRequest();
        } catch (JWTException $e) {
            return $this->response->unauthorized('error en el token');
        }
    }
    /**
     * 
     * Listar preguntas por el rol
     * 
     * El estado puede ser falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"questions": [{"id": "int", "type": "int", "question": "String", "check_type": "String"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function questionByRol(Request $request)
    {
        $rol = $request->header('rol');
        $career = $request->header('career');
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            $person = $user->person()->first();
            $career = $person->careers()->where('id', $career)->first();
            $questions = [];
            switch ($rol) {
                case Roles::TEACHER_PRACTICT:
                    $teacher = $person->teacher()->first();
                    if ($teacher) {
                        $questions = $career->questions()->where('check_type', 'like', '%students%')->get();
                    }
                    break;
                case Roles::COORDINATOR:
                    $coordinator = $person->coordinator()->first();
                    if ($coordinator) {
                        $questions = $career->questions()->where('check_type', 'like', '%teachers%')->get();
                    }
                    break;
                default:
                    $questions = [];
                    break;
            }
            return $this->response->ok(compact('questions'), !$questions ? false : true);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * 
     * Listar preguntas paginada por el tipo de chequeo
     * 
     * El estado puede ser falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [{"id": "int", "type": "int", "question": "String", "check_type": "String"}],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function getQuestionsByCheckType(Request $request)
    {
        try {
            $careerId = intval($request->header('career'));
            $career = Career::find($careerId);
            $per_page = 13;
            $questions = [];
            if ($career) {
                if ($request->input('type') == 2) {
                    $questions = $career->questions()->where('type', $request->input('type'))->paginate($per_page);
                } else {
                    $check = '%' . $request->input('check_type') . '%';
                    $questions = $career->questions()->where('type', $request->input('type'))->where('check_type', 'like', $check)->paginate($per_page);
                }
            }
            $total = $questions->total();
            $questions = $questions->items();
            return $this->response->ok(compact('questions', 'total', 'per_page'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Listar todas las preguntas por el tipo de chequeo
     * 
     * El estado puede ser falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [{"id": "int", "type": "int", "question": "String", "check_type": "String"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function getQuestionsByCheckTypeAll(Request $request)
    {
        try {
            $careerId = intval($request->header('career'));
            $career = Career::find($careerId);
            $questions = [];
            if ($career) {
                if ($request->input('type') == 2) {
                    $questions = $career->questions()->where('type', $request->input('type'))->get();
                } else {
                    $check = '%' . $request->input('check_type') . '%';
                    $questions = $career->questions()->where('type', $request->input('type'))->where('check_type', 'like', $check)->get();
                }
            }
            return $this->response->ok(compact('questions'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Buscar pregunta
     * 
     * El estado puede ser falso si no hay datos
     * 
     * @bodyParam question String required Pregunta a buscar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [{"id": "int", "type": "int", "question": "String", "check_type": "String"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function search(Request $request)
    {
        $question = $request->input('question');
        $query = "MATCH(question) AGAINST('$question' IN BOOLEAN MODE)";
        $questions = Question::whereRaw($query)->get();
        return $this->response->ok(compact('questions'), count($questions) == 0 ? false : true);
    }

    /**
     * Actualizar pregunta
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam type int required Tipo de pregunta
     * @bodyParam question String required Pregunta
     * @bodyParam check_type String required Tipo de chequeo
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"question": {"id": "int", "type": "int", "question": "String", "check_type": "String"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function update(ValidateQuestionRequest $request, $id)
    {
        $mensaje = '';
        try {
            DB::beginTransaction();
            $question = Question::findOrFail($id);
            if ($question->update($request->all())) {
                DB::commit();
                return $this->response->ok(compact('question'));
            }
            DB::rollBack();
            return $this->response->badRequest();
        } catch (QueryException $e) {
            return $this->response->badRequest();
        }
    }
    /**
     * 
     * Actualizar pregunta en carrea
     * 
     * @bodyParam career_id int required Id de la carrera
     * @bodyParam question_id int required Id de la pregunta
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"career_question": {"career_id": "int", "question_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     * 
     */
    public function updateCareerQuestion(Request $request)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message' => 'user_not_found'], 404);
        } else {
            $mensaje = '';
            try {
                DB::beginTransaction();
                $career_question = new CareersQuestions();
                if ($career_question->update($request->all())) {
                    return $this->response->ok(compact('career_question'));
                }
                DB::rollBack();
                return $this->response->unauthorized($mensaje);
            } catch (QueryException $e) {
                return $this->response->badRequest();
            }
        }
    }
}

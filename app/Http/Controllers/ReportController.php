<?php

namespace App\Http\Controllers;

use App\AcademicPeriod;
use App\Http\CompresionImagen;
use App\Http\Response;
use App\Report;
use App\ReportData;
use App\ReportMeta;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Reportes
 * 
 */
class ReportController extends Controller
{

    public $response;
    public $compresionImage;
    public function __construct()
    {
        $this->response = new Response();
        $this->compresionImage = new CompresionImagen();
    }


    /**
     * Reporte por categoría
     * 
     * El estado cambia a falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"reports": [{"id":"int", "career_id": "int", "title": "String", "description": "String", "report_category": "int", "image": "String", "report_metas": [], "report_datas": []}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function reportByCategory(Request $request, int $reportCategory, int $academicPeriodAux)
    {
        try {
            $career_id = intval($request->header('career'));
            $reports = Report::where('career_id', $career_id)
                ->where('report_category', $reportCategory)
                ->where('academic_period_id', $academicPeriodAux)->get();
            foreach ($reports as &$report) {
                $report->report_metas = $report->reportMetas()->get(['key', 'value', 'tabla']);
                $report->report_datas = $report->reportDatas()->get(['name', 'total']);
                foreach ($report->report_metas as &$reportMeta) {
                    if ($reportMeta->key == 'academic_period') {
                        $academicPeriod = AcademicPeriod::find($reportMeta->value);
                        $report->academic_period = $academicPeriod->year . ' - ' . $academicPeriod->period;
                    }
                }
            }
            return $this->response->ok(compact('reports'), count($reports) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar reporte
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam report_category String required Categoría a la que pertenece el reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Grafica del reporte
     * @bodyParam report_metas Array required Metas que debe tener el reporte
     * @bodyParam report_datas Array datos del reporte
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"report": {"id": "int", "report_category": "String", "title": "String", "description": "String", "image": "String", "report_metas": [], "report_datas": []}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $career_id = intval($request->header('career'));
            $report = new Report();
            $report->career_id = $career_id;
            $report->report_category = $request->input('report_category');
            $report->title = $request->input('title');
            $report->description = $request->input('description');
            $image = $request->file('image');
            if ($image) {
                $directory = public_path('images/reports/');
                $array = explode('.', $image->getClientOriginalName());
                $uid = uniqid();
                $image->move($directory, $array[0] . $uid . '.' . end($array));
                $report->image = $directory . $array[0] . $uid . '.' . end($array);
                // $report->image = $this->compresionImage->saveImage($image, 'reports');
            }
            $report->academic_period_id = $academicPeriod->academic_period_id;
            if ($report->save()) {
                $report_metas = json_decode($request->input('report_metas'));
                foreach ($report_metas as $report_meta) {
                    $reportMeta = new ReportMeta();
                    $reportMeta->key = $report_meta->key;
                    $reportMeta->value = $report_meta->value;
                    $reportMeta->tabla = $report_meta->tabla;
                    $reportMeta->report_id = $report->id;
                    $reportMeta->save();
                }
                $report_datas = json_decode($request->input('report_datas'));
                foreach ($report_datas as $report_data) {
                    $reportData = new ReportData();
                    $reportData->name = $report_data->name;
                    $reportData->total = $report_data->total;
                    $reportData->report_id = $report->id;
                    $reportData->save();
                }
                $report->report_metas = $report_metas;
                $report->report_datas = $report_datas;
            }
            DB::commit();
            return $this->response->ok(compact('report'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
}

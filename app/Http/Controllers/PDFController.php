<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Attention;
use Illuminate\Http\Request;
use App\Career;
use App\Enums\Roles;
use App\Enums\StateAttention;
use App\Enums\TypeRotation;
use App\GroupModule;
use App\Module;
use App\StudentGroup;
use App\PracticeScenario;
use App\Procedure;
use App\Student;
use App\Http\CompresionImagen;
use App\Patient;
use App\PracticeScenarioArea;
use App\Rotation;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use stdClass;

/**
 * @group Pdf 
 *
 */
class PDFController extends Controller
{
    public $compresionImage;

    public function __construct()
    {
        $this->compresionImage = new CompresionImagen();
    }


    public function getScenario($id, $periodo_Academic)
    {
        return PracticeScenario::select('ps.name')->from('assignments as ass')
            ->join('practice_scenarios_areas as psa', 'ass.practice_scenarios_areas_id', '=', 'psa.id')
            ->join('practice_scenarios as ps', 'ps.id', '=', 'psa.practice_scenarios_id')
            ->where('ass.module_id', '=', $id)
            ->where('ass.academic_period_id', '=', $periodo_Academic)->get();
    }

    public function getProcedures($id, $periodo_Academic)
    {
        return Procedure::select('p.name')->selectRaw('COUNT(pa.procedure_id) as total')->from('procedures_attentions as pa')
            ->join('procedures as p', 'p.id', '=', 'pa.procedure_id')
            ->join('attentions as a', 'a.id', '=', 'pa.attention_id')
            ->where('a.academic_period_id', '=', $periodo_Academic)
            ->where('p.module_id', '=', $id)
            ->groupBy('pa.procedure_id')->get();
    }

    public function getStudents($id, $periodo_Academic)
    {
        return StudentGroup::select('students_groups.*')
            ->join('groups_modules', 'students_groups.group_module_id', '=', 'groups_modules.id')
            ->where('groups_modules.module_id', '=', $id)
            ->where('students_groups.academic_period_id', '=', $periodo_Academic)->get();
    }

    /**
     * Reporte por asignatura
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ; 
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportBySubject(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $arrayTest = [];
        $report->metas = [];
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $pdf = App::make('dompdf.wrapper');
        $report->modules = Module::where('career_id', $career_id)->where('subject_id',  $report->metas['subject']->id)->get();
        $students = count((new Student())->studentsCountBySubject($report->metas['subject']->id));
        $attentions = 0;
        $patients = count((new Patient())->patientsCountBySubject($report->metas['subject']->id));
        $report->scenarios = [];
        foreach ($report->modules as $module) {
            $attentions += (new Attention())->attentionCount($module->id);
            $scenariosAux = (new PracticeScenario())->scenarios($career_id, $module->id);
            foreach ($scenariosAux as $scenario) {
                if (array_search($scenario, $report->scenarios) === false) {
                    array_push($report->scenarios, $scenario);
                }
            }
        }
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $tempDirectory = public_path('images/reports/');
            $array = explode('.', $image->getClientOriginalName());
            $uid = uniqid();
            $image->move($tempDirectory, $array[0] . $uid . '.' . end($array));
            $filePath = $tempDirectory . $array[0] . $uid . '.' . end($array);
            $report->image = 'data:image/png;base64,' . base64_encode(file_get_contents($filePath));
            File::delete($filePath);
        } else {
            $report->image = $request->input('image');
        }
        $report->duration = Rotation::where('academic_period_id', $report->metas['academic_period']->id)->where('type', TypeRotation::FULL)->first();
        $report->report_datas = json_decode($request->input('report_datas'));
        $data = compact('report', 'students', 'patients', 'attentions', 'career');
        $pdf->loadView('reports.attentionBySubject', $data);
        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed' => TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );
        return $request->hasFile('image') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('atenciones_por_asignatura.pdf')) : $pdf->download($report->title . '.pdf');
    }

    /**
     * Reporte por module
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportByModule(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $pdf = App::make('dompdf.wrapper');
        $report->scenarios = (new PracticeScenario())->scenarios($career_id, $report->metas['module']->id); //$this->getAssignment($institution_id, $periodo_Academic);
        $students = count((new Student())->studentsCountByModule($report->metas['module']->id));
        $attentions = (new Attention())->attentionCount($report->metas['module']->id);
        $patients = count((new Patient())->patientsCountByModule($report->metas['module']->id)->get());
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $tempDirectory = 'images/temporal/';
            $array = explode('.', $image->getClientOriginalName());
            $uid = uniqid();
            $image->move($tempDirectory, $array[0] . $uid . '.' . end($array));
            $filePath = $tempDirectory . $array[0] . $uid . '.' . end($array);
            $report->image = 'data:image/png;base64,' . base64_encode(file_get_contents($filePath));
            File::delete($filePath);
        } else {
            $report->image = $request->input('image');
        }
        $report->duration = Rotation::where('academic_period_id', $report->metas['academic_period']->id)->where('type', TypeRotation::FULL)->first();
        $report->report_datas = json_decode($request->input('report_datas'));
        $data = compact('report', 'students', 'patients', 'attentions', 'career');
        $pdf->loadView('reports.attentionByModule', $data);
        return $request->hasFile('image') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('rotations.pdf')) : $pdf->download($report->title . '.pdf');
    }

    /**
     * Reporte de asistencia
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportAssistenceStudent(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $pdf = App::make('dompdf.wrapper');
        $report->modules = Module::where('career_id', $career_id)->where('subject_id',  $report->metas['subject']->id)->get();
        $students = count((new Student())->studentsCountBySubject($report->metas['subject']->id));
        // foreach ($report->modules as $module) {
        //     $students += (new Student())->studentsCountBySubject($module->id);
        // }
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $tempDirectory = 'images/temporal/';
            $array = explode('.', $image->getClientOriginalName());
            $uid = uniqid();
            $image->move($tempDirectory, $array[0] . $uid . '.' . end($array));
            $filePath = $tempDirectory . $array[0] . $uid . '.' . end($array);
            $report->image = 'data:image/png;base64,' . base64_encode(file_get_contents($filePath));
            File::delete($filePath);
        } else {
            $report->image = $request->input('image');
        }
        $report->report_datas = json_decode($request->input('report_datas'));
        $report->duration = Rotation::where('academic_period_id', $report->metas['academic_period']->id)->where('type', TypeRotation::FULL)->first();
        $days = $this->countDays($report->duration->start_date, date('Y-m-d'));
        $asistencesTomadas = array_reduce($report->report_datas, function ($a, $x) {
            return $a + $x->total;
        }, 0);
        $asistencesEsperadas = $days * $students;
        $data = compact('report', 'students', 'career', 'days', 'asistencesEsperadas', 'asistencesTomadas');
        $pdf->loadView('reports.studentBySubject', $data);
        return $request->hasFile('image') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('rotations.pdf')) : $pdf->download($report->title . '.pdf');
        // return $pdf->stream('asistencias_por_asignatura.pdf');
    }

    /**
     * Reporte por encuesta
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportByPoll(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        $pdf = App::make('dompdf.wrapper');
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $groups = Module::find($report->metas['module']->id)
            ->groups($report->metas['academic_period']->id)->with('students')->get();
        foreach ($groups as &$group) {
            foreach ($group->students as &$student) {
                $student->load('person');
                $attentionsAux = $student->attentions($report->metas['academic_period']->id)->get();
                $student->attentionsAll = count($attentionsAux);
                $student->attentionsAccepted = count(array_filter($attentionsAux->toArray(), function ($value) { return in_array($value['state'], [strval(StateAttention::EVALUATED), strval(StateAttention::ACCEPTED)]); }));
                $student->attentionsEvaluated = count(array_filter($attentionsAux->toArray(), function ($value) { return $value['state'] == strval(StateAttention::EVALUATED); }));
                $student->person->fullName = $student->person->first_name . ' ' .
                    ($student->person->second_name ? $student->person->second_name . ' ' : '') . $student->person->first_last_name . ' ' . $student->person->second_last_name;
            }
        }
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        $data = compact('report', 'career', 'groups');
        $pdf->loadView('reports.reportPatientpoll', $data);
        return $request->input('show_pdf') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('patient_poll.pdf')) : $pdf->download($report->title . '.pdf');
        // return $pdf->stream('patient_poll.pdf');
    }

    /**
     * Reporte chequeo de estudiantes
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportByCheckStudent(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        $pdf = App::make('dompdf.wrapper');
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $groups = Module::find($report->metas['module']->id)
            ->groups($report->metas['academic_period']->id)->with('students')->get();
        $students = [];
        foreach ($groups as &$group) {
            $rotations = $group->rotations()->get();
            foreach ($group->students as &$student) {
                $student->load('person');
                $student->person->fullName = $student->person->first_name . ' ' .
                    ($student->person->second_name ? $student->person->second_name . ' ' : '') . $student->person->first_last_name . ' ' . $student->person->second_last_name;
                $checks = $student->person->checkByEvaluatedAll($report->metas['academic_period']->id);
                $rotAux = [];
                foreach ($rotations as &$rotation) {
                    $rotaAux = new stdClass();
                    $rotaAux->checks = [];
                    foreach ($checks as &$check) {
                        $chAux = $check->checksRotations($rotation->id)->first();
                        if ($chAux) {
                            $check->observation = $check->observations()->first()->observation;
                            $check->yes = $check->answers()->where('answer', 1)->count();
                            $check->no = $check->answers()->where('answer', 0)->count();
                            array_push($rotaAux->checks, $check);
                        }
                    }
                    array_push($rotAux, $rotaAux);
                }
                $student->rotations = $rotAux;
                array_push($students, $student);
            }
        }
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        $rol = Roles::STUDENT;
        $dataRol = $students;
        $data = compact('report', 'career', 'dataRol', 'rol');
        $pdf->loadView('reports.check', $data);
        return $request->input('show_pdf') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('checks_estudiantes.pdf')) : $pdf->download($report->title . '.pdf');
        // return $pdf->stream('checks.pdf');
    }

    /**
     * Reporte chequeo de profesores
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "File || Link de descarga" 
     * 
     */
    public function reportByCheckTeacher(Request $request)
    {
        $career_id = intval($request->header('career'));
        $report = new stdClass();
        $career = Career::find($career_id);
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        $pdf = App::make('dompdf.wrapper');
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key]  = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $assignments = Assignment::select('assignments.*')
            ->join('assign_student_groups as asg', 'assignments.id', 'asg.assignment_id')
            ->join('groups_modules', 'asg.group_module_id', 'groups_modules.id')
            ->groupBy('assignments.id')
            ->where('groups_modules.module_id', $report->metas['module']->id)->get();

        $teachers = [];
        foreach ($assignments as &$assignment) {
            $teacher = $assignment->teacher()->with('person')->first();
            $teacher->person->fullName = $teacher->person->first_name . ' ' .
                ($teacher->person->second_name ? $teacher->person->second_name . ' ' : '') . $teacher->person->first_last_name . ' ' . $teacher->person->second_last_name;
            $teacher->rotations = $assignment->rotations()->get();
            $rotations = $assignment->rotations()->get();
            $checks = $teacher->person->checkByEvaluatedAll($report->metas['academic_period']->id);
            $rotAux = [];
            foreach ($rotations as &$rotation) {
                $rotaAux = new stdClass();
                $rotaAux->checks = [];
                foreach ($checks as &$check) {
                    $chAux = $check->checksRotations($rotation->id)->first();
                    if ($chAux) {
                        $check->observation = $check->observations()->first()->observation;
                        $check->yes = $check->answers()->where('answer', 1)->count();
                        $check->no = $check->answers()->where('answer', 0)->count();
                        array_push($rotaAux->checks, $check);
                    }
                }
                array_push($rotAux, $rotaAux);
            }
            $teacher->rotations = $rotAux;
            array_push($teachers, $teacher);
        }
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        $rol = Roles::TEACHER;
        $dataRol = $teachers;
        $data = compact('report', 'career', 'dataRol', 'rol');
        $pdf->loadView('reports.check', $data);
        return $request->input('show_pdf') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('checks_docentes.pdf')) : $pdf->download($report->title . '.pdf');
        // return $pdf->stream('checksByTeacher.pdf');
    }

    public function countDays($start_date, $end_date)
    {
        $day = 0;
        $dateInterval = new DateInterval('P1D');
        $days = new DatePeriod(new DateTime($start_date), $dateInterval, new DateTime($end_date));
        foreach ($days as $dayAux) {
            // Asignamos un número por cada día de la semana 6 y 7 para sábado y domingo
            $weekDay = $dayAux->format('N');
            // Si es sábado, domingo o festivo no lo imprime
            if ($weekDay !== '6' && $weekDay !== '7') {
                $day += 1;
            }
        }
        return $day;
    }

    public function getAssignment($escenario, $periodo_Academic)
    {
        return Assignment::select('ass.id', 'p.first_name', 'p.first_last_name', 'ar.name as area', 'ps.name as institution', 'su.name as subject', 'asg.group_module_id as module')
            ->from('assignments as ass')
            ->join('teachers as t', 'ass.teacher_id', '=', 't.id')
            ->join('persons as p', 'p.id', '=', 't.person_id')
            ->join('practice_scenarios_areas as psa', 'ass.practice_scenarios_areas_id', '=', 'psa.id')
            ->join('practice_scenarios as ps', 'ps.id', '=', 'psa.practice_scenarios_id')
            ->join('areas as ar', 'ar.id', '=', 'psa.area_id')
            ->join('modules as m', 'm.id', '=', 'ass.module_id')
            ->join('subjects as su', 'su.id', '=', 'm.subject_id')
            ->join('groups_modules as gm', 'gm.module_id', '=', 'm.id')
            ->join('assign_student_groups as asg', 'asg.group_module_id', '=', 'gm.id')
            ->where('ps.id', '=', $escenario)->where('ass.academic_period_id', '=', $periodo_Academic)->get();
    }

    public function getStudentGroup($periodo_Academic)
    {
        return Student::select('*')
            ->from('students as s')->join('people as p', 'p.id', '=', 's.person_id')
            ->join('students_groups as sg', 's.id', '=', 'sg.student_id')
            ->join('groups_modules as gm', 'gm.id', '=', 'sg.group_module_id')
            ->join('assign_student_groups as asg', 'asg.group_module_id', '=', 'gm.id')
            ->join('assignments as ass', 'ass.id', '=', 'asg.assignment_id')
            ->where('ass.academic_period_id', '=', $periodo_Academic)->get();
    }

    /**
     * Reporte cuadro de rotación
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "Link de descarga" 
     * 
     */
    public function boxRotation(Request $request)
    {
        $career_id = intval($request->header('career'));
        $career = Career::find($career_id);
        $moduleId = $_GET['module'];
        $moduleName = Module::find($moduleId)->name;
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $rotations = [];
        $rota = Rotation::select('rotations.id', 'asg.assignment_id', 'rotations.start_date', 'rotations.end_date', 'modules.name as module')
            ->join('assign_student_groups as asg', 'rotations.id', 'asg.rotation_id')
            ->join('groups_modules as gm', 'asg.group_module_id', 'gm.id')
            ->join('modules', 'gm.module_id', 'modules.id')
            ->where('modules.id', $moduleId)
            ->groupBy('rotations.id')
            ->get();
        // $groups = GroupModule::where('academic_period_id', $academicPeriod->id)->where('module_id', $moduleId)->get();
        foreach ($rota as &$rotation) {
            $groups = GroupModule::select('groups_modules.*', 'asg.working_day')
                ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
                ->where('groups_modules.module_id', $moduleId)->where('asg.rotation_id', $rotation->id)->get();
            $groupStudents = [];
            if (!$groups->isEmpty()) {
                foreach ($groups as $index => $group) {
                    $studentsGroup = [];
                    $groupStudentClass = new stdClass();
                    $students = $group->students()->get();
                    foreach ($students as $student) {
                        $studentClass = new stdClass();
                        $studentClass->person = $student->person()->first(['first_name', 'second_name', 'first_last_name', 'second_last_name']);
                        array_push($studentsGroup, $studentClass);
                    }
                    $assignment = $group->assignment()->where('rotation_id', $rotation->id)->first();
                    $area_module = $assignment->area_module()->first();
                    $teacher = $assignment->teacher()->first();
                    $assignment->teacher = $teacher->person()->first(['first_name', 'second_name', 'first_last_name', 'second_last_name']);
                    $scenario_area = $area_module->scenario_area()->first();
                    $assignment->scenario = $scenario_area->scenario()->first(['name']);
                    $assignment->scenario->area = $scenario_area->area()->first(['name']);
                    $groupStudentClass->studentsGroup = $studentsGroup;
                    $groupStudentClass->assignment = $assignment;
                    $groupStudentClass->working_day = $group->working_day;
                    array_push($groupStudents, $groupStudentClass);
                }
            }
            $rotation->groups = $groupStudents;
        }
        $data = compact('rota', 'academicPeriod', 'career');
        if (count($data['rota']) == 0) {
            return null;
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('reports.tableRotation', $data);
        return $pdf->download('CuadroDeRotación' . $moduleName . '.pdf');
    }

    /**
     * Reporte cuadro de rotación
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "Link de descarga" 
     * 
     */
    public function boxRotationByScenario(Request $request)
    {
        $career_id = intval($request->header('career'));
        $career = Career::find($career_id);
        $scenarioId = $_GET['scenario'];
        $scenarioName = PracticeScenario::find($scenarioId)->name;
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $scenariosAreasAux = PracticeScenarioArea::where('practice_scenarios_id', $scenarioId)->get();
        $scenariosAreas = [];
        foreach ($scenariosAreasAux as &$scenarioArea) {
            $scenarioAreaModule = $scenarioArea->scenarios_area_module()->first();
            if ($scenarioAreaModule) {
                $assignment = $scenarioAreaModule->assignment()->first();
                $groupStudents = [];
                if ($assignment) {
                    $groups = GroupModule::select('groups_modules.*', 'asg.working_day')
                        ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
                        ->where('asg.assignment_id', $assignment->id)->get();
                    if (!$groups->isEmpty()) {
                        foreach ($groups as $index => $group) {
                            $studentsGroup = [];
                            $groupStudentClass = new stdClass();
                            $students = $group->students()->get();
                            foreach ($students as $student) {
                                $studentClass = new stdClass();
                                $studentClass->person = $student->person()->first(['first_name', 'second_name', 'first_last_name', 'second_last_name']);
                                array_push($studentsGroup, $studentClass);
                            }
                            $teacher = $assignment->teacher()->first();
                            $assignment->teacher = $teacher->person()->first(['first_name', 'second_name', 'first_last_name', 'second_last_name']);
                            $assignment->area = $scenarioArea->area()->first(['name']);
                            $groupStudentClass->studentsGroup = $studentsGroup;
                            $groupStudentClass->assignment = $assignment;
                            $groupStudentClass->working_day = $group->working_day;
                            array_push($groupStudents, $groupStudentClass);
                        }
                        $scenarioArea->groups = $groupStudents;
                        array_push($scenariosAreas, $scenarioArea);
                    }
                }
            }
        }
        $data = compact('scenariosAreas', 'academicPeriod', 'career', 'scenarioName');
        if (count($data['scenariosAreas']) == 0) {
            return null;
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('reports.tableRotation', $data);
        return $pdf->download('CuadroDeRotación' . $scenarioName . '.pdf');
    }

    /**
     * Reporte cuadro de rotación
     * 
     * report_meta : {"key": "String", "tabla": "String", "value": "String"} ;
     * 
     * report_datas : {"name": "String", "total": "int", "report_id": "int"}
     * 
     * @bodyParam report_metas Array<report_meta> required key del reporte
     * @bodyParam title String required Título del reporte
     * @bodyParam description String required Descripción del reporte
     * @bodyParam image File required Imagen del reporte
     * @bodyParam report_datas Array<report_datas> Datos del reporte
     * 
     * @response "Link de descarga" 
     * 
     */
    public function reportByScenario(Request $request)
    {
        $career_id = intval($request->header('career'));
        $career = Career::find($career_id);
        $report = new stdClass();
        $report_metas = json_decode($request->input('report_metas'));
        $report->metas = [];
        foreach ($report_metas as $reportMeta) {
            $report->metas[$reportMeta->key] = DB::table($reportMeta->tabla)->find(($reportMeta->value));
        }
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $report->areas = [];
        $scenarios = PracticeScenarioArea::where('practice_scenarios_id', $report->metas['scenario']->id)->get();
        foreach ($scenarios as $key => $scenario) {
            array_push($report->areas, $scenario->area()->first());
        }
        $students = count((new Student())->studentsCountByScenario($report->metas['scenario']->id));
        $attentions = 0;
        $patients = count((new Patient())->patientsCountByScenario($report->metas['scenario']->id));
        $attentions = (new Attention())->attentionCountByScenario($report->metas['scenario']->id);
        $data = compact('report', 'students', 'patients', 'attentions', 'career');
        $report->title = $request->input('title');
        $report->description = $request->input('description');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $tempDirectory = 'images/temporal/';
            $array = explode('.', $image->getClientOriginalName());
            $uid = uniqid();
            $image->move($tempDirectory, $array[0] . $uid . '.' . end($array));
            $filePath = $tempDirectory . $array[0] . $uid . '.' . end($array);
            $report->image = 'data:image/png;base64,' . base64_encode(file_get_contents($filePath));
            File::delete($filePath);
        } else {
            $report->image = $request->input('image');
        }
        $report->duration = Rotation::where('academic_period_id', $report->metas['academic_period']->id)->where('type', TypeRotation::FULL)->first();
        $report->report_datas = json_decode($request->input('report_datas'));
        $data = compact('report', 'students', 'patients', 'attentions', 'career');
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('reports.attentionByScenario', $data);
        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed' => TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );
        return $request->hasFile('image') ? 'data:application/pdf;base64,' . base64_encode($pdf->stream('atenciones_por_asignatura.pdf')) : $pdf->download($report->title . '.pdf');
    }
}

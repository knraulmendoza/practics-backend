<?php

namespace App\Http\Controllers;

use App\AcademicPeriod;
use App\Enums\StateAssistence;
use App\Http\Response;
use App\Assistence;
use App\Career;
use App\Module;
use App\PracticeScenario;
use App\Subject;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * @group  Asistencias
 *
 * endpoints para asistencia
 */
class AssistenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    public function assistencesBySubjectCount($subjectId, $academicPeriodId)
    {
        return Assistence::select('assistence.*')
            ->join('students', 'assistence.student_id', 'students.id')
            ->join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->join('modules', 'groups_modules.module_id', 'modules.id')
            ->where('modules.subject_id', $subjectId)
            ->where('assistence.assist', '!=', strval(StateAssistence::ASISTIO))
            ->where('assistence.academic_period_id', $academicPeriodId)->get();
    }

    /**
     * Listar asistencias por asignatura
     * 
     * @bodyParam academicPeriod int required Periodo academico
     * @bodyParam subject String required Código de la asignatura
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "subject": "Object<Subject>",
     *          "students": "int",
     *          "assistences": "int",
     *          "noAssistences": "int" 
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "date": null
     * }
     */
    public function assistencesBySubject(Request $request)
    {
        $career = intval($request->header('career'));
        try {
            $academicPeriod = AcademicPeriod::find($request->input('academicPeriod'));
            $subject = Subject::where('code', $request->input('subject'))->first();
            $modules = Module::where('subject_id', $subject->id)->get();
            $assistenceAux = $this->assistencesBySubjectCount($subject->id, $academicPeriod->id);
            $students = 0;
            $assistenceData = [];
            $countAssistenceAux = count($assistenceAux);
            if ($countAssistenceAux > 0) {
                $noAssistences = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                    return $assistence['assist'] == strval(StateAssistence::NO_ASISTIO);
                }));
                array_push($assistenceData, ['total' => $noAssistences, 'name' => 'Rechazadas', 'percentage' => ($noAssistences / $countAssistenceAux) * 100]);
                $noAssistencesExcusa = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                    return $assistence['assist'] == strval(StateAssistence::NO_ASISTIO_EXCUSA);
                }));
                array_push($assistenceData, ['total' => $noAssistencesExcusa, 'name' => 'Otras excusas', 'percentage' => ($noAssistencesExcusa / $countAssistenceAux) * 100]);
                $covidNeg = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                    return $assistence['assist'] == strval(StateAssistence::COVID_NEG);
                }));
                array_push($assistenceData, ['total' => $covidNeg, 'name' => 'Covid negativo', 'percentage' => ($covidNeg / $countAssistenceAux) * 100]);
                $covidPos = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                    return $assistence['assist'] == strval(StateAssistence::COVID_POS);
                }));
                array_push($assistenceData, ['total' => $covidPos, 'name' => 'Covid positivo', 'percentage' => ($covidPos / $countAssistenceAux) * 100]);
            }
            return $this->response->ok(compact('subject', 'assistenceData'), $countAssistenceAux > 0);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar asistencias por asignatura
     * 
     * @bodyParam academicPeriod int required Periodo academico
     * @bodyParam subject String required Código de la asignatura
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "subject": "Object<Subject>",
     *          "students": "int",
     *          "assistences": "int",
     *          "noAssistences": "int" 
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "date": null
     * }
     */
    public function assistencesByCareer(Request $request)
    {
        $careerId = intval($request->header('career'));
        try {
            $academicPeriod = AcademicPeriod::find($request->input('academicPeriod'));
            $subject = Career::find($careerId);
            $subjects = $subject->subjects()->get();
            $assistenceData = [];
            $totalAssistence = 0;
            foreach ($subjects as &$subjectAux) {
                $assistences = $this->assistencesBySubjectCount($subjectAux->id, $academicPeriod->id);
                array_push($assistenceData, ['total' => count($assistences), 'name' => $subjectAux->name]);
                $totalAssistence += count($assistences);
            }
            foreach ($assistenceData as &$assistence) {
                $assistence['percentage'] = intval(($assistence['total'] / $totalAssistence) * 100);
            }
            return $this->response->ok(compact('subject', 'assistenceData'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar asistencias por asignatura
     * 
     * @bodyParam academicPeriod int required Periodo academico
     * @bodyParam subject String required Código de la asignatura
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "subject": "Object<Subject>",
     *          "students": "int",
     *          "assistences": "int",
     *          "noAssistences": "int" 
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "date": null
     * }
     */
    public function assistencesByScenario(Request $request)
    {
        $career = intval($request->header('career'));
        try {
            $academicPeriod = AcademicPeriod::find($request->input('academicPeriod'));
            $scenario = PracticeScenario::find($request->input('scenario'));
            $assistenceAux = Assistence::select('assistence.*')
                ->join('students', 'assistence.student_id', 'students.id')
                ->join('students_groups', 'students.id', 'students_groups.student_id')
                ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
                ->join('assign_student_groups as asg', 'groups_modules.id', 'asg.group_module_id')
                ->join('assignments', 'asg.assignment_id', 'assignments.id')
                ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.module_id')
                ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
                ->where('psa.practice_scenarios_id', $scenario->id)
                ->where('psa.area_id', $request->input('area'))
                ->where('assistence.academic_period_id', $academicPeriod->id)->get();
            $students = 0;
            $assistences = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                return $assistence['assist'] == strval(StateAssistence::ASISTIO);
            }));
            $noAssistences = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                return $assistence['assist'] == strval(StateAssistence::NO_ASISTIO);
            }));
            $noAssistencesExcusa = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                return $assistence['assist'] == strval(StateAssistence::NO_ASISTIO_EXCUSA);
            }));
            $covidNeg = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                return $assistence['assist'] == strval(StateAssistence::COVID_NEG);
            }));
            $covidPos = count(array_filter($assistenceAux->toArray(), function ($assistence) {
                return $assistence['assist'] == strval(StateAssistence::COVID_POS);
            }));
            return compact('assistences', 'noAssistences', 'noAssistencesExcusa', 'covidNeg', 'covidPos');
            // foreach ($modules as $module) {
            //     $groups = $module->groups($academicPeriod->id)->get();
            //     foreach ($groups as $group) {
            //         $studentsAux = $group->students()->get();
            //         $students += count($studentsAux);
            //         foreach ($studentsAux as $student) {
            //             $noAssistences += $student->inassistences($academicPeriod->id)->where('assist', '!=', StateAssistence::ASISTIO)->count();
            //             $assistences += $student->assistences($academicPeriod->id)->count();
            //         }
            //     }
            // }
            return $this->response->ok(compact('subject', 'students', 'assistences', 'noAssistences'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar una asistencia
     * 
     * @bodyParam student_id int required Id del estudiante a tomar asistencia
     * @bodyParam observation String optional Observación de la asistencia
     * @bodyParam academic_period_id int required Id del periodo academico
     * @bodyParam date Date optional Fecha de la asistencia
     * @bodyParam assist int required Asistencia del estudiante
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "assistence": {
     *               "student_id": "int",
     *               "observation": "String",
     *               "academic_period_id": "int",
     *               "date": "Date",
     *               "assist": "int"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "date": null
     * }
     */
    public function store(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $resp = $this->validar(date('Y-m-d'), $request->input('student_id'));
            if ($resp) {
                return $this->response->badRequest("Ya se registro está asistencia para este estudiante");
            }
            $assistence = new Assistence($request->all());
            $assistence->academic_period_id = $academicPeriod->academic_period_id;
            $assistence->date = date('Y-m-d');
            $assistence->save();
            DB::commit();
            return $this->response->ok(compact('assistence'));
        } catch (QueryException $ex) {
            DB::rollBack();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Registrar varias asistencias
     * 
     * 
     * @bodyParam assistences Array required Lista a registrar asistencia
     * @bodyParam assistences.*.student_id int required Id del estudiante a tomar asistencia
     * @bodyParam assistences.*.observation String optional Observación de la asistencia
     * @bodyParam assistences.*.academic_period_id int required Id del periodo academico
     * @bodyParam assistences.*.date Date optional Fecha de la asistencia
     * @bodyParam assistences.*.assist int required Asistencia del estudiante
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "assistences": [{
     *               "student_id": "int",
     *               "observation": "String",
     *               "academic_period_id": "int",
     *               "date": "Date",
     *               "assist": "int"
     *          }]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "date": null
     * }
     */
    public function manyAssistences(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $assistences = $request->input('assistences');
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            foreach ($assistences as &$assistence) {
                $assistenceClass = new Assistence($assistence);
                $resp = $this->validar(date('Y-m-d'), $assistenceClass->student_id);
                if ($resp) {
                    $assistenceClass->state = StateAssistence::ASISTIO;
                } else {
                    $assistenceClass->academic_period_id = $academicPeriod->academic_period_id;
                    $assistenceClass->date = date('Y-m-d');
                    $assistenceClass->save();
                }
            }
            DB::commit();
            return $this->response->ok(compact('assistences'));
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    public function validar($date, $student)
    {
        $consulta = Assistence::where('date', '=', $date)->where('student_id', '=', $student)->first();
        return $consulta ? true : false;
    }
}

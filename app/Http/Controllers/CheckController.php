<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Attention;
use App\Check;
use App\CheckRotation;
use App\Enums\Roles;
use App\Enums\StateAttention;
use App\Http\Response;
use App\Module;
use App\PatientPoll;
use App\Person;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  Chequeos 
 *
 */
class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $response;

    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2;3;5', ['only' => ['index', 'store', 'update']]);
    }

    /**
     * Lista de chequeos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "checks": [
     *              {
     *                  "id": "int",
     *                  "date": "Date()",
     *                  "evaluated": "Object<Person>",
     *                  "evaluator": "Object<Person>",
     *                  "academic_period_id": "int",
     *                  "state": "int",
     *                  "observation": "String",
     *                  "checks": "int"
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function index(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $checks = Check::where('career_id', $career_id)->where('academic_period_id', $academicPeriod->academic_period_id)->groupBy('evaluator')->get();
            foreach ($checks as &$check) {
                $check->evaluated = $check->evaluated()->first();
                $check->evaluator = $check->evaluator()->first();
                $check->observation = $check->observations()->first()->observation;
                $check->checks = Check::where('career_id', $career_id)->where('academic_period_id', $academicPeriod->academic_period_id)->where('evaluator', $check->evaluator->id)->count();
            }
            return $this->response->ok(compact('checks'), $checks->isEmpty() ? false : true);
        } catch (Exception $e) {
            return $this->response->badRequest(($e->getMessage()));
        }
    }

    /**
     * Lista de chequeos paginado
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "checks": [
     *              {
     *                  "id": "int",
     *                  "date": "Date()",
     *                  "evaluated": "Object<Person> con Object<User> y Object<Rol>",
     *                  "evaluator": "Object<Person> con Object<User> y Object<Rol>",
     *                  "academic_period_id": "int",
     *                  "state": "int",
     *                  "observation": "String",
     *                  "checks": "int"
     *              }
     *          ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function paginate(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $per_page = 8;
            $checks = Check::where('career_id', $career_id)->groupBy('evaluated')->paginate($per_page);
            $total = $checks->total();
            $checks = $checks->items();
            foreach ($checks as &$check) {
                $check->evaluated = $check->evaluated()->first();
                $check->evaluated->load('user');
                $check->evaluated->user->load('roles');
                $check->evaluator = $check->evaluator()->first();
                $check->evaluator->load('user');
                $check->evaluator->user->load('roles');
                $check->observation = $check->observations()->first()->observation;
                $check->checks = Check::where('career_id', $career_id)->where('academic_period_id', $academicPeriod->academic_period_id)->where('evaluator', $check->evaluator->id)->count();
            }
            return $this->response->ok(compact('checks', 'per_page', 'total'), count($checks) == 0 ? false : true);
        } catch (Exception $e) {
            return $this->response->badRequest(($e->getMessage()));
        }
    }

    /**
     * Guardar encuesta
     * 
     * @authenticated
     * 
     * @bodyParam attention_id int required Id de la atención
     * @bodyParam date Date required Fecha en el cual se hizo la encuesta
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "patient_poll": {"id": "int", "date": "String", "evaluated": "int", "attention_id": "int"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function saveEncuesta(Request $request)
    {
        $career_id = $request->header('career');
        $rol = $request->header('rol');
        try {
            DB::beginTransaction();
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->response->unauthorized();
            } else {
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $poll = PatientPoll::where('attention_id', $request->input('attention_id'))->first();
                if ($poll) {
                    return $this->response->ok(compact('poll'));
                }
                $attention = Attention::find($request->input('attention_id'));
                $person = $user->person()->first();
                $patient_poll = new PatientPoll();
                $patient_poll->date = $request->input('date');
                $patient_poll->evaluated = $person->id;
                $patient_poll->attention_id = $attention->id;
                $patient_poll->save();
                $attention->state = StateAttention::EVALUATED;
                $attention->save();
                DB::commit();
                return $this->response->ok(compact('patient_poll'));
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * 
     * Guardar chequeo
     * 
     * @authenticated
     * 
     * @bodyParam evaluated int required Id de la atención
     * @bodyParam date Date required Fecha en el cual se hizo la encuesta
     * @bodyParam observation String Observación del chequeo
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "patient_poll": {"id": "int", "date": "String", "evaluated": "int", "evaluator": "int", "academic_period_id": "int", "career_id": "int"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        $action = $request->input('action');
        $career_id = $request->header('career');
        $evaluator = null;
        $evaluated = null;
        try {
            DB::beginTransaction();

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->response->unauthorized();
            } else {
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $person = $user->person()->first();
                $check = $person->checkByEvaluator($request->input('evaluated'));
                $answers = [];
                if ($check) {
                    $answers = $check->answers()->get();
                }
                if (empty($answers)) {
                    $personAux = Person::find($request->input('evaluated'));
                    switch ($action) {
                        case 1:
                            if ($personAux->student()->first()) {
                                $evaluated = $personAux->id;
                            } else {
                                return $this->response->badRequest('Este usuario no es estudiante');
                            }
                            break;
                        case 2:
                            if ($personAux->teacher()->first()) {
                                $evaluated = $personAux->id;
                            } else {
                                return $this->response->badRequest('Este usuario no es docente');
                            }
                            break;
                        default:
                            return $this->response->badRequest();
                            break;
                    }
                }
                if ($evaluated) {
                    $check = new Check();
                    $check->date = $request->input('date');
                    $check->evaluated = $evaluated;
                    $check->evaluator = $person->id;
                    $check->academic_period_id = $academicPeriod->academic_period_id;
                    $check->career_id = $career_id;
                    $check->save();
                    $check->observations()->create(['observation' => $request->input('observation')]);
                    $checkRotation = new CheckRotation();
                    $checkRotation->check_id = $check->id;
                    $checkRotation->rotation_id = (new RotationController())->getRotation($career_id)->id;
                    $checkRotation->save();
                    DB::commit();
                    return $this->response->ok(compact('check'));
                } else {
                    return $this->response->badRequest('Esta persona no existe');
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Buscar chequeo
     * 
     * @authenticated
     * 
     * @bodyParam type String required Tipo de busqueda  
     * @bodyParam value String required Valor a buscar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "checks": [
     *              {
     *                  "id": "int",
     *                  "date": "Date()",
     *                  "evaluated": "Object<Person> con Object<User> y Object<Rol>",
     *                  "evaluator": "Object<Person> con Object<User> y Object<Rol>",
     *                  "academic_period_id": "int",
     *                  "state": "int",
     *                  "observation": "String",
     *                  "checks": "int"
     *              }
     *          ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * 
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function search(Request $request)
    {
        try {
            $career_id = $request->header('career');
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $search = 'checks.' . $request->input('type');
            $name = explode(' ', $request->input('value'));
            $full_name = '';
            foreach ($name as $nameAux) {
                $full_name = $full_name . '+' . $nameAux . '-';
            }
            $full_name = str_replace('-', ' ', $full_name);
            $query = "MATCH(people.first_name, people.second_name, people.first_last_name, people.second_last_name) AGAINST('$full_name' IN BOOLEAN MODE)";
            $per_page = 8;
            $checks = Check::select('checks.*')->join('people', $search, 'people.id')->whereRaw($query)->groupBy('evaluator')->paginate(8);
            foreach ($checks as &$check) {
                $check->evaluated = $check->evaluated()->first();
                $check->evaluated->load('user');
                $check->evaluated->user->load('roles');
                $check->evaluator = $check->evaluator()->first();
                $check->evaluator->load('user');
                $check->evaluator->user->load('roles');
                $check->observation = $check->observations()->first()->observation;
                $check->checks = Check::where('career_id', $career_id)->where('academic_period_id', $academicPeriod->academic_period_id)->where('evaluator', $check->evaluator->id)->count();
            }
            $total = $checks->total();
            $checks = $checks->items();
            return $this->response->ok(compact('checks', 'per_page', 'total'), count($checks) == 0 ? false : true);
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     *  Listar chequeos por rotación
     * 
     * @authenticated
     * 
     * @bodyParam evaluated int required Id de la persona a consultar
     * @bodyParam rotation int required Id de la rotación el cual se quiere consultar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [
     *              {
     *                  "id": "int",
     *                  "name": "String",
     *                  "answers": "Array<Answer>"
     *              }
     *          ],
     *          "checksObservation": "Array<String>"
     *      }
     * 
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function checksByRotation(Request $request)
    {
        $rol = $request->header('rol');
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = Person::find($request->input('evaluated'));
                if ($person) {
                    $career = $person->careers()->where('id', $career_id)->first();
                    $rol = $person->user()->first()->hasRole(strval(Roles::STUDENT)) ? 'students' : 'teachers';
                    $questions = $career->questions()->where('check_type', 'like', '%' . $rol . '%')->get();
                    $checksObservation = [];
                    $checksAux = $person->checksByEvaluator($person->id);
                    foreach ($checksAux as &$check) {
                        $checkRotation = $check->checksRotation($request->input('rotation'))->first();
                        if ($checkRotation) {
                            $observationCheck = $check->observations()->first()->observation;
                            array_push($checksObservation, $observationCheck);
                        }
                    }
                    foreach ($questions as &$question) {
                        $checks = $person->countCheckByEvaluated($person->id, $career_id, $request->input('rotation'))->get();
                        $answers = [];
                        foreach ($checks as &$check) {
                            $checkClass = Check::find($check->id);
                            $answer = $checkClass->answers->where('question_id', $question->id)->first();
                            if ($answer) {
                                $answer->observation = $answer->observation()->first()->observation;
                            }
                            array_push($answers, $answer);
                        }
                        $question->answers = $answers;
                    }
                    if (count($questions[0]->answers) == 0) {
                        $questions = [];
                    }
                    return $this->response->ok(compact('questions', 'checksObservation'), count($questions) == 0 ? false : true);
                }
                return $this->response->badRequest();
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     *  Listar chequeos por estudiante
     * 
     * @authenticated
     * 
     * @bodyParam module int required Id del módulo
     * @bodyParam academic_period int required Id del periodo academico
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "data": [{"id": "int", "person_id": "int", "person": "Object<Person>"}],
     *          "module": "Object<Module>"
     *      }
     * 
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function checksByStudent(Request $request)
    {
        try {
            $module = Module::find($request->input('module'));
            $groups = $module->groups($request->input('academic_period'))->with('students')->get();
            $students = [];
            foreach ($groups as &$group) {
                $rotations = $group->rotations()->get();
                $studentsAux = $group->students;
                foreach ($studentsAux as &$student) {
                    $student->load('person');
                    $student->person->fullName = $student->person->first_name . ' ' .
                        ($student->person->second_name ? $student->person->second_name . ' ' : '') . $student->person->first_last_name . ' ' . $student->person->second_last_name;

                    $checks = $student->person->checkByEvaluatedAll($request->input('academic_period'));
                    $rotAux = [];
                    foreach ($rotations as &$rotation) {
                        $rotaAux = new stdClass();
                        $rotaAux->checks = [];
                        foreach ($checks as &$check) {
                            $chAux = $check->checksRotations($rotation->id)->first();
                            if ($chAux) {
                                $check->observation = $check->observations()->first()->observation;
                                $check->yes = $check->answers()->where('answer', 1)->count();
                                $check->no = $check->answers()->where('answer', 0)->count();
                                array_push($rotaAux->checks, $check);
                            }
                        }
                        array_push($rotAux, $rotaAux);
                    }
                    $student->rotations = $rotAux;
                    array_push($students, $student);
                }
            }
            $data = $students;
            return $this->response->ok(compact('data', 'module'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     *  Listar chequeos por profesor
     * 
     * @authenticated
     * 
     * @bodyParam module int required Id del módulo
     * @bodyParam academic_period int required Id del periodo academico
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "data": [{"id": "int", "person_id": "int", "person": "Object<Person>"}],
     *          "module": "Object<Module>"
     *      }
     * 
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function checksByTeacher(Request $request)
    {
        try {
            $career_id = intval($request->header('career'));
            $module = Module::find($request->input('module'));
            $assignments = Assignment::select('assignments.*')
                ->join('assign_student_groups as asg', 'assignments.id', 'asg.assignment_id')
                ->join('groups_modules', 'asg.group_module_id', 'groups_modules.id')
                ->groupBy('assignments.id')
                ->where('groups_modules.module_id', $request->input('module'))->get();
            $teachers = [];
            foreach ($assignments as &$assignment) {
                $teacher = $assignment->teacher()->with('person')->first();
                $teacher->person->fullName = $teacher->person->first_name . ' ' .
                    ($teacher->person->second_name ? $teacher->person->second_name . ' ' : '') . $teacher->person->first_last_name . ' ' . $teacher->person->second_last_name;
                $rotations = $assignment->rotations()->get();
                $checks = $teacher->person->checkByEvaluatedAll($request->input('academic_period'));
                $rotAux = [];
                foreach ($rotations as &$rotation) {
                    $rotaAux = new stdClass();
                    $rotaAux->checks = [];
                    foreach ($checks as &$check) {
                        $chAux = $check->checksRotations($rotation->id)->first();
                        if ($chAux) {
                            $check->observation = $check->observations()->first()->observation;
                            $check->yes = $check->answers()->where('answer', 1)->count();
                            $check->no = $check->answers()->where('answer', 0)->count();
                            array_push($rotaAux->checks, $check);
                        }
                    }
                    array_push($rotAux, $rotaAux);
                }
                $teacher->rotations = $rotAux;
                array_push($teachers, $teacher);
            }
            $data = $teachers;
            return $this->response->ok(compact('data', 'module'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     *  Lista de preguntas por el id chequeo
     * 
     * @authenticated
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [{"id": "int", "name": "String", "answers": "Array<Answer>"}],
     *          "checks": [{"id": "int", "state": "String"}]
     *      }
     * 
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function checksById(Request $request, int $personId = null)
    {
        $rol = $request->header('rol');
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->response->unauthorized('user_not_found');
            } else {
                $person = null;
                if ($personId) {
                    $person = Person::find($personId);
                }
                if ($person) {
                    $checks = [];
                    $rol = $person->user()->first()->hasRole(strval(Roles::STUDENT)) ? 'students' : 'teachers';
                    if ($person->user()->first()->hasRole(strval(Roles::STUDENT))) {
                        $student = $person->student()->first();
                        $group = $student->groups($academicPeriod->academic_period_id)->first();
                        $rotation = $group->rotation()->first();
                    } else {
                        $teacher = $person->teacher()->first();
                        $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
                        $rotation = $assignment->rotation()->first();
                    }
                    $career = $person->careers()->where('id', $career_id)->first();
                    $questions = $career->questions()->where('check_type', 'like', '%' . $rol . '%')->get();
                    foreach ($questions as &$question) {
                        $checks = $person->countCheckByEvaluated($person->id, $career_id, $rotation->id)->get();
                        $answers = [];
                        foreach ($checks as &$check) {
                            $checkClass = Check::find($check->id);
                            $answer = $checkClass->answers()->where('question_id', $question->id)->first();
                            if ($answer) {
                                $answer->observation = $answer->observation()->first()->observation;
                            }
                            array_push($answers, $answer);
                        }
                        $question->answers = $answers;
                    }
                    return $this->response->ok(compact('questions', 'checks'));
                }
                return $this->response->badRequest();
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    public function activeCheck(Request $request)
    {
        try {
            $check = Check::find($request->input('check.id'));
            $check->state = $request->input('check.state');
            $check->save();
            return $this->response->ok(compact('check'));
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return $this->response->badRequest();
        }
    }
}

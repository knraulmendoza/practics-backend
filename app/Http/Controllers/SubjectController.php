<?php

namespace App\Http\Controllers;

use App\CareersSubjects;
use App\Http\Requests\ValidateSubjectRequest;
use App\Http\Response;
use App\Module;
use App\Subject;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Asignaturas
 */
class SubjectController extends Controller
{
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }
    /**
     * Registrar asignatura
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre de la asignatura
     * @bodyParam code String required Código de la asignatura
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"subject": {"id": "int", "name": "String", "code": "String"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function store(ValidateSubjectRequest $request)
    {
        $career = intval($request->header('career'));
        try {
            DB::beginTransaction();
            $subject = new Subject($request->all());
            $subject->save();
            $Careerssubjects = new CareersSubjects();
            $Careerssubjects->career_id = $career;
            $Careerssubjects->subject_id = $subject->id;
            $Careerssubjects->save();
            DB::commit();
            return $this->response->ok(compact('subject'));
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
        return $this->response->unauthorized('ocurrio un error');
    }
    /**
     * 
     * Listar módulos por asignatura
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"modules": [{"id": "int", "module": "String", "code": "String", "description": "String", "subject": "String", "subject_id": "int"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function modules(string $subject)
    {
        try {
            $modules = Subject::select('modules.id', 'modules.code', 'modules.name as module', 'modules.description', 'subjects.name as subject', 'subjects.id as subject_id')
                ->join('careers_subjects', 'careers_subjects.subject_id', '=', 'subjects.id')
                ->join('careers', 'careers_subjects.career_id', '=', 'careers.id')
                ->join('modules', function ($join) {
                    $join->on('modules.career_id', '=', 'careers.id')->on('modules.subject_id', '=', 'subjects.id');
                })
                ->where('subjects.code', '=', $subject)->get();
            return $this->response->ok(compact('modules'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Listar módulos por asignatura y area
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "modulesWithoutArea": [{"id": "int", "name": "String", "code": "String", "description": "String", "career_id": "int", "subject_id": "int"}],
     *          "modulesWithArea": [{"id": "int", "name": "String", "code": "String", "description": "String", "career_id": "int", "subject_id": "int"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function modulesArea(Request $request, string $subject, int $area, int $practice_scenario_id)
    {
        try {
            $career = intval($request->header('career'));
            $subject = Subject::where('code', $subject)->first();
            $modules = Module::where('career_id', $career)->where('subject_id', $subject->id)->get();
            $areas = [];
            $modulesWithoutArea = [];
            $modulesWithArea = [];
            foreach ($modules as &$module) {
                $areas = [];
                $scenariosModule = $module->practicScenarioAreaModule()->get();
                foreach ($scenariosModule as &$scenarioModule) {
                    $scenario_area = $scenarioModule->scenario_area()->where('area_id', $area)->where('practice_scenarios_id', $practice_scenario_id)->first();
                    if ($scenario_area) {
                        array_push($areas, $scenario_area);
                    }
                }
                if (count($areas) > 0) {
                    array_push($modulesWithArea, $module);
                } else {
                    array_push($modulesWithoutArea, $module);
                }
            }
            return $this->response->ok(compact('modulesWithoutArea', 'modulesWithArea'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar asignatura
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre de la asignatura
     * @bodyParam code String required Código de la asignatura
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"subject": {"id": "int", "name": "String", "code": "String"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function update(Request $request, String $code)
    {
        try {
            DB::beginTransaction();
            $subject = Subject::where('code', $code)->first();
            if (!$subject) {
                return $this->response->badRequest('No se encontró la asignatura');
            }
            $subject->update($request->all());
            DB::commit();
            return $this->response->ok(compact('subject'));
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

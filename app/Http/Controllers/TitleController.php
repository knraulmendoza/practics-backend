<?php

namespace App\Http\Controllers;

use App\Http\Response;
use App\TeacherTitle;
use App\Title;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group títulos academicos
 */
class TitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $response;
    public $name_table = "title";
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Listar títulos academicos
     * 
     * El estado puede cambiar a falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"titles": [{"name": "String", "description": "String"}]}
     * }
     */
    public function index()
    {
        $titles = Title::all();
        return $this->response->ok(compact('titles'), count($titles) == 0 ? false : true);
    }

    /**
     * Registrar un titulo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre del título
     * @bodyParam description String Descripción del título
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"title": {"id": "int", "name": "String", "description": "String"}}
     * }
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $title = new Title($request->all());
            $title->save();
            DB::commit();
            return $this->response->ok(compact('title'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }


    /**
     * Asignar título a profesor
     * 
     * @bodyParam titles Array<int> required Lista de títulos
     * @bodyParam teacher_id int Descripción del título
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"request": {"titles": "Array<int>", "teacher_id": "int"}}
     * }
     */
    public function asignerTitle(Request $request)
    {
        try {
            DB::beginTransaction();
            for ($i = 0; $i < count($request->input("titles")); $i++) {
                $teacher_title = new TeacherTitle();
                $teacher_title->teacher_id = $request->input('teacher_id');
                $teacher_title->title_id = $request->input('titles')[$i];
                $teacher_title->save();
            }
            DB::commit();
            return $this->response->ok(compact('request'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Enums\StatePerson;
use App\Enums\StateUser;
use App\Http\Response;
use App\RolesUsers;
use App\Teacher;
use App\TeacherTitle;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Profesor
 */
class TeacherController extends Controller
{
    public $table = 'teacher';
    public $response;

    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2', ['only' => ['updateTeacher']]);
    }

    /**
     * 
     * Obtener profesor por el documento
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"teacher": {"id": "int", "person": "Object<Person>", "checksByRotation": "int", "checks": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function teacherByDocument(Request $request, String $document)
    {
        $career_id = intval($request->header('career'));
        try {
            $teacher = Teacher::select('teachers.*')
                ->join('people', 'teachers.person_id', 'people.id')
                ->where('document', $document)->with('person')->first();
            $rotation = (new RotationController())->getRotation($career_id);
            $teacher->checksByRotation = count($teacher->person->countCheckByEvaluated($teacher->person->id, $career_id, $rotation->id)->get());
            $teacher->checks = count($teacher->person->countCheckByEvaluated($teacher->person->id, $career_id)->get());
            return $this->response->ok(compact('teacher'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    public function getTitles($id)
    {
        try {
            $titles = Teacher::find($id)->titles()->get();
            return $this->response->ok(compact('titles'));
        } catch (Exception $ex) {
            return $this->response->badRequest('Ocurrio un error en la consulta');
        }
    }

    /**
     * 
     * Listar profesor por escenario
     * 
     * El estado puede cambiar a falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"teacher": {"id": "int", "person": "Object<Person>", "title": "Array<String>", "types": "Array<int>"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function teachersByScenario(int $scenrioId)
    {
        try {
            $teachers = Teacher::select('teachers.*')
                ->join('assignments', 'teachers.id', 'assignments.teacher_id')
                ->join('assign_student_groups as asg', 'assignments.id', 'asg.assignment_id')
                ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
                ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
                ->where('psa.practice_scenarios_id', $scenrioId)->groupBy('teachers.id')->with('person', 'titles')->get();
            foreach ($teachers as &$teacher) {
                $roles = $teacher->person->user()->first()->roles()->get()->toArray();
                $teacher->types = array_map(function ($rol) {
                    return $rol['id'];
                }, $roles);
            }
            return $this->response->ok(compact('teachers'), count($teachers) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Listar profesor
     * 
     * El estado puede cambiar a falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"teachers": [{"id": "int", "person": "Object<Person>", "title": "Array<String>", "types": "Array<int>"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function getPeople(Request $request)
    {
        $career = intval($request->header('career'));
        try {
            $teachers = Teacher::select('teachers.*')
                ->join('people as p', 'teachers.person_id', '=', 'p.id')
                ->join('people_careers as pc', 'p.id', '=', 'pc.person_id')
                ->join('careers as c', 'pc.career_id', '=', 'c.id')
                ->Where('c.id', '=', $career)->get();
            foreach ($teachers as &$teacher) {
                $teacher->with('person', 'titles');
                $roles = $teacher->person->user()->first()->roles()->get()->toArray();
                $teacher->types = array_map(function ($rol) {
                    return $rol['id'];
                }, $roles);
            }
            return $this->response->ok(compact('teachers'), count($teachers) > 0 ? true : false);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * 
     * Listar profesor paginado
     * 
     * El estado puede cambiar a falso si no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "teachers": [{"id": "int", "person": "Object<Person>", "title": "Array<String>", "types": "Array<int>"}],
     *          "total": "int",
     *          "per_page": "int",
     *          "career": "Object<Career>"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function getPeoplePaginate(Request $request)
    {
        $career = intval($request->header('career'));
        try {
            $per_page = 8;
            $teachers = Teacher::select('teachers.*')
                ->join('people as p', 'teachers.person_id', '=', 'p.id')
                ->join('people_careers as pc', 'p.id', '=', 'pc.person_id')
                ->join('careers as c', 'pc.career_id', '=', 'c.id')
                ->Where('c.id', '=', $career)->paginate($per_page);
            foreach ($teachers as &$teacher) {
                $teacher->with('person', 'titles');
                $roles = $teacher->person->user()->first()->roles()->get()->toArray();
                $teacher->types = array_map(function ($rol) {
                    return $rol['id'];
                }, $roles);
            }
            $total = 0;
            if (count($teachers)  > 0) {
                $total = $teachers->total();
            }
            $teachers = $teachers->items();
            return $this->response->ok(compact('teachers', 'total', 'per_page', 'career'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar profesor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam person.phone String required Número telefonico del profesor
     * @bodyParam person.address String required Dirección del profesor
     * @bodyParam state int required Estado del profesor
     * @bodyParam types Array<int> required Tipos de profesores
     * @bodyParam titles_id Array<int> required Títulos universitarios
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"teacher": {"id": "int", "person": "Object<Person>", "title": "Array<String>", "types": "Array<int>"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $teacher = Teacher::find($id);
            if ($teacher) {
                $person = $teacher->person()->first();
                $person->phone = $request->input('person.phone');
                $person->address = $request->input('person.address');
                $person->update();
                $user = $person->user()->first();
                if ($teacher->state != $request->input('state')) {
                    $user->state = $request->input('state') == StatePerson::INACTIVO ? StateUser::INACTIVO : StateUser::ACTIVO;
                    $user->update();
                }
                if (count($user->roles()->get()) == 0) {
                    for ($i = 0; $i < count($request->input('types')); $i++) {
                        $user_role = new RolesUsers();
                        $user_role->user_id = $user->id;
                        $user_role->role_id = $request->input('types')[$i];
                        $user_role->save();
                    }
                } else {
                    if (count($user->roles()->get()) != count($request->input('types'))) {
                        if (count($user->roles()->get()) < count($request->input('types'))) {
                            for ($i = 0; $i < count($request->input('types')); $i++) {
                                if ($user->roles()->get()[0]->id != $request->input('types')[$i]) {
                                    $user_role = new RolesUsers();
                                    $user_role->user_id = $user->id;
                                    $user_role->role_id = $request->input('types')[$i];
                                    $user_role->save();
                                }
                            }
                        } else {
                            if (count($request->input('types')) == 0) {
                                for ($i = 0; $i < count($user->roles()->get()); $i++) {
                                    RolesUsers::where('user_id', '=', $user->id)->where('role_id', '=', $user->roles()->get()[$i]->id)->update(['state' => "0"]);
                                }
                            } else {
                                for ($i = 0; $i < count($user->roles()->get()); $i++) {
                                    if ($request->input('types')[0] != $user->roles()->get()[$i]->id) {
                                        RolesUsers::where('user_id', '=', $user->id)->where('role_id', '=', $user->roles()->get()[$i]->id)->update(['state' => "0"]);
                                    } else {
                                        RolesUsers::where('user_id', '=', $user->id)->where('role_id', '=', $user->roles()->get()[$i]->id)->update(['state' => "1"]);
                                    }
                                }
                            }
                        }
                    } else {
                        for ($i = 0; $i < count($user->roles()->get()); $i++) {
                            RolesUsers::where('user_id', '=', $user->id)->where('role_id', '=', $user->roles()->get()[$i]->id)->update(['state' => "1"]);
                        }
                    }
                }
                $teacher->state = strval($request->input('state'));
                $titles = $teacher->titles()->get();
                TeacherTitle::where('teacher_id', $teacher->id)->delete();
                foreach ($request->input('titles_id') as $titleId) {
                    $teacherTitle = new TeacherTitle();
                    $teacherTitle->teacher_id = $teacher->id;
                    $teacherTitle->title_id = $titleId;
                    $teacherTitle->save();
                }
                $teacher->update();
                DB::commit();
                return $this->response->ok(compact('teacher'));
            }
            return $this->response->badRequest('Este profesor no existe');
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Eliminar profesor
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"teacher": {"id": "int", "person": "Object<Person>", "title": "Array<String>", "types": "Array<int>"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $docente = Teacher::delete($id);
            DB::commit();
            return $this->response->ok(compact('docente'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Area;
use App\Assignment;
use App\Http\Response;
use App\PracticeScenarioArea;
use App\PracticeScenarioAreaModule;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * @group  Asignaciones
 *
 * endpoints para asignaciones
 */
class AssignmentController extends Controller
{
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    public function assignment($assignment)
    {
        $assignmentClass = new stdClass();
        $assignmentClass->id = $assignment->id;
        $area_module = $assignment->area_module()->first();
        $assignmentClass->module = $area_module->module()->first();
        $assignmentClass->module->subject_code = $assignmentClass->module->subjects()->first()->code;
        $assignmentClass->teacher = $assignment->teacher()->first();
        $assignmentClass->teacher->person = $assignment->teacher->person()->first();
        $scenario_area = $area_module->scenario_area()->first();
        $assignmentClass->scenario = $scenario_area->scenario()->first();
        $assignmentClass->scenario_id = $assignmentClass->scenario->id;
        $assignmentClass->scenario->areas = [Area::find($scenario_area->area_id)];
        return $assignmentClass;
    }

    /**
     * Asignaciones por módulos
     * @urlParam module_id int required Módulo del id a asignar
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "assignments": [
     *              {
     *                  "id": "int",
     *                  "module": "string",
     *                  "teacher": {
     *                        "person": {}
     *                   },
     *                  "escenario": {
     *                       "areas": []
     *                  }
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */
    public function assignmentByModule(Request $request, int $module_id)
    {
        $career_id = intval($request->header('career'));
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $assignmentsAux = Assignment::select('assignments.*')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.id')
            ->join('modules', 'psam.module_id', 'modules.id')
            ->where('assignments.career_id', $career_id)->where('modules.id', $module_id)->where('assignments.academic_period_id', $academicPeriod->academic_period_id)->get();
        $assignments = [];
        foreach ($assignmentsAux as &$assignment) {
            $assignmentClass = new stdClass();
            $assignmentClass->id = $assignment->id;
            $area_module = $assignment->area_module()->first();
            $assignmentClass->module = $area_module->module()->first();
            $assignmentClass->teacher = $assignment->teacher()->first();
            $assignmentClass->teacher->person = $assignment->teacher->person()->first();
            $scenario_area = $area_module->scenario_area()->first();
            $assignmentClass->scenario = $scenario_area->scenario()->first();
            $assignmentClass->scenario->areas = $assignmentClass->scenario->areas();
            array_push($assignments, $assignmentClass);
        }
        return $this->response->ok(compact('assignments'), count($assignments) == 0 ? false : true);
    }
    /**
     * Listar asignaciones
     *
     * @return \Illuminate\Http\Response
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "assignments": [
     *              {
     *                  "id": "int",
     *                  "module": "string",
     *                  "teacher": {
     *                        "person": {}
     *                   },
     *                  "escenario": {
     *                       "areas": []
     *                  }
     *              }
     *          ]
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */

    public function index(Request $request)
    {
        $career_id = intval($request->header('career'));
        $assignmentsAux = Assignment::where('career_id', $career_id)->get();
        $assignments = [];
        foreach ($assignmentsAux as &$assignment) {
            array_push($assignments, $this->assignment($assignment));
        }
        return $this->response->ok(compact('assignments'), count($assignments) == 0 ? false : true);
    }

    /**
     * Registrar asignación
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam scenario_id int required Id del escenario de práctica
     * @bodyParam area_id int required Id del área
     * @bodyParam module_id int required Id del módulo
     * @bodyParam teacher_id int required Id del profesor a asociar
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "assignment":
     *              {
     *                  "id": "int",
     *                  "module_area_id": "int",
     *                  "teacher_id": "int",
     *                  "career_id": "int",
     *                  "academic_period_id": "int",
     *                  "date": "2021-10-20"
     *              }
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        try {
            $career_id = intval($request->header('career'));
            $assignment = new Assignment();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $scenario_area = PracticeScenarioArea::where('practice_scenarios_id', $request->input('scenario_id'))->where('area_id', $request->input('area_id'))->first();
            if (is_null($scenario_area)){
                return $this->response->badRequest('No existe la relación escenario de práctica con el área');
            }
            $module_area = PracticeScenarioAreaModule::where('module_id', $request->input('module_id'))->where('practice_scenarios_areas_id', $scenario_area->id)->first();
            if (is_null($module_area)){
                return $this->response->badRequest('No existe la relación del área con el módulo de práctica');
            }
            $exist = Assignment::where('module_area_id',$module_area->id)->where('teacher_id',$request->input('teacher_id'))->where('academic_period_id',$academicPeriod->academic_period_id)->where('career_id',$career_id)->first();
            if (is_null($exist)){
                DB::beginTransaction();
                $assignment->module_area_id = $module_area->id;
                $assignment->teacher_id = $request->input('teacher_id');
                $assignment->academic_period_id = $academicPeriod->academic_period_id;
                $assignment->career_id = $career_id;
                $assignment->date = date('yy-m-d');
                $assignment->save();
                DB::commit();
                return $this->response->ok(compact('assignment'));
            } else {
                return $this->response->badRequest("Ya existe la asignación");
            }
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar asignación
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam scenario_id int required Id del escenario de práctica
     * @bodyParam area_id int required Id del área
     * @bodyParam module_id int required Id del módulo
     * @bodyParam teacher_id int required Id del profesor a asociar
     * 
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "assignment":
     *              {
     *                  "id": "int",
     *                  "module_area_id": "int",
     *                  "teacher_id": "int",
     *                  "career_id": "int",
     *                  "academic_period_id": "int",
     *                  "date": "2021-10-20"
     *              }
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */
    public function update(Request $request, int $id)
    {
        try {
            DB::beginTransaction();
            $career_id = intval($request->header('career'));
            $assignment = Assignment::find($id);
            $scenario_area = PracticeScenarioArea::where('practice_scenarios_id', $request->input('scenario_id'))->where('area_id', $request->input('area_id'))->first();
            $module_area = PracticeScenarioAreaModule::where('module_id', $request->input('module_id'))->where('practice_scenarios_areas_id', $scenario_area->id)->first();
            $assignment->module_area_id = $module_area->id;
            $assignment->teacher_id = $request->input('teacher_id');
            $assignment->career_id = $career_id;
            $assignment->date = date('yy-m-d');
            $assignment->update();
            DB::commit();
            return $this->response->ok(compact('assignment'));
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }



    /**
     * Obtener la asignación por id
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     * 
     * @urlParam id int required Id de la asignación
     * @response {
     *     "state": true,
     *     "message" : "ok",
     *     "data": {
     *          "assignment":
     *              {
     *                  "id": "int",
     *                  "module_area_id": "int",
     *                  "teacher_id": "int",
     *                  "career_id": "int",
     *                  "academic_period_id": "int",
     *                  "date": "Date() -> 2021-10-20"
     *              }
     *      }
     * }
     * 
     * @response 404 {
     *      "state": false,
     *      "message": "Error",
     *      "data": null
     * }
     */
    public function show(int $id)
    {
        $assignment = Assignment::find($id);
        $assignmentClass = $this->assignment($assignment);
        return $this->response->ok(compact('assignmentClass'), $assignmentClass == null ? false : true);
    }
}

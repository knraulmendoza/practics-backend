<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateProcedureRequest;
use App\Http\Response;
use App\Module;
use App\Procedure;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Mockery\Expectation;

/**
 * 
 * @group Procedimientos
 * 
 */
class ProcedureController extends Controller
{

    public $response;
    public $table = 'procedure';
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Registrar procedimiento o actividad de práctica
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam name String required Nombre del procedimiento
     * @bodyParam description String Descripción del procedimiento
     * @bodyParam module_id int required Id del módulo a asociar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"procedure": {"id": "int", "name": "String", "description": "String", "module_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function store(ValidateProcedureRequest $request)
    {
        try {
            DB::beginTransaction();
            $procedure = new Procedure($request->all());
            $procedure->save();
            DB::commit();
            return $this->response->ok(compact('procedure'));
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * 
     * Listar procedimientos por grupo
     * 
     * El estado cambia a falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"procedure": {"id": "int", "name": "String", "description": "String", "module_id": "int"}}
     * }
     * 
     * 
     */
    public function getProdceduresByGroup(int $groupId)
    {
        $module = Module::find($groupId);
        $procedures = $module->procedures()->get();
        return $this->response->ok(compact('procedures'), count($procedures) == 0 ? false : true);
    }


    /**
     * Actualizar procedimiento
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * 
     * @bodyParam name String required Nombre del procedimiento
     * @bodyParam description String Descripción del procedimiento
     * @bodyParam module_id int required Id del módulo a asociar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"procedure": {"id": "int", "name": "String", "description": "String", "module_id": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function update(ValidateProcedureRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $procedure = Procedure::findOrFail($id);
            $procedure->update($request->all());
            DB::commit();
            return $this->response->ok(compact('procedure'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Eliminar procedures
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"procedure": { }}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $procedure = Procedure::destroy($id);
            DB::commit();
            return $this->response->ok(compact('procedure'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

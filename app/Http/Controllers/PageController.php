<?php

namespace App\Http\Controllers;

use App\Http\Response;
use App\Page;
use Exception;
use Illuminate\Http\Request;

/**
 * @group Página 
 *
 */
class PageController extends Controller
{
    public $response;

    public function __construct()
    {
        $this->response = new Response;
    }

    /**
     * Listar páginas
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "pages": [{"id": "int", "title": "String", "icon": "String", "path": "String", "state": "int", "subGroup": "Array<Page>"}]}
     * }
     * 
     */
    public function index()
    {
        $pagesInd = Page::where('parent_page', null)->where('state', '1')->get();
        $pagesGro = Page::where('parent_page', '!=', null)->where('state', '1')->get()->groupBy('parent_page')->toArray();
        $pages = [];
        foreach ($pagesGro as &$page) {
            foreach ($page as &$item) {
                $pageClass = new Page($item);
                if ($item['id'] == $item['parent_page']) {
                    $pageClass->subGroup = $page;
                    array_push($pages, $pageClass);
                }
            }
        }
        foreach ($pagesInd as &$value) {
            array_push($pages, $value);
        }
        return $this->response->ok(compact('pages'));
    }

    /**
     * Listar páginas sin discriminación de estado
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": [true-false],
     *      "message": "ok",
     *      "data": { "pages": [{"id": "int", "title": "String", "icon": "String", "path": "String", "state": "int", "subGroup": "Array<Page>"}]}
     * }
     * 
     */
    public function getAllPages()
    {
        $pagesInd = Page::where('parent_page', null)->get();
        $pagesGro = Page::where('parent_page', '!=', null)->get()->groupBy('parent_page')->toArray();
        $pages = [];
        foreach ($pagesGro as &$page) {
            foreach ($page as &$item) {
                $pageClass = new Page($item);
                if ($item['id'] == $item['parent_page']) {
                    $pageClass->subGroup = $page;
                    array_push($pages, $pageClass);
                }
            }
        }
        foreach ($pagesInd as &$value) {
            array_push($pages, $value);
        }
        return $this->response->ok(compact('pages'));
    }

    /**
     * Actualizar página
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam title String Título de la página
     * @bodyParam icon String Icono de la página
     * @bodyParam path String Path url del sitio web
     * @bodyParam state int Estado de la página
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "page": {"id": "int", "title": "String", "icon": "String", "path": "String", "state": "int"}}
     * }
     * 
     */
    public function update(Request $request, $id)
    {
        try {
            $page = Page::find($id);
            $page->update($request->all());
            return $this->response->ok(compact('page'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

<?php

namespace App\Http\Controllers;
use App\Http\Response;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

class UpcController extends Controller
{
    public $query = '?token=47c58f2146251d7dd590d34c1082e8b2&user=saludupc&password=practicas%232022';
    public $baseUrl;
    public $response;
    public function __construct()
    {
        $this->baseUrl = env('API_ENDPOINT');
        $this->response = new Response();
    }
    public function getSubjectAll(String $program)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/asignatura.jsp'.$this->query.'&programa='.$program);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getSubject(String $code)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/asignatura.jsp'.$this->query.'&codigo='.$code);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getPeriod()
    {
        try {
            $responseData = Http::get($this->baseUrl.'/periodo.jsp'.$this->query);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getStudentSubject(String $subject)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/estudiantesAsignaturas.jsp'.$this->query.'&asignatura='.$subject);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function verifyUser(String $email)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/user.jsp'.$this->query.'&email='.$email);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getFaculties()
    {
        try {
            $responseData = Http::get($this->baseUrl.'/facultades.jsp'.$this->query);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getPrograms(String $faculty)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/programa.jsp'.$this->query.'&facultad='.$faculty);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
    public function getProgram(String $code)
    {
        try {
            $responseData = Http::get($this->baseUrl.'/programa.jsp'.$this->query.'&codigo='.$code);
            return $responseData->json();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

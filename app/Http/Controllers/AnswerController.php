<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Attention;
use App\Enums\Roles;
use App\Enums\StateAttention;
use App\Enums\StatesChecks;
use App\Http\Response;
use App\Question;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  Respuestas
 *
 * APIs para respuestas
 */
class AnswerController extends Controller
{

    public $response;

    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2;3;5', ['only' => ['index', 'store', 'update']]);
    }

    /**
     * Registrar Respuesta para encuesta pacientes
     *
     * @bodyParam attention_id int required Atención a encuestar Example: 2
     * @bodyParam answers Array required respuestas Example: [{"answer":1,"question_id":13,"observation":"Respuesta correcta"}]
     * 
     * @response {
     *  "status": true,
     *  "message": "ok",
     *  "data" : { "answers": [{"answer":1,"question_id":13,"observation":"Respuesta correcta"}] }
     * }
     * 
     * @response 404 {
     *   "status": false,
     *   "message" : "Error",
     *   "data": null
     * }
     * @return \Illuminate\Http\Response
     */
    public function saveAnswerEncuesta(Request $request)
    {
        $rol = intval($request->header('rol'));
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person->first();
                DB::beginTransaction();
                $attention = Attention::find($request->input('attention_id'));
                $poll = $attention->poll()->first();
                $answers = $request->input('answers');
                $questions = Question::where('type', 2)->get();
                if (count($questions) != count($answers)) {
                    return $this->response->badRequest('debe mandar todas las respuestas ');
                }
                if ($poll) {
                    foreach ($answers as $answer) {
                        $poll->answers()->create(['answer' => $answer['answer'], 'question_id' => $answer['question_id']]);
                    }
                    $attention->state = StateAttention::EVALUATED;
                    $attention->save();
                } else {
                    return $this->response->badRequest('Esta encuesta no existe');
                }
                DB::commit();
                return $this->response->ok(compact('answers'));
            }
        } catch (Exception $e) {

            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar Respuesta para chequeos
     * 
     * @bodyParam evaluated int required Persona a hacer chequeo Example: 2
     * @bodyParam answers Array required respuestas Example: [{"answer":1,"question_id":13,"observation":"Respuesta correcta"}]
     * 
     * @response {
     *  "status": true,
     *  "message": "Respuestas guardadas",
     *  "data" : { "answers": [{"answer":1,"question_id":13,"observation":"Respuesta correcta"}] }
     * }
     * 
     * @response 404 {
     *   "status": false,
     *   "message" : "Error",
     *   "data": null
     * }
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol = intval($request->header('rol'));
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                DB::beginTransaction();
                $person = $user->person()->first();
                $check = $person->checkByEvaluator($request->input('evaluated'));
                $answers = $request->input('answers');
                //$condicion = $rol==Roles::TEACHER_PRACTICT?'students':$rol==Roles::COORDINATOR?'teachers':'';
                switch ($rol) {
                    case Roles::TEACHER_PRACTICT:
                        $condicion = 'students';
                        break;
                    case Roles::COORDINATOR:
                        $condicion = 'teachers';
                        break;

                    default:
                        $condicion = '';
                        break;
                }
                $questions = Question::where('check_type', 'like', '%' . $condicion . '%')->get();
                if (count($questions) != count($answers)) {
                    return $this->response->badRequest('debe mandar todas las respuestas ' . $condicion);
                }
                if ($check) {
                    foreach ($answers as $answer) {
                        $check->answers()
                            ->create(['answer' => $answer['answer'], 'question_id' => $answer['question_id']])
                            ->observation()->create(['observation' => $answer['observation']]);
                    }
                    $check->state = StatesChecks::REALIZADA;
                    $check->save();
                } else {
                    return $this->response->badRequest('Este chequeo no existe');
                }
                DB::commit();
                return $this->response->ok(compact('answers'));
            }
        } catch (Exception $e) {

            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        //
    }
}

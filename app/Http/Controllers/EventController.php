<?php

namespace App\Http\Controllers;

use App\EventPractict;
use App\Http\Response;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group  Eventos 
 *
 */
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Listar eventos paginado
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "events": [{"title": "String", "description": "String", "date": "String", "hour": "int", "address": "String", "state": "int", "image": "String"}],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response {
     *      "state" : false,
     *      "message": "Ok",
     *      "data": {
     *          "events": [],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     */
    public function index()
    {
        $per_page = 3;
        $events = EventPractict::orderBy('date', 'DESC')->paginate($per_page);
        $total = $events->total();
        $events = $events->items();
        return $this->response->ok(compact('events', 'total', 'per_page'), count($events) == 0 ? false : true);
    }


    /**
     *
     * Registrar evento
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * 
     * @bodyParam title String required Título del evento
     * @bodyParam description String required Descripción del evento
     * @bodyParam date String required Fecha en la cual se va a realizar el evento
     * @bodyParam hour int required Hora en la cual se va a realizar el evento
     * @bodyParam address String required Lugar en la cual se va a realizar el evento
     * @bodyParam image File Imagen referente al evento
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "event": {"title": "String", "description": "String", "date": "String", "hour": "int", "address": "String", "state": "int", "image": "String"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state" : false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $event = new EventPractict($request->all());
            if ($request->file('image')) {
                $image = $request->file('image');
                $array = explode('.', $image->getClientOriginalName());
                $timestamp = now()->timestamp;
                $image->move('images', $array[0] . $timestamp . '.' . end($array));
                $event->image = $array[0] . $timestamp . '.' . end($array);
            } else {
                $event->image = "default.png";
            }
            $event->save();
            DB::commit();
            return $this->response->ok(compact('event'));
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Obtener evento por el id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "event": {"title": "String", "description": "String", "date": "String", "hour": "int", "address": "String", "state": "int", "image": "String"}
     *      }
     * }
     * 
     * @response {
     *      "state" : false,
     *      "message": "ok",
     *      "data": { "event": []}
     * }
     */
    public function show($id)
    {
        $event = EventPractict::find($id);
        return $this->response->ok(compact('event'), $event == null ? false : true);
    }


    /**
     * Actualizar evento
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @bodyParam title String required Título del evento
     * @bodyParam description String required Descripción del evento
     * @bodyParam date String required Fecha en la cual se va a realizar el evento
     * @bodyParam hour int required Hora en la cual se va a realizar el evento
     * @bodyParam address String required Lugar en la cual se va a realizar el evento
     * @bodyParam image File Imagen referente al evento
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "event": {"title": "String", "description": "String", "date": "String", "hour": "int", "address": "String", "state": "int", "image": "String"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state" : false,
     *      "message": "ok",
     *      "data": { "event": []}
     * }
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $event = EventPractict::findorFail($id);
            if (!$event) {
                return $this->response->badRequest('No se encontro el evento');
            }
            $event->update($request->all());
            DB::commit();
            return $this->response->ok(compact('event'));
        } catch (QueryException $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

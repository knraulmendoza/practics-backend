<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateProgramRequest;
use App\Career;
use App\Enums\Roles;
use App\Faculty;
use App\Http\Response;
use App\Module;
use App\Student;
use Exception;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  Carrera 
 *
 * endpoints para asignaciones
 */

class CareerController extends Controller
{

    public $response;
    public $careerId = 'career_id';
    public function __construct()
    {
        $this->response = new Response();
    }
    /**
     * Listar carreras
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "careers": [
     *               {"id": "int", "code": "String", "name": "String", "faculty": {}}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function index()
    {
        $careers = Career::with('faculty')->get();
        return $this->response->ok(compact('careers'));
    }

    /**
     * Listar carreras por facultad
     *
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "careers": [
     *               {"id": "int", "code": "String", "name": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getCareerByFaculty(String $facultyAux)
    {
        $faculty = Faculty::where('code', $facultyAux)->first();
        $careers = Career::where('faculty_id', $faculty->id)->get();
        return $this->response->ok(compact('careers'));
    }

    /**
     * Lista de facultad
     *
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "faculties": [
     *               {"id": "int", "code": "String", "name": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */

    public function getFaculty()
    {
        $faculties = Faculty::all();
        return $this->response->ok(compact('faculties'));
    }

    /**
     * Registrar carrera
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * @authenticated
     * @bodyParam name string required Nombre de la carrera
     * @bodyParam code string required código de la carrera
     * @bodyParam faculty String required Código de la facultad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "faculties": [
     *               {"id": "int", "code": "String", "name": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function store(ValidateProgramRequest $request)
    {
        //
        try {
            DB::beginTransaction();
            $faculty = Faculty::where('code', $request->input('faculty'))->first();
            $career = new Career($request->all());
            $career->faculty_id = $faculty->id;
            $career->save();
            DB::commit();
            return $this->response->ok(compact('career'));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }



    /**
     * 
     * Registrar Facultad
     * @authenticated
     * @bodyParam name string required Nombre de la facultad
     * @bodyParam code string required código de la facultad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "faculty": {"id": "int", "code": "String", "name": "String"}
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */

    public function saveFaculty(Request $request)
    {
        //
        try {
            DB::beginTransaction();
            $faculty = new Faculty($request->all());
            $existFaculty = Faculty::where('code', $request->input("code"))->first();
            if ($existFaculty) {
                $existFaculty->update($request->all());
                DB::commit();
                return $this->response->ok(compact('existFaculty'));
            }else{
                $faculty->save();
                DB::commit();
                return $this->response->ok(compact('faculty'));
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }


    /**
     * Listar modulos por carrera
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "modules": [
     *               {"id": "int", "code": "String", "name": "String", "description": "String", "subject": "String", "subject_code": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getModules(Request $request)
    {
        $careerId = $request->header('career');
        $modules = Career::select('modules.id', 'modules.code', 'modules.name as module', 'modules.description', 'subjects.name as subject', 'subjects.code as subject_code')
            ->join('careers_subjects', 'careers_subjects.career_id', '=', 'careers.id')
            ->join('subjects', 'careers_subjects.subject_id', '=', 'subjects.id')
            ->join('modules', function ($join) {
                $join->on('modules.career_id', '=', 'careers.id')->on('modules.subject_id', '=', 'subjects.id');
            })
            ->where('modules.' . $this->careerId, '=', $careerId)->get();
        return $this->response->ok(compact('modules'));
    }

    /**
     * Listar modulos paginado por carrera
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "modules": [
     *               {"id": "int", "code": "String", "name": "String", "description": "String", "subject": "String", "subject_code": "String"}
     *           ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getModulesPaginate(Request $request)
    {
        $careerId = $request->header('career');
        $per_page = 8;
        $modules = Career::select('modules.id', 'modules.code', 'modules.name as module', 'modules.description', 'subjects.name as subject', 'subjects.code as subject_code')
            ->join('careers_subjects', 'careers_subjects.career_id', '=', 'careers.id')
            ->join('subjects', 'careers_subjects.subject_id', '=', 'subjects.id')
            ->join('modules', function ($join) {
                $join->on('modules.career_id', '=', 'careers.id')->on('modules.subject_id', '=', 'subjects.id');
            })
            ->where('modules.' . $this->careerId, '=', $careerId)->paginate($per_page);
        $total = $modules->total();
        $modules = $modules->items();
        return $this->response->ok(compact('modules', 'total', 'per_page'));
    }
    /**
     * Listar carreras por asignatura
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "subjects": [
     *               {"id": "int", "code": "String", "name": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getSubjects(Request $request)
    {
        try {
            $careerId = $request->header('career');
            $career = Career::find($careerId);
            $subjects = [];
            if ($career) {
                $subjects = $career->subjects()->get();
            }
            return $this->response->ok(compact('subjects'));;
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }


    /**
     * Listar carreras paginada por asignatura
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "subjects": [
     *               {"id": "int", "code": "String", "name": "String"}
     *           ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getSubjectsPaginate(Request $request)
    {
        try {
            $careerId = $request->header('career');
            $career = Career::find($careerId);
            $per_page = 8;
            $subjects = [];
            if ($career) {
                $subjects = $career->subjects()->paginate($per_page);
            }
            $total = $subjects->total();
            $modules = $subjects->items();
            return $this->response->ok(compact('subjects', 'total', 'per_page'));;
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }


    /**
     * Listar preguntas por carrera
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [
     *               {"id": "int", "type": "String", "question": "String", "check_type": "int"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getQuestions(Request $request)
    {
        try {
            $careerId = intval($request->header('career'));
            $career = Career::find($careerId);
            $questions = [];
            if ($career) {
                $questions = $career->questions()->get();
            }
            return $this->response->ok(compact('questions'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar preguntas paginada por carrera
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [
     *               {"id": "int", "type": "String", "question": "String", "check_type": "int"}
     *           ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getQuestionsPaginate(Request $request)
    {
        try {
            $careerId = intval($request->header('career'));
            $career = Career::find($careerId);
            $per_page = 12;
            $questions = [];
            $total = 0;
            if ($career) {
                $questions = $career->questions()->paginate($per_page);
            }
            if (count($questions) > 0) {
                $total = $questions->total();
                $questions = $questions->items();
            }
            return $this->response->ok(compact('questions', 'total', 'per_page'), count($questions) == 0 ? false : true);
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar preguntas paginada por carrera
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "students": [
     *               {"id": "int", "person_id": "int", "person": "Object<Person>"}
     *           ],
     *          "total": "int",
     *          "per_page": "int"
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */

    public function getStudents(Request $request)
    {
        $career = intval($request->header('career'));
        $per_page = 8;
        $students = Student::select('students.*')
            ->join('people as p', 'students.person_id', '=', 'p.id')
            ->join('people_careers as pc', 'p.id', '=', 'pc.person_id')
            ->join('careers as c', 'pc.career_id', '=', 'c.id')
            ->Where('c.id', '=', $career)->with('person')->paginate($per_page);
        $total = $students->total();
        $students = $students->items();
        return $this->response->ok(compact('students', 'total', 'per_page'));
    }

    /**
     * Listar preguntas por chequeos
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "questions": [
     *               {"id": "int", "type": "int", "question": "String", "check_type": "String"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */

    public function getQuestionsCheck(Request $request)
    {
        $career = intval($request->header('career'));
        $rol = intval($request->header('rol'));
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            $questions = [];
            if ($user->hasRole(strval($rol)) && $user->person()->first()->hasCarrers(strval($career))) {
                $check_type = '';
                if (Roles::TEACHER_PRACTICT == $rol) {
                    $check_type = 'students';
                }
                if (Roles::COORDINATOR == $rol) {
                    $check_type = 'teachers';
                }
                $career = Career::find($career);
                $questions = $career->questions()->where('type', 'like', 1)->where('check_type', 'like', $check_type)->get();
            }
            return $this->response->ok(compact('questions'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar asignaciones
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "assignments": [
     *               {"academic_period": "Object<PeriodAcademic>", "module": "Object<Module>", "teacher": "Object<Teacher>"}
     *           ]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getAssignments(Request $request, $subject_id)
    {
        $career_id = intval($request->header('career'));
        $condition = $subject_id == 0 ? 'orWhere' : 'where';
        $modules = Module::select('modules.*')
            ->join('careers_subjects', function ($join) {
                $join->on('modules.career_id', '=', 'careers_subjects.career_id')->on('modules.subject_id', '=', 'careers_subjects.subject_id');
            })
            ->join('subjects', 'careers_subjects.subject_id', '=', 'subjects.id')
            ->join('careers', 'careers_subjects.career_id', '=', 'careers.id')
            ->where('careers.id', '=', $career_id)->$condition('subjects.id', '=', $subject_id)->get();
        $assignments = [];
        for ($i = 0; $i < count($modules); $i++) {
            $assignmentsAux = $modules[$i]->assignments()->get();
            for ($j = 0; $j < count($assignmentsAux); $j++) {
                $module = $assignmentsAux[$j]->module()->first();
                $teacher = $assignmentsAux[$j]->teacher()->first();
                $person = $teacher->person()->first();
                $academic_period = $assignmentsAux[$j]->periodAcademic()->first();
                $teacher->person = $person;
                $dataJson = compact('academic_period', 'module', 'teacher');
                array_push($assignments, $dataJson);
            }
        }
        return $this->response->ok(compact('assignments'));
    }


    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * Actualizar carrera
     * 
     * @bodyParam name String required Nombre de la carrera
     * @bodyParam code String required Código de la carrera
     *
     * @authenticated
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "career": {
     *              "id": "int",
     *              "name": "String",
     *              "code": "String"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $career = Career::find($id);
            if ($career) {
                $career->update($request->all());
                DB::commit();
                return $this->response->ok(compact('career'));
            }
            return $this->response->badRequest('No se encontro la carrera');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->response->badRequest($e->getMessage());
        }
    }
}

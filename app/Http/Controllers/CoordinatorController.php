<?php

namespace App\Http\Controllers;

use App\Coordinator;
use App\Enums\StatePerson;
use App\Enums\StateUser;
use App\Http\Requests\ValidateAuthRequest;
use App\Http\Requests\ValidatePersonRequest;
use App\Http\Response;
use App\Person;
use App\PeopleCareers;
use App\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group  Coordinador 
 *
 */
class CoordinatorController extends Controller
{
    public $response;
    public $table = "coordinator";

    public function __construct()
    {
        $this->middleware('auth.role:1');
        $this->response = new Response();
    }

    /**
     * Listar coordinadores con datos personas
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "coordinators": [{"id": "int", "person_id": "int", "person": "Object<Person>", "career": "Object<Career>"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": "true",
     *      "message": "Error de servidor",
     *      "data" : null
     * }
     * 
     */
    public function getPeople()
    {
        try {
            $coordinators = array();
            $people = Coordinator::select('p.*')
                ->join('people as p', 'coordinators.person_id', '=', 'p.id')
                ->join('people_careers as pc', 'pc.person_id', '=', 'p.id')
                ->join('careers as c', 'pc.career_id', '=', 'c.id')->get();
            for ($i = 0; $i < count($people); $i++) {
                $person = Person::find($people[$i]->id);
                $career = $person->careers()->first();
                $coordinator = $person->coordinator()->first(['id', 'state']);
                $coordinator->person = $people[$i];
                $coordinator->career = $career;
                array_push($coordinators, $coordinator);
            }
            return $this->response->ok(compact('coordinators'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar estado y datos de la coordinadora
     * 
     * @bodyParam state int required Estado de la coordinador/a
     * @bodyParam person.phone String Número telefonico de la coordinadora
     * @bodyParam person.address String Dirección de la coordinadora
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "coordinators": [{"id": "int", "person_id": "int", "person": "Object<Person>", "career": "Object<Career>"}]
     *      }
     * }
     * 
     * @response 400 {
     *      "state": "true",
     *      "message": "Error de servidor",
     *      "data" : null
     * }
     * 
     */
    public function update(Request $request, $id)
    {
        try {
            $career = intval($request->header('career'));
            DB::beginTransaction();
            $coordinator = Coordinator::find($id);
            if ($coordinator) {
                if (strval($request->input('state') == '1')) {
                    $coordinatorAux = Coordinator::select('coordinators.*')->join('people', 'coordinators.person_id', 'people.id')
                        ->join('people_careers as pc', 'people.id', 'pc.person_id')
                        ->join('careers', 'pc.career_id', 'careers.id')
                        ->where('careers.id', $career)->where('state', '1')->first();
                    // $coordinator = Coordinator::find($coordinatorAux->id);
                    if ($coordinatorAux) {
                        $coordinatorAux->state = '0';
                        $coordinatorAux->update();
                        $person = $coordinatorAux->person()->first();
                        $user = $person->user()->first();
                        $user->state = StateUser::INACTIVO;
                        $user->update();
                    }
                }
                $person = $coordinator->person()->first();
                $person->phone = $request->input('person.phone') ? $request->input('person.phone') : '';
                $person->address = $request->input('person.address') ? $request->input('person.address') : '';
                $person->update();
                $user = $person->user()->first();
                if ($coordinator->state != $request->input('state')) {
                    $user->state = $request->input('state') == StatePerson::INACTIVO ? StateUser::INACTIVO : StateUser::ACTIVO;
                    $user->update();
                }
                $coordinator->state = strval($request->input('state'));
                $coordinator->update();
                DB::commit();
                return $this->response->ok(compact('coordinator'));
            }
            DB::rollBack();
            return $this->response->badRequest('No se pudo actualizar el coordinador');
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

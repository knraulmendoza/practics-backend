<?php

namespace App\Http\Controllers;

use App\AcademicPeriod;
use App\AssignStudentGroup;
use App\Enums\Roles;
use App\Http\Response;
use App\Person;
use App\Rotation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group Rotación
 */
class RotationController extends Controller
{
    public $response;
    public function __construct()
    {
        $this->response = new Response();
        $this->middleware('auth.role:1;2', ['only' => ['index']]);
        $this->middleware('auth.role:1;5', ['only' => ['rotationsByGroup']]);
        $this->middleware('auth.role:1;4', ['only' => ['AsignarRotacion']]);
        $this->middleware('auth.role:1;2', ['only' => ['store']]);
        $this->middleware('auth.role:1;3', ['only' => ['rotationsWithGroups']]);
    }

    /**
     * Listar rotaciones
     * 
     * El estado cambia a falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "rotationsType": [{"rotations": [{"id": "int", "start_date": "String", "end_date": "String", "academic_period_id": "int", "type": "int", "career_id": "int"}]}],
     *          "rotationEnd": { "id": "int", "start_date": "String", "end_date": "String", "academic_period_id": "int", "type": "int", "career_id": "int"}
     *      }
     * }
     */
    public function index(Request $request)
    {
        $career_id = intval($request->header('career'));
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $rotationOrder = Rotation::orderBy('type', 'ASC')->where('academic_period_id', $academicPeriod->academic_period_id)
            ->where('career_id', $career_id)->get();
        $rotationsAux = $rotationOrder->groupBy('type')->toArray();
        $rotationsType = [];
        foreach ($rotationsAux as &$rotation) {
            $rotationClass = new stdClass();
            $rotationClass->rotations = $rotation;
            array_push($rotationsType, $rotationClass);
        }
        $rotationEnd = Rotation::orderBy('created_at', 'DESC')->first();
        return $this->response->ok(compact('rotationsType', 'rotationEnd'), count($rotationsType) == 0 ? false : true);
    }

    public function getRotation($career_id)
    {
        $rotation = Rotation::where('rotations.career_id', $career_id)->whereRaw("? BETWEEN rotations.start_date AND rotations.end_date", [date('Y-m-d')])->first();
        return !$rotation ? null : $rotation;
    }

    /**
     * 
     * Listar rotaciones por code
     * 
     * El estado puede ser falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"rotations": [{"id": "int", "start_date": "String", "end_date": "String", "code": "String", "type": "int"}]}
     * }
     * 
     */
    public function getRotationsByCode(Request $request, String $code)
    {
        $career_id = $request->header('career');
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $rotations = Rotation::select('r.id', 'r.start_date', 'r.end_date', 'r.code', 'r.type')->from('rotations as r')
            ->where('r.academic_period_id', $academicPeriod->academic_period_id)
            ->where('r.career_id', $request->header('career'))
            ->where('r.code', $code)->get();
        return $this->response->ok(compact('rotations'), count($rotations) == 0 ? false : true);
    }

    /**
     * 
     * Listar rotaciones por rol
     * 
     * El estado puede ser falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"rotations": [{"id": "int", "start_date": "String", "end_date": "String", "code": "String", "type": "int"}]}
     * }
     * 
     */
    public function getRotationsByRol(Request $request, $person)
    {
        $career_id = $request->header('career');
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (is_null($academicPeriod)) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $person = Person::find($person);
        $user = $person->user()->first();
        $rotations = [];
        if ($user->hasRole(Roles::STUDENT)) {
            $student = $person->student()->first();
            $group =  $student->groups($academicPeriod->academic_period_id)->first();
            $rotations = $group->rotations()->get();
        }
        if ($user->hasRole(Roles::TEACHER_PRACTICT)) {
            $teacher = $person->teacher()->first();
            $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
            $rotations = $assignment->rotations()->get();
        }
        return $this->response->ok(compact('rotations'), count($rotations) == 0 ? false : true);
    }
    /**
     * 
     * Registrar rotación
     * 
     * @bodyParam start_date String required Fecha inicial de la rotación
     * @bodyParam end_date String required Fecha final de la rotación
     * @bodyParam type int required Tipo de rotación (Full, doble, triple)
     * @bodyParam code String Código de la rotación
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": true
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function store(Request $request)
    {
        $career_id = intval($request->header('career'));
        if ($career_id) {
            try {
                DB::beginTransaction();
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (is_null($academicPeriod)) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                foreach ($request->input('rotations') as &$rotation) {
                    $rotationClass = new Rotation($rotation);
                    $rotationClass->academic_period_id = $academicPeriod->academic_period_id;
                    $rotationClass->career_id = $career_id;
                    $rotationClass->save();
                }
                DB::commit();
                return $this->response->ok(compact(true));
            } catch (Exception $e) {
                DB::rollBack();
                return $this->response->badRequest('Ocurrio un error ' . $e->getMessage());
            }
        } else {
            return $this->response->badRequest('El usuario debe estar relacionado a una carrera');
        }
    }

    public function show($id)
    {
        try {
            $rotation = Rotation::find($id);
            return $this->response->ok(compact('rotation'));
        } catch (Exception $ex) {
            return $this->response->badRequest('Ocurrio un error al obtener la rotación');
        }
    }

    public function AsignarRotacion(Request $request)
    {
        try {
            DB::beginTransaction();
            $asinacion = new AssignStudentGroup($request->all());
            $asinacion->save();
            DB::commit();
            return $this->response->ok(compact('asinacion'));
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->response->badRequest('Ocurrio un error al asignar la rotación');
        }
    }

    /**
     * 
     * Listar rotaciones por grupo de práctica
     * 
     * El estado puede ser falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"rotations": [{"escenario": "String", "area": "String", "working_day": "String", "start_date": "String", "end_date": "String", "state": "boolean"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "no hay rotaciones que mostrar",
     *      "data": null
     * }
     * 
     */
    public function rotationsByGroup(Request $request, int $groupId)
    {
        $career_id = $request->header('career');
        $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
        if (!$academicPeriod) {
            return $this->response->badRequest('No existe periodo académico vigente');
        }
        $rotations = Rotation::select('ps.name as escenario', 'areas.name as area', 'asg.working_day', 'rotations.start_date', 'rotations.end_date')
            ->join('assign_student_groups as asg', 'asg.rotation_id', '=', 'rotations.id')
            ->join('assignments', 'asg.assignment_id', '=', 'assignments.id')
            ->join('academic_periods', 'assignments.academic_period_id', '=', 'academic_periods.id')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', '=', 'psam.id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', '=', 'psa.id')
            ->join('practice_scenarios as ps', 'psa.practice_scenarios_id', '=', 'ps.id')
            ->join('areas', 'psa.area_id', '=', 'areas.id')
            ->orderBy('rotations.start_date', 'ASC')
            ->where('asg.group_module_id', $groupId)->where('academic_periods.id', $academicPeriod->academic_period_id)
            ->get();
        if (count($rotations) == 0) {
            return $this->response->badRequest('no hay rotaciones que mostrar');
        }
        foreach ($rotations as &$rotation) {
            $rotation->state = date('Y-m-d') > date('Y-m-d', strtotime($rotation->start_date)) ? true : false;
        }
        return $this->response->ok(compact('rotations'));
    }


    /**
     * 
     * Listar rotaciones por grupo de práctica
     * 
     * El estado puede ser falso cuando no hay datos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"rotations": [{"id": "int", "start_date": "String", "end_date": "String", "code": "String", "type": "int", "group": "Object<Group> con students"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "no hay rotaciones que mostrar",
     *      "data": null
     * }
     * 
     */
    public function rotationsWithGroups(Request $request, $actual = null)
    {
        $career_id = $request->header('career');
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person()->first();
                $teacher = $person->teacher()->first();
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
                $rotations = $assignment->rotations()->groupBy('assign_student_groups.rotation_id')->get();
                foreach ($rotations as &$rotation) {
                    $rotation->group = $assignment->groupModule($rotation->id)->first();
                    $rotation->group->students = $rotation->group->students()->get();
                    foreach ($rotation->group->students as &$student) {
                        $student->person = $student->person()->first();
                    }
                }
                return $this->response->ok(compact('rotations'));
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Actualizar rotación
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     * @bodyParam rotations Array<Rotation> required Rotaciones a actualizar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"rotations": "Array<Rotation>"}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function update(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (!$academicPeriod) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            if (!$request->rotations) {
                return $this->response->badRequest('Error en los datos de entrada');
            }
            $rotations = Rotation::where('type', $request->rotations[0]['type'])->where('academic_period_id', $academicPeriod->academic_period_id)->get();
            foreach ($rotations as $key => $rotation) {
                $rotation->update($request->input('rotations')[$key]);
            }
            DB::commit();
            return $this->response->ok(compact('rotations'));
        } catch (Exception $ex) {
            DB::rollback();
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Eliminar rotación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"resp": "Array<Rotation>"}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function deletingRotations(Request $request)
    {
        $career_id = $request->header('career');
        try {
            $career = intval($request->header('career'));
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }

            $resp = Rotation::where('academic_period_id', $academicPeriod->academic_period_id)->where('career_id', $career)->delete();
            return $this->response->ok(compact('resp'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
}

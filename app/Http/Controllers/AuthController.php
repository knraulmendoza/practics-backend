<?php

namespace App\Http\Controllers;

use App\Career;
use App\Enums\Roles;
use App\Enums\StatePassword;
use App\Enums\StateUser;
use App\Http\Requests\ValidateAuthenticateRequest;
use App\Http\Requests\ValidateAuthRequest;
use App\Http\Response;
use App\Mail\RecoveryPassword;
use App\Person;
use App\Role;
use App\RolesUsers;
use App\SecurityQuestion;
use App\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

/**
 * @group  autenticación 
 *
 * endpoints para asignaciones
 */
class AuthController extends Controller
{

    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Iniciar sesión
     * 
     * @bodyParam user string required Usuario. Example: poncio@gmail.com
     * @bodyParam password string required Contraseña. Example: 123456
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "token": "String",
     *              "user": {"id": "int", "user": "String", "state": "int", "state_password": "int", "security_answer": "String", "security_question_id": "int", "roles": [], "role": {}},
     *              "careers": "Array<Carrer>"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function authenticate(ValidateAuthenticateRequest $request)
    {
        try {
            $credentials = $request->only('user', 'password');
            $user = User::where('user', $credentials['user'])->first();
            if ($user) {
                if ($user->state == StateUser::INACTIVO) {
                    return $this->response->badRequest('Usuario inactivo');
                }
                $token = JWTAuth::attempt($credentials);
                if ($token) {
                    $user->roles = $user->roles()->get(); //(['id', 'name', 'visual_name']);
                    $user->role = null;
                    if (count($user->roles) > 1) {
                        if ($user->hasRole(strval(Roles::COORDINATOR))) {
                            $user->role = Role::find(Roles::COORDINATOR);
                        } else {
                            $user->role = Role::find(Roles::TEACHER_PRACTICT);
                        }
                    } else {
                        $user->role = $user->roles[0];
                    }
                    $careers = new stdClass();
                    if ($user->hasRole(strval(Roles::ADMIN))) {
                        $careers = Career::all();
                    } else {
                        $careers = $user->person()->first()->careers()->get(['id', 'name', 'code']);
                    }
                    $dataJson = compact('token', 'user', 'careers');
                    return $this->response->ok($dataJson);
                } else {
                    return $this->response->badRequest('Contraseña incorrecta');
                }
            } else {
                return $this->response->badRequest('Usuario incorrecto');
            }
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }


    public function last()
    {
        return User::latest('id')->person()->first();
    }

    public function sendEmail(Request $request)
    {
        try {
            $user = User::where('user', '=', $request->input('user'))->first();
            if (!$user) {
                return '';
            }
            $person = Person::where('document', '=', $request->input('document'))->where('user_id', '=', $user->id)->first();

            if ($person) {
                $password = strtotime("now");
                DB::beginTransaction();
                $user->password = Hash::make($password);
                $user->state_password = StatePassword::CHANGE_PASSWORD;
                $user->update();
                $user->password = $password;
                $sem = true;
                while ($sem) {
                    try {
                        Mail::to($user->user)->send(new RecoveryPassword($user));
                        $sem = false;
                    } catch (Exception $e) {
                        printf("Error: %s\n", $e->getMessage());
                        return $this->response->badRequest($e->getMessage());
                    }
                    sleep(5);
                }
                // Mail::to($user->user)->send(new RecoveryPassword($user));
                DB::commit();
                return $this->response->ok(compact('user'));
            }
            return $this->response->badRequest();
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Verificar usuario
     * 
     * @bodyParam user String required Usuario
     * @bodyParam security_question_id int required Id de la pregunta de seguridad
     * @bodyParam security_answer String required Respuesta de seguridad
     * @bodyParam document String required Documento de identidad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "user": {"id": "int", "user": "String", "state": "int", "state_password": "int", "security_answer": "String", "security_question_id": "int"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function verifyUser(Request $request)
    {
        try {
            $user = User::where('user', '=', $request->input('user'))
                ->where('security_question_id', $request->input('security_question_id'))->first();
            if ($user) {
                if (Hash::check(strtolower($request->input('security_answer')), $user->security_answer)) {
                    if (!$user->hasRole(strval(Roles::ADMIN))) {
                        $person = Person::where('document', '=', $request->input('document'))->where('user_id', '=', $user->id)->first();
                        if ($person) {
                            return $this->response->ok(compact('user'));
                        }
                    } else {
                        return $this->response->ok(compact('user'));
                    }
                    
                }
            }
            return $this->response->badRequest('usuario no existe');
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    public function find(string $user_name)
    {
        try {
            $user = User::where('user', '=', $user_name)->first();
            $person = $user != null ? $user->person()->first() : null;
            return $user != null ? $this->response->ok(compact('user', 'person')) : $this->response->badRequest('usuario no encontrado');
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar preguntas de seguridad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "security_questions": {"question": "String" }
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getSecurityQuestions()
    {
        try {
            $security_questions = SecurityQuestion::all();
            return $this->response->ok(compact('security_questions'), $security_questions->isEmpty() ? false : true);
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Obtener datos del usuario con el token
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "user": {"id": "int", "user": "String", "state": "int", "state_password": "int", "security_answer": "String", "security_question_id": "int"},
     *              "person": "Object<Person>"
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->response->unauthorized();
            }
            $person = $user->person()->first();
        } catch (TokenExpiredException $e) {
            return $this->response->unauthorized('Your token has expired. Please, login again.');
        } catch (TokenInvalidException $e) {
            return $this->response->unauthorized('Your token has invalid. Please, login again.');
        } catch (JWTException $e) {
            return $this->response->unauthorized('Please, attach a Bearer Token to your request');
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
        return $this->response->ok(compact('user', 'person'));
    }

    /**
     * Registrar pregunta de seguridad
     * 
     * @bodyParam question String required Pregunta de seguridad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "security_question": {"id": "int", "question": "String"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function addSecurityQuestion(Request $request)
    {
        try {
            $security_question = new SecurityQuestion($request->all());
            $security_question->save();
            return $this->response->ok(compact('security_question'));
        } catch (Exception $e) {
            return $this->response->badRequest();
        }
    }

    /**
     * Actualizar pregunta de seguridad
     * 
     * @bodyParam question String required Pregunta de seguridad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "security_question": {"id": "int", "question": "String"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function updateSecurityQuestion(Request $request, int $id)
    {
        try {
            $security_question = SecurityQuestion::find($id);
            $security_question->question = $request->input('question');
            $security_question->update();
            return $this->response->ok(compact('security_question'));
        } catch (Exception $e) {
            return $this->response->badRequest();
        }
    }

    /**
     * Eliminar pregunta de seguridad
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "security_question": {"id": "int", "question": "String"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function deleteSecurityQuestion(int $id)
    {
        try {
            $security_question = SecurityQuestion::find($id);
            $security_question->delete();
            return $this->response->ok(compact('security_question'));
        } catch (Exception $e) {
            return $this->response->badRequest();
        }
    }

    /**
     * Registrar usuario
     * 
     * @bodyParam user String required usuario
     * @bodyParam password String required Contraseña
     * @bodyParam rol int required Rol del usuario
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "user": {"id": "int", "user": "String", "state": "int", "state_password": "int", "security_answer": "String", "security_question_id": "int"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function register(ValidateAuthRequest $request)
    {

        try {
            if (!$userAux = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            }
            DB::beginTransaction();
            $user = User::create([
                'user' => $request->get('user'),
                'password' => Hash::make($request->get('password')),
                'user_parent' => $userAux->id,
                'state' => StateUser::ACTIVOSINPERSON
            ]);
            if ($user) {
                $role_user = new RolesUsers();
                $role_user->user_id = $user->id;
                $role_user->role_id = $request->input('rol');
                if ($role_user->save()) {
                    JWTAuth::fromUser($user);
                    DB::commit();
                    return $this->response->ok(compact('user'));
                }
            }
            return $this->response->badRequest();
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        }
    }

    /**
     * Cerrar sesión con el token
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": true
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "fallo en cerrar sesión",
     *      "data": null
     * }
     */
    public function logout()
    {
        try {
            $token =  JWTAuth::getToken();
            JWTAuth::invalidate($token);
            return $this->response->ok(compact(true));
        } catch (Exception $ex) {
            return $this->response->badRequest($ex->getMessage());
        } catch (JWTException $e) {
            return $this->response->badRequest('fallo en cerrar sesión');
        }
    }

    /**
     * Actualizar usuario
     * 
     * @bodyParam password String required Contraseña
     * @bodyParam security_answer String required Respuesta de seguridad
     * @bodyParam security_question_id int required Id de la pregunta de seguridad
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {
     *          "dataJson" : {
     *              "user": {"id": "int", "user": "String", "state": "int", "state_password": "int", "security_answer": "String", "security_question_id": "int"}
     *          }
     *      }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error en el servidor",
     *      "data": null
     * }
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $user->password = Hash::make($request->input('password'));
            if ($user->security_answer) {
                if (!Hash::check($request->input('security_answer'), $user->security_answer)) {
                    return $this->response->badRequest('la pregunta de seguridad no coincide');
                }
            } else {
                $user->security_question_id = $request->input('security_question_id');
                $user->security_answer = Hash::make(strtolower($request->input('security_answer')));
                $user->state_password = StatePassword::UPDATED_PASSWORD;
            }
            $user->update();
            DB::commit();
            return $this->response->ok(compact('user'));
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->response->badRequest($ex->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\AcademicPeriod;
use App\GroupModule;
use App\Http\Response;
use App\Person;
use App\Student;
use App\StudentGroup;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group Grupo de estudiantes
 */
class StudentGroupController extends Controller
{
    public $response;
    public function __construct()
    {
        $this->response = new Response();
    }
    /**
     * Obtener grupo
     *
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": { "group": {"id": "int", "code": "String", "module_id": "int"} }
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     */
    public function index()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person()->first();
                $student = $person->student()->first();
                $academicPeriod = AcademicPeriod::whereRaw("? BETWEEN academic_periods.start_date AND academic_periods.end_date", [date('Y-m-d')])->first();
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $group = $student->groups($academicPeriod->id)->first();
                return $group != null ? $this->response->ok(compact('group')) : $this->response->badRequest('Este estudiante no está asigando a ningún grupo');
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar Grupos de práctica
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * group : {"name": "String", "module_id": "int", "students": "Array<String>"}
     * 
     * @bodyParam groups Array<group> required Lista de grupos a registrar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"groups": [{"id": "int", "code": "String", "name": "String", "module_id": "int", "academic_period_id": "int"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function store(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $groups = [];
            for ($i = 0; $i < count($request->input('groups')); $i++) {
                $groupModule = new GroupModule();
                $groupModule->code = strval(strtotime('now') + $i);
                //$year.''.$period. $request->input('module_id').$request->input('groups')[$i]['groupId'];
                $groupModule->name = $request->input('groups')[$i]['name'];
                $groupModule->module_id = $request->input('groups')[$i]['module_id'];
                $groupModule->academic_period_id = $academicPeriod->academic_period_id;
                $groupModule->save();
                if ($groupModule) {
                    for ($j = 0; $j < count($request->input('groups')[$i]['students']); $j++) {
                        $person = Person::where('document', $request->input('groups')[$i]['students'][$j])->first();
                        $studentGroup = new StudentGroup();
                        $studentGroup->student_id = $person->student()->first()->id;
                        $studentGroup->group_module_id = $groupModule->id;
                        $studentGroup->save();
                    }
                }
                array_push($groups, $groupModule);
            }
            DB::commit();
            return $this->response->ok(compact('groups'));
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Registrar estudiante a grupo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * 
     * @bodyParam students Array<group> required Lista de grupos a registrar
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"students": "Array<int>"}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Error de servidor",
     *      "data": null
     * }
     * 
     */
    public function addStudentGroup(Request $request)
    {
        try {
            DB::beginTransaction();
            $findStudentGroup = StudentGroup::where('group_module_id', '=', $request->input('groupId'))->firstOrFail();
            for ($j = 0; $j < count($request->input('students')); $j++) {
                $studentGroup = new StudentGroup();
                $studentGroup->academic_period_id = $findStudentGroup->academic_period_id;
                $studentGroup->student_id = $request->input('students')[$j];
                $studentGroup->group_module_id = $findStudentGroup->group_module_id;
                $studentGroup->save();
            }
            DB::commit();
            return $this->response->ok(compact($request));
        } catch (QueryException $e) {
            return $this->response->badRequest($e);
        }
    }
    /**
     * Listar grupos de prácticas por el id del grupo
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"studentsGroup": "Array<Student> con person"}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Este grupo no tiene estudiantes",
     *      "data": null
     * }
     * 
     */
    public function studentGroup(int $groupId)
    {
        try {
            $group = GroupModule::find($groupId);
            $students = $group->students()->get();
            $studentsGroup = [];
            foreach ($students as &$student) {
                $studentClass = Student::find($student->id);
                $studentClass->person = $studentClass->person()->first();
                array_push($studentsGroup, $studentClass);
            }
            return count($studentsGroup) > 0 ?
                $this->response->ok(compact('studentsGroup'))
                : $this->response->badRequest('Este grupo no tiene estudiantes');
        } catch (QueryException $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar grupos de prácticas por el módulo
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"groups": [{"name": "String", "module_id": "int", "students": "Array<Student>"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Este grupo no tiene estudiantes",
     *      "data": null
     * }
     * 
     */
    public function studentGroupByModule(Request $request, int $moduleId)
    {
        $career_id = $request->header('career');
        try {
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            $groups = GroupModule::where('academic_period_id', $academicPeriod->academic_period_id)->where('module_id', $moduleId)->get();
            if ($groups) {
                foreach ($groups as &$group) {
                    $students = $group->students()->get();
                    $group->students = [];
                    $group->assign = $group->assignStudentGroup()->first();
                    $studentGroups = [];
                    foreach ($students as &$student) {
                        $studentClass = Student::find($student->id);
                        $studentClass->person = $studentClass->person()->first();
                        array_push($studentGroups, $studentClass);
                    }
                    $group->students = $studentGroups;
                }

                return count($groups) > 0 ?
                    $this->response->ok(compact('groups'))
                    : $this->response->badRequest('Este grupo no tiene estudiantes');
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }

    /**
     * Listar grupos de prácticas por el profesor
     * 
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"studentsGroup": {"person": "Object<person>", "checks": "int"}}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Este grupo no tiene estudiantes",
     *      "data": null
     * }
     * 
     */
    public function studentGroupByTeacher(Request $request)
    {
        $career_id = $request->header('career');
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message' => 'user_not_found'], 404);
            } else {
                $person = $user->person()->first();
                $teacher = $person->teacher()->first();
                $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
                if (!$academicPeriod) {
                    return $this->response->badRequest('No existe periodo académico vigente');
                }
                $assignment = $teacher->assignment($academicPeriod->academic_period_id)->first();
                $rotation = $assignment->rotations()
                    ->whereRaw("? BETWEEN rotations.start_date AND rotations.end_date", [date('Y-m-d')])->first();
                $groupModule = $assignment->groupModule($rotation->id)->first();
                $students = $groupModule->students()->get();
                $studentsGroup = [];
                foreach ($students as &$student) {
                    $studentClass = Student::find($student['id']);
                    $studentClass->person = $studentClass->person()->first();
                    $studentClass->checks = $studentClass->person->countCheckByEvaluated($studentClass->person->id, $career_id, $rotation->id)->count();
                    array_push($studentsGroup, $studentClass);
                }
                return count($studentsGroup) > 0 ?
                    $this->response->ok(compact('studentsGroup'))
                    : $this->response->badRequest('Este grupo no tiene estudiantes');
            }
        } catch (Exception $e) {
            return $this->response->badRequest($e->getMessage());
        }
    }
    /**
     * Actualizar grupo de práctica
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * group: {"code": "String", "students": "Array<String>"}
     * @bodyParam groups Array<group> required Array de grupos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"groups": [{"name": "String", "module_id": "int", "students": "Array<Student>"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Este grupo no tiene estudiantes",
     *      "data": null
     * }
     * 
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $groups = [];
            foreach ($request->input('groups') as &$group) {
                $groupModule = GroupModule::where('code', $group['code']);
                if ($groupModule) {
                    foreach ($group['students'] as &$student) {
                        $person = Person::where('document', $student)->first();
                        $student = $person->student()->first()->id;
                        $studentGroup = StudentGroup::where('student_id', $student)->where('group_module_id', $groupModule->id)->first();
                        $studentGroup->student_id = $person->student()->first()->id;
                        $studentGroup->update();
                    }
                }
                array_push($groups, $groupModule);
            }
            return $this->response->ok(compact('groups'));
        } catch (QueryException $e) {
            return $this->response->badRequest($e);
        }
    }

    /**
     * Actualizar grupo de práctica
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * group: {"code": "String", "students": "Array<String>"}
     * @bodyParam groups Array<group> required Array de grupos
     * 
     * @response {
     *      "state": true,
     *      "message": "ok",
     *      "data": {"groups": [{"name": "String", "module_id": "int", "students": "Array<Student>"}]}
     * }
     * 
     * @response 400 {
     *      "state": false,
     *      "message": "Este grupo no tiene estudiantes",
     *      "data": null
     * }
     * 
     */
    public function updateGroup(Request $request)
    {
        $career_id = $request->header('career');
        try {
            DB::beginTransaction();
            $groups = [];
            $academicPeriod = (new AcademicPeriodController())->getAcademicPeriod($career_id);
            if (is_null($academicPeriod)) {
                return $this->response->badRequest('No existe periodo académico vigente');
            }
            foreach ($request->input('groups') as &$group) {
                $groupModule = GroupModule::where('code', $group['code'])->first();
                $groupModuleId = $groupModule->id;
                if ($groupModule) {
                    foreach ($group['students'] as &$student) {
                        $person = Person::where('document', $student)->first();
                        $studentAux = $person->student()->first();
                        $groupStudent = $studentAux->group($academicPeriod->academic_period_id)->first();
                        $studentGroup = StudentGroup::where('group_module_id', $groupStudent->id)
                            ->where('student_id', $studentAux->id)
                            ->first()->update(['group_module_id' => $groupModuleId]);
                    }
                }
                array_push($groups, $groupModule);
            }
            DB::commit();
            return $this->response->ok(compact('groups'));
        } catch (QueryException $e) {
            return $this->response->badRequest($e);
        }
    }
}

<?php


namespace App\Http;


class Response
{
    public function ok($data, $state = true)
    {
        return response()->json([
            'message' => 'ok',
            'state' => $state,
            'data' => $data
        ], 200);
    }

    public function badRequest($message = null)
    {
        return response()->json([
            'message' => $message ? $message : 'Error de aplicación consulte con el administrador del sistema',
            'state' => false,
            'data' => null
        ], 400);
    }
    public function unauthorized($message = null)
    {
        return response()->json([
            'message' => $message ? $message : 'No estas autorizado',
            'state' => false,
            'data' => null
        ], 401);
    }
}

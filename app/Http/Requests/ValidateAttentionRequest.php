<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateAttentionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return arra60y
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'procedures' => 'required|array'
        ];
    }

    public function messages()
    {
        return [
            'date.required'         => 'La fecha es obligatoria',
            'procedures.required'   => 'los procedimientos son requeridos',
            'date.date'             => 'Esta fecha no es valida',
            'procedures.array'      => 'Debe ser un array',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

<?php

namespace App\Http\Requests;

use App\Enums\Roles;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user"          => "required|email|unique:users,user",
            "password"      => "required",
            "rol"           => "required|in:1,2,3,4,5",
        ];
    }

    public function messages()
    {
        return [
            "user.required"         => "El usuario es requerido",
            "user.email"            => "Este campo tiene que ser tipo email",
            "user.unique"           => "Este usuario ya existe",
            "password.required"     => "La contraseña es requerida",
            "user_parent.required"  => "El campo quien lo registro es requerido",
            "rol.required"          => "El rol es requerido",
            "rol.in"                => "El rol debe estar en una de estas opciones"
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

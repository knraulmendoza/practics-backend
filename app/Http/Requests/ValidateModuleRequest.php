<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateModuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"          => "required|unique:modules,name",
            'code'          => 'required|max:7|unique:modules,code,' . $this->id,
            "subject_code"    => "required"
        ];
    }
    public function messages()
    {
        return [
            'name.unique'         => 'Este modulo ya existe',
            'name.required'       => 'El nombre es obligatorio',
            'code.required'       => 'El código es requerido',
            'code.max'            => 'El código es maximo de 7 caracteres',
            'code.unique'         => 'Este código ya existe',
            'subject_code.required' => 'La asignatura es obligatoria'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

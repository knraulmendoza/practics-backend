<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidatePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person.document_type' => 'required|in:1,2,3',
            'person.document' => 'required|min:6|max:11|unique:people,document',
            'person.first_name' => 'required|min:3|max:25',
            'person.first_last_name' => 'required|min:3|max:30',
            'person.second_last_name' => 'required|min:3|max:30',
            'person.phone' => 'min:10|max:11',
            'person.document_type' => 'required|numeric|min:1|max:1',
            'person.address' => 'required',
            'person.gender' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'person.document_type.in' => 'Debe seleccionar una de estas opciones',
            'person.document_type.required' => 'El tipo de documento es obligatorio',
            'person.document_type.min' => 'El valor seleccionado del tipo de documento no es correcto',
            'person.document_type.max' => 'El valor seleccionado del tipo de documento no es correcto',
            'person.document.required' => 'La cedula es requerida',
            'person.document.min' => 'la cedula debe tener minimo 6 digitos',
            'person.document.max' => 'la cedula debe tener maximo 11 digitos',
            'document.unique' => 'El documento esta repetido',
            'person.first_name.required' => 'El primer nombre es obligatorio',
            'person.first_name.min' => 'El primer nombre debe tener mínimo 3 carácteres',
            'person.first_name.max' => 'El primer nombre debe tener máximo 25 carácteres',
            'person.second_name.min' => 'El segundo nombre debe tener mínimo 3 carácteres',
            'person.second_name.max' => 'El segundo nombre debe tener máximo 25 carácteres',
            'person.first_last_name.required' => 'El primer apellido es obligatorio',
            'person.first_last_name.min' => 'El primer apellido debe tener mínimo 3 carácteres',
            'person.first_last_name.max' => 'El primer apellido debe tener máximo 30 carácteres',
            'person.first_second_name.required' => 'El segundo apellido es obligatorio',
            'person.first_second_name.min' => 'El segundo apellido debe tener mínimo 3 carácteres',
            'person.first_second_name.max' => 'El segundo apellido debe tener máximo 30 carácteres',
            'person.phone.numeric' => 'En el número telefónio solo se admiten números',
            'person.phone.min' => 'El número telefónico debe tener minimo 10 dígitos',
            'person.phone.max' => 'El número telefónico debe ser de 10 dígitos',
            'person.gender.required' => 'El género es obligatorio',
            'person.address.required' => 'La dirección es obligatoria'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

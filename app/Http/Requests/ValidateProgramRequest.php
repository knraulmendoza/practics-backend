<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => ['required', 'max:10', 'unique:careers,code'],
            'name' => 'required|unique:careers,name'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'El código del programa es requerido',
            'code.min' => 'El código debe ser de mínimo 3 caracteres',
            'code.max' => 'El código debe ser de máximo 10 caracteres',
            'code.unique' => 'El código debe ser único',
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre debe ser único'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

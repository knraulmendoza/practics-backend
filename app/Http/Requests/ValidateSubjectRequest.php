<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateSubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|max:7|unique:subjects,code',
            'name' => 'required|unique:subjects,name',
        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'El código es requerido',
            'code.max' => 'El código es max de 7 caracteres',
            'code.unique' => 'Este código ya existe',
            'name.unique' => 'Esta asignatura ya existe',
            'name.required' => 'El nombre es obligatorio',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

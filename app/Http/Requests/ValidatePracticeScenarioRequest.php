<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidatePracticeScenarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|min:5',
            'code' => 'required|max:6|min:1|unique:practice_scenarios,code',
            'complexity_level' => 'numeric',
            'scene_type_id' => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del escenario de práctica es requerido',
            'name.max' => 'El tamaño máximo del nombre es de 45 caracteres',
            'name.min' => 'El tamaño mínimo del nombre es de 45 caracteres',
            'code.required' => 'El código es requerido',
            'code.max' => 'El tamaño máximo del código es 6',
            'code.min' => 'El tamaño mínimo del código es 1',
            'complexity_level.numeric' => 'El nivel de complejidad debe ser numérico',
            'scene_type_id.numeric' => 'El tipo de escenario debe ser numérico'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

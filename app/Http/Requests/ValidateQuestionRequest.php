<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class ValidateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:1,2',
            'question' => [
                'required',
                'unique:questions,question,' . $this->id
            ],
            'check_type' => 'in:students,teachers,"students,teachers",""'
        ];
    }

    public function messages()
    {
        return [
            'type.required'       => 'El tipo es requerido',
            'question.required'       => 'El nombre es obligatorio',
            'subject_id.required' => 'La asignatura es obligatoria'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $fillable = ['observation'];
    public function observationable()
    {
        return $this->morphTo();
    }
}

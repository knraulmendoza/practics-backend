<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherTitle extends Model
{
    protected $table = "teachers_titles";
    protected $fillable = ["teacher_id","title_id"];
}

<?php

namespace App;

use App\Enums\StateAttention;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = ['document_type', 'document', 'name'];

    public function attentions()
    {
        return $this->hasMany(Attention::class);
    }

    public function patientsCountByModule(int $moduleId)
    {
        return Patient::join('attentions', 'patients.id', 'attentions.patient_id')
            ->join('students', 'attentions.student_id', 'students.id')
            ->join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->where('groups_modules.module_id', $moduleId)->groupBy('patients.id');
    }

    public function patientsCountBySubject(int $subjectId)
    {
        return Patient::join('attentions', 'patients.id', 'attentions.patient_id')
            ->join('students', 'attentions.student_id', 'students.id')
            ->join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->join('modules', 'groups_modules.module_id', 'modules.id')
            ->where('modules.subject_id', $subjectId)
            ->where('attentions.state', '!=', strval(StateAttention::AWAIT))
            ->groupBy('patients.id')->get();
    }

    public function patientsCountByScenario(int $scenarioId)
    {
        return Patient::join('attentions', 'patients.id', 'attentions.patient_id')
            ->join('assignments', 'attentions.assignment_id', 'assignments.id')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.module_id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
            ->join('areas', 'psa.area_id', 'areas.id')
            ->where('psa.practice_scenarios_id', $scenarioId)
            ->where('attentions.state', '!=', strval(StateAttention::AWAIT))
            ->groupBy('patients.id')->get();
    }
}

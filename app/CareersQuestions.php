<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareersQuestions extends Model
{
    protected $table = 'careers_questions';
    protected $fillable = ['career_id', 'question_id'];
}

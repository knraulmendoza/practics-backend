<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    protected $fillable = ['name', 'description'];

    public function practiceScnarios(){
        return $this->belongsToMany(PracticeScenario::class, 'practice_scenarios_areas','area_id')->get();
    }
}

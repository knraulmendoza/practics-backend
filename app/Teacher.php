<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['person_id'];


    public function person(){
        return $this->belongsTo(Person::class,'person_id');
    }

    public function titles(){
        return $this->belongsToMany(Title::class,'teachers_titles');
    }

    public function assignment(int $academicPeriodId)
    {
        return $this->hasMany(Assignment::class)->where('assignments.academic_period_id',$academicPeriodId);
    }
}

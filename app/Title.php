<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Title extends Model
{
    protected $table = "titles";
    protected $fillable = ["name","description"];

    public function teachers(){
        return $this->belongsToMany(teacher::class,'teachers_titles');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPracticeScenarioAcademicPeriod extends Model
{
    protected $table = 'career_practice_scenario_academic_period';
    protected $fillable = ['career_practice_scenario_id', 'academic_period_id', 'quotas'];
}

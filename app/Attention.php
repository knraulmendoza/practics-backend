<?php

namespace App;

use App\Enums\StateAttention;
use Illuminate\Database\Eloquent\Model;

class Attention extends Model
{
    protected $fillable = ['id', 'date', 'state', 'student_id', 'academic_period_id', 'patient_id'];

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function procedures()
    {
        return $this->belongsToMany(Procedure::class, 'procedures_attentions');
    }

    public function attentionCount(int $moduleId)
    {
        return Attention::join('students', 'attentions.student_id', 'students.id')
            ->join('students_groups', 'students.id', 'students_groups.student_id')
            ->join('groups_modules', 'students_groups.group_module_id', 'groups_modules.id')
            ->where('groups_modules.module_id', $moduleId)
            ->where('attentions.state', '!=', strval(StateAttention::AWAIT))
            ->count();
    }

    public function attentionCountByScenario(int $scenarioId)
    {
        return Attention::join('assignments', 'attentions.assignment_id', 'assignments.id')
            ->join('practice_scenarios_areas_modules as psam', 'assignments.module_area_id', 'psam.module_id')
            ->join('practice_scenarios_areas as psa', 'psam.practice_scenarios_areas_id', 'psa.id')
            ->where('psa.practice_scenarios_id', $scenarioId)
            ->where('attentions.state', '!=', strval(StateAttention::AWAIT))->count();
    }

    public function poll()
    {
        return $this->hasOne(PatientPoll::class);
    }
}

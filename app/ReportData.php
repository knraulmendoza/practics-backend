<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportData extends Model
{
    protected $table = 'report_datas';
    protected $fillable = ['id', 'name', 'total', 'report_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = ['code','name'];

    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }
    
    public function subjects(){
        return $this->belongsToMany(Subject::class, 'careers_subjects');
    }

    public function modules(){
        return $this->belongsToMany(Module::class,'careers_subjects');
    }

    public function person(){
        return $this->belongsToMany(Person::class, 'people_careers');
    }

    public function questions(){
        return $this->belongsToMany(Question::class, 'careers_questions');
    }
}

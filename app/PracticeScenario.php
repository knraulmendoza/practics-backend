<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PracticeScenario extends Model
{
    protected $table = 'practice_scenarios';
    protected $fillable = ['id', 'name','description','complexity_level','scene_type', 'entity'];

    public function scenarios(int $careerId, string $moduleId = '') {
        $scenarios = PracticeScenario::select('practice_scenarios.*')
        ->join('practice_scenarios_areas as psa', 'psa.practice_scenarios_id', '=', 'practice_scenarios.id')
        ->join('practice_scenarios_areas_modules as psam', 'psam.practice_scenarios_areas_id', '=', 'psa.id')
        ->join('modules', 'psam.module_id', '=', 'modules.id')
        ->join('subjects', 'modules.subject_id', '=', 'subjects.id')
        ->join('careers_subjects as cs', function ($join) {
            $join->on('cs.subject_id', '=', 'subjects.id')->On('modules.career_id', '=', 'cs.career_id');
        })
        ->join('careers', 'cs.career_id', '=', 'careers.id')
        ->groupBy('practice_scenarios.id')
        ->where('careers.id', $careerId);
        if ($moduleId) {
            $scenarios->Where('modules.id',$moduleId);
        }
        return $scenarios->get();
    }

    public function scenariosPaginate(int $careerId, int $per_page, string $moduleId = '') {
        $scenarios = PracticeScenario::select('practice_scenarios.*')
        ->join('practice_scenarios_areas as psa', 'psa.practice_scenarios_id', '=', 'practice_scenarios.id')
        ->join('practice_scenarios_areas_modules as psam', 'psam.practice_scenarios_areas_id', '=', 'psa.id')
        ->join('modules', 'psam.module_id', '=', 'modules.id')
        ->join('subjects', 'modules.subject_id', '=', 'subjects.id')
        ->join('careers_subjects as cs', function ($join) {
            $join->on('cs.subject_id', '=', 'subjects.id')->On('modules.career_id', '=', 'cs.career_id');
        })
        ->join('careers', 'cs.career_id', '=', 'careers.id')
        ->groupBy('practice_scenarios.id')
        ->where('careers.id', $careerId);
        if ($moduleId) {
            $scenarios->Where('modules.id',$moduleId);
        }
        return $scenarios->paginate(8);
    }

    public function scenariosByType(int $careerId, $sceneType) {
        $scenarios = PracticeScenario::select('practice_scenarios.*')
        ->join('practice_scenarios_areas as psa', 'psa.practice_scenarios_id', '=', 'practice_scenarios.id')
        ->join('practice_scenarios_areas_modules as psam', 'psam.practice_scenarios_areas_id', '=', 'psa.id')
        ->join('modules', 'psam.module_id', '=', 'modules.id')
        ->join('careers', 'modules.career_id', '=', 'careers.id')
        ->groupBy('practice_scenarios.id')
        ->where('careers.id', $careerId)
        ->where('practice_scenarios.scene_type', $sceneType);
        return $scenarios->get();
    }

    public function scenariosType(){
        return $this->belongsTo(ScenarioType::class,'scene_type');
    }

    public function areas(){
        return $this->belongsToMany(Area::class, 'practice_scenarios_areas', 'practice_scenarios_id')->get();
    }

}

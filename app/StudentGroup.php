<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentGroup extends Model
{
    protected $table = 'students_groups';
    protected $fillable = ['student_id', 'group_module_id'];
}

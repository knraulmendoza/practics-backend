<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicPeriodCareer extends Model
{
    protected $table = 'academic_periods_careers';
    protected $fillable = ['academic_period_id', 'career_id', 'state','start_date', 'end_date'];
}

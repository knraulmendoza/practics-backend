<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleCareers extends Model
{
    protected $table = 'people_careers';
    protected $fillable = ['person_id','career_id'];
}

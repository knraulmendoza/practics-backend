<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPractict extends Model
{
    protected $table = 'events_practicts';
    protected $fillable= ['title', 'description', 'date', 'hour', 'address', 'state', 'image'];
}

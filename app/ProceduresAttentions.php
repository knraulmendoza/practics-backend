<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProceduresAttentions extends Model
{
    protected $fillable = ['procedure_id', 'attention_id'];
}

<?php

namespace App;

use App\Enums\StatesChecks;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'people';
    protected $fillable = [
        'document_type', 'document', 'first_name', 'second_name',
        'first_last_name', 'second_last_name', 'address', 'gender', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function careers()
    {
        return $this->belongsToMany(Career::class, 'people_careers');
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }

    public function coordinator()
    {
        return $this->hasOne(Coordinator::class);
    }

    public function student()
    {
        return $this->hasOne(Student::class);
    }

    public function hasCarrers($carrer)
    {
        return $this->careers()->where('id', $carrer)->count() > 0;
    }

    public function checkByEvaluated($evaluator)
    {
        return $this->hasMany(Check::class, 'evaluated')->where('evaluator', $evaluator)->where('state', StatesChecks::CREADA)->first();
    }

    public function checkByEvaluatedAll($academic_period_id)
    {
        return $this->hasMany(Check::class, 'evaluated')
            ->where('academic_period_id', $academic_period_id)
            ->whereIn('state', [StatesChecks::CREADA, StatesChecks::REALIZADA])->get();
    }

    public function countCheckByEvaluated($evaluated, $careerId, $rotationId = null)
    {
        $person = Person::select('checks.*')
            ->join('checks', 'checks.evaluated', 'people.id')
            ->join('checks_rotations', 'checks_rotations.check_id', 'checks.id')
            ->join('rotations', 'rotations.id', 'checks_rotations.rotation_id')
            ->where('people.id', $evaluated)->where('rotations.career_id', $careerId)->whereIn('checks.state', [StatesChecks::REALIZADA, StatesChecks::CHEQUEADO]);
        if ($rotationId != null) {
            $person->where('rotations.id', $rotationId);
        }
        return $person;
    }

    public function checkByEvaluator($evaluated)
    {
        return $this->hasMany(Check::class, 'evaluator')->where('evaluated', $evaluated)->where('state', StatesChecks::CREADA)->first();
    }

    public function checksByEvaluator($evaluated)
    {
        return $this->hasMany(Check::class, 'evaluated')->where('evaluated', $evaluated)->get();
    }
}

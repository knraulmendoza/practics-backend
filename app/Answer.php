<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer', 'question_id'];
    public function answerable()
    {
        return $this->morphTo();
    }
    
    public function observation()
    {
        return $this->morphOne(Observation::class,'observationable');
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}

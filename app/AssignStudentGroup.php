<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignStudentGroup extends Model
{
    protected $table = 'assign_student_groups';
    protected $fillable = ['group_module_id', 'rotation_id', 'assignment_id', 'working_day'];

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    protected $fillable = ['id', 'date', 'evaluated', 'evaluator', 'academic_period_id', 'state'];

    public function person()
    {
        return $this->hasOne(Person::class);
    }

    public function evaluated()
    {
        return $this->belongsTo(Person::class, 'evaluated');
    }

    public function evaluator()
    {
        return $this->belongsTo(Person::class, 'evaluator');
    }

    public function answers()
    {
        return $this->morphMany(Answer::class,'answerable');
    }
    public function answer(int $questionId)
    {
        return $this->morphMany(Answer::class,'answerable')->where('question_id', $questionId);
    }

    public function checksRotation(int $rotation)
    {
        return $this->belongsToMany(Rotation::class, 'checks_rotations')->where('rotations.id', $rotation);
    }

    public function checksRotations(int $rotation)
    {
        return $this->hasMany(CheckRotation::class)->where('checks_rotations.rotation_id', $rotation);
    }

    public function observations()
    {
        return $this->morphMany(Observation::class,'observationable');
    }

    public function periodAcademic()
    {
        return $this->hasOne(AcademicPeriod::class);
    }
}

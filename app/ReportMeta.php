<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportMeta extends Model
{
    protected $fillable = ['id', 'key', 'value', 'report_id', 'tabla'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupModule extends Model
{
    protected $table = 'groups_modules';
    protected $fillable = ['code', 'module_id'];

    public function students()
    {
        return $this->belongsToMany(Student::class, 'students_groups', 'group_module_id');
    }

    public function assignStudentGroup()
    {
        return $this->hasOne(AssignStudentGroup::class, 'group_module_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function rotation()
    {
        return $this->belongsToMany(Rotation::class, 'assign_student_groups')->whereRaw("? BETWEEN rotations.start_date AND rotations.end_date", [date('Y-m-d')]);
    }

    public function rotations()
    {
        return $this->belongsToMany(Rotation::class, 'assign_student_groups');
    }

    public function assignment()
    {
        return $this->belongsToMany(Assignment::class, 'assign_student_groups', 'group_module_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PracticeScenarioAreaModule extends Model
{
    protected $table = 'practice_scenarios_areas_modules';
    protected $fillable = ['practice_scenarios_areas_id', 'module_id'];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function scenario_area()
    {
        return $this->belongsTo(PracticeScenarioArea::class, 'practice_scenarios_areas_id');
    }

    public function scenarios_area()
    {
        return $this->belongsToMany(PracticeScenarioArea::class, 'practice_scenarios_areas', 'practice_scenarios_areas_id');
    }

    public function assignment()
    {
        return $this->hasOne(Assignment::class, 'module_area_id');
    }
}

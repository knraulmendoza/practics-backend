<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerPoll extends Model
{
    protected $table = 'answers_polls';
    protected $fillable = ['answer','patient_poll_id','question_id'];
}

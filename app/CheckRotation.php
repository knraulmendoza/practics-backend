<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckRotation extends Model
{
    protected $fillable = ['check_id', 'rotation_id'];
    protected $table = 'checks_rotations';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name','code','description', 'career_id', 'subject_id'];

    public function subjects(){
        return $this->belongsTo(Subject::class,'subject_id');
    }

    public function groups(int $periodoAcademicId)
    {
        return $this->hasMany(GroupModule::class)->where('academic_period_id', $periodoAcademicId);
    }

    public function practicScenarioAreaModule()
    {
        return $this->hasOne(PracticeScenarioAreaModule::class, 'module_id');
    }

    public function procedures(){
        return $this->hasMany(Procedure::class);
    }

    public function assignments(){
        return $this->hasMany(Assignment::class);
    }

    public function assignmentsAll(){
        return $this->belongsToMany(Assignment::class, 'assign_student_groups', 'group_module_id');
    }

    public function career(){
        return $this->belongsTo(Career::class);
    }
}

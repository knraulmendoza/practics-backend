<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    protected $fillable = ['person_id,type'];


    public function person(){
        return $this->belongsTo(Person::class,'person_id');
    }
}

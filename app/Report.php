<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['id', 'career_id', 'title', 'description', 'report_category', 'image'];

    public function reportMetas()
    {
        return $this->hasMany(ReportMeta::class);
    }
    public function reportDatas()
    {
        return $this->hasMany(ReportData::class);
    }
}

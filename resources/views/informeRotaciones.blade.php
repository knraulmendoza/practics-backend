<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
     integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Informe de rotaciones</title>
</head>
<body>
    <header>
        <div style="align-items: flex-start;">
            <img src="../storage/images/logo.png" class="rounded" alt="responsive image" width="240" height="70">
        </div>
        <h6>FACULTAD DE CIENCIAS DE LA SALUD</h6>
        <h6>COMITÉ DOCENCIA - SERVICIO</h6>
    </header>
    <div class="container">
        <p style="text-align: center;">CUADROS DE ROTACIÓN {{$semestre}} PERIODO {{$year}}</p>
        @foreach($assignments as $assignment)
        <table class="table table-bordered" >
            <tbody>
                <tr>
                    <td>INSTITUCIÓN</td>
                    <td>{{strtoupper($assignment->institution)}}</td>
                </tr>
                <tr>
                    <td>ASIGNATURA</td>
                    <td>{{strtoupper($assignment->subject)}}</td>
                </tr>
            </tbody>
         </table>
         <table class="table table-bordered">
         <thead>
                <tr>
                    <th scope="col">GRUPO</th>
                    <th scope="col">NOMBRE DEL ALUMNO</th>
                    <th scope="col">FECHA</th>
                    <th scope="col">ÁREA</th>
                    <th scope="col">DOCENTE</th>
                </tr>
             </thead>
            <tbody>
                @foreach($students as $student)
                @if($student->group_module_id == $assignment->module)
                <tr>
                    <td>{{$student->code}}</td>
                    <td>{{$student->first_name}} {{$student->second_name}} {{$student->first_last_name}} {{$student->second_last_name}}</td>
                    <td>{{$student->date}}</td>
                    <td>{{$assignment->area}}</td>
                    <td>{{$assignment->first_name}} {{$assignment->first_last_name}}</td>
                </tr>
                @endif
                @endforeach
            </tbody>
         </table>
         @endforeach
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
     integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Informe de no asistencias</title>
</head>
<body>
    <header>
        <div style="align-items: flex-start;">
            <img src="../storage/images/logo.png" class="rounded" alt="responsive image" width="240" height="70">
        </div>
        <h6>FACULTAD DE CIENCIAS DE LA SALUD</h6>
        <h6>COMITÉ DOCENCIA - SERVICIO</h6>
    </header>
    <div class="container">
        <p style="text-align: center;">No asistencias {{$semestre}} PERIODO {{$year}}</p>
        <table class="table table-bordered" >
            <thead>
                <tr>
                    <th scope="col">CÓDIGO</th>
                    <th scope="col">NOMBRE DEL ALUMNO</th>
                    <th scope="col">FECHA</th>
                    <th scope="col">OBSERVACIÓN</th>
                </tr>
             </thead>
            <tbody>
            @foreach($noAssistences as $noAssistence)
                <tr>
                    <td>{{$noAssistence->document}}</td>
                    <td>{{strtoupper($noAssistence->first_name)}} {{$noAssistence->second_name}} {{$noAssistence->first_last_name}} {{$noAssistence->second_last_name}}</td>
                    <td>{{$noAssistence->date}}</td>
                    <td>{{$noAssistence->observation}}</td>
                </tr>
                @endforeach
            </tbody>
         </table>
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
     integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <title>Informe de atenciones</title>
</head>
<body>
    <header>
        <div style="align-items: flex-start;">
            <img src="../storage/images/logo.png" class="rounded" alt="responsive image" width="240" height="70">
        </div>
        <h6>    COMITÉ DOCENCIA - SERVICIO</h6>
    </header>
    <div class="container">
        <P style="text-align: center;">FACULTAD DE CIECIAS DE LA SALUD PROGRAMA DE {{strtoupper($career->name)}}</P>
        <br>
        <p style="text-align: center;">INFORME DE LA ATENCIÓN DURANTE LAS PRACTICAS FORMATIVAS {{$semestre}} SEMESTRE {{$year}}</p>
        <table class="table table-bordered" >
            <tbody>
                <tr>
                    <td>ASIGNATURA</td>
                    <td>{{strtoupper($subject->name)}}</td>
                </tr>
                <tr>
                    <td>USUARIOS ATENDIDOS</td>
                    <td></td>
                </tr>
                <tr>
                    <td>SITIOS DE PRÁCTICA</td>
                    <td>@foreach($institution as $institucion)
                            {{$institucion->name}},
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>No. DE ESTUDIANTES</td>
                    <td>{{sizeof($students)}}</td>
                </tr>
                <tr>
                    <td>DURACIÓN</td>
                    <td></td>
                </tr>
            </tbody>
         </table>
         <table class="table table-bordered">
             <thead>
                <tr>
                    <th scope="col">ACTIVIDADES</th>
                    <th scope="col">CANTIDAD</th>
                </tr>
             </thead>
            <tbody>
                @foreach($procedures as $actividad)
                <tr>
                    <td>{{$actividad->name}}</td>
                    <td>{{$actividad->total}}</td>
                </tr>
                @endforeach
            </tbody>
         </table>
         <div id="chart_div"></div>
    </div>
</body>
</html>
<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

  
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['Mushrooms', 3],
        ['Onions', 1],
        ['Olives', 1],
        ['Zucchini', 1],
        ['Pepperoni', 2]
    ]);

    // Set chart options
    var options = {'title':'Atenciones',
                    'width':400,
                    'height':300};s

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
    }
</script>
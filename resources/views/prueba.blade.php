<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <title>Prueba de pdf</title>
</head>
<body>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Código</th>
                <th scope="col">Nombre</th>
                </tr>
            </thead>
            <tbody>
                @foreach($careers as $career)
                <tr>
                    <th scope="row">{{$career->id}}</th>
                    <td>{{$career->code}}</td>
                    <td>{{$career->name}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        <!-- <img src="{{public_path($file)}}" height="100"> -->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>

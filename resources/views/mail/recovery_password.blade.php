<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Recuperar contraseña</title>
</head>
<body>
    <h1>Recuperando la contraseña</h1>
    <strong>usuario: </strong> {{ $user->user}} <br />
    <strong>Contraseña: </strong> {{ $user->password }}
</body>
</html>

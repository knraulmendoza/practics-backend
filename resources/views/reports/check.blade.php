<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Informe de rotaciones</title>
    <style type="text/scss">
        .banner img {
            display: inline;
            width: 300px;
        }
        .banner p {
            display: inline;
            position: absolute;
            right: 0;
            padding: 0;
        }
        .title {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        .subtitle {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        table, td, th {
            border: 1px solid black;
            padding: 5px;
        }

        table {
            empty-cells: hide;
            width: 100%;
        }
        .table-row {
            height: 10px;
        }
        .table-title {
            width: 30%;
            font-weight: bold;
        }
        .tale-value {
            width: 70%;
        }
        .width-title {
            width: 70% !important;
        }
        .width-value {
            width: 30% !important;
        }
        img {
            width: 100%;
            text-align: center;
        }
        .description p {
            text-align: justify;
        }
        .chart {
            max-height: 500px;
        }
    </style>
</head>
<body>
    <div class="banner">
        <img src="{{public_path('images/logo.png')}}" alt="logo universidad popular del cesar" width="240" height="70">
        <p >Comité docencia - servicio</p>
    </div>
    <h1 class="center-align title">FACULTAD DE CIENCIAS DE LA SALUD</h1>
    <h2 class="center-align subtitle">programa de {{ $career->name }}</h2>
    <div class="container">
        <br />
        <h3 class="center-align title">{{$report->title}} {{ $report->metas['academic_period']->period }} semestre {{ $report->metas['academic_period']->year }}</h3>
        <table>
            <tbody>
                <tr class="table-row">
                    <td class="table-title">Asignatura</td>
                    <td class="table-value">{{ $report->metas['subject']->name }}</td>
                </tr>
                <tr class="table-row">
                    <td class="table-title">Módulo</td>
                    <td class="table-value">{{ $report->metas['module']->name }}</td>
                </tr>
                <tr>
                    <td class="table-title">Número de {{ $rol == 5? "estudiantes": "docentes" }}</td>
                    <td class="table-value">{{ count($dataRol) }}</td>
                </tr>
            </tbody>
        </table>
        <br />
        <div class="description">
            <h2 class="subtitle">Descripción</h2>
            {!! $report->description !!}
        </div>
        <br />
        <h2 class="subtitle">Resultado chequeos de {{ $rol == 5? "estudiantes": "docentes" }}</h2>
        @foreach($dataRol as $data)
        <table>
            <tbody>
                <tr>
                    <th colspan="4">{{ $data->person->fullName }}</th>
                </tr>
                @foreach($data->rotations as $keyRotation => $rotation)
                    <tr>
                        <td colspan="4" style="font-weight: bold;text-align: center;">Rotación {{ $keyRotation + 1 }}</td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="width: 20%; font-weight: bold;">Chequeo</td>
                        <td colspan="2" style="width: 20%; font-weight: bold;">Resultado</td>
                        <td rowspan="2" style="width: 60%; font-weight: bold;">Descripción</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">si</td>
                        <td style="font-weight: bold;">no</td>
                    </tr>
                    @if (count($rotation->checks) == 0)
                        <tr class="table-row">
                            <td colspan="4" style="text-align: center;">No hay chequeos</td>
                        </tr>
                    @else
                        @foreach($rotation->checks as $key => $check)
                        <tr class="table-row">
                            <td >{{ $key + 1 }}</td>
                            <td >{{ $check->yes }}</td>
                            <td >{{ $check->no }}</td>
                            <td >{{ $check->observation }}</td>
                        </tr>
                        @endforeach
                    @endif
                @endforeach
            </tbody>
        </table>
        <br />
        <br />
        @endforeach
        
    </div>
</body>
</html>

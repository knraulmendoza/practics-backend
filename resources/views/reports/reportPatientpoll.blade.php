<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Informe de rotaciones</title>
    <style type="text/scss">
        .banner img {
            display: inline;
            width: 300px;
        }
        .banner p {
            display: inline;
            position: absolute;
            right: 0;
            padding: 0;
        }
        .title {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        .subtitle {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        table, td {
            border-bottom: 1px solid black;
            padding: 5px;
        }

        table {
            empty-cells: hide;
            width: 100%;
        }
        .table-row {
            height: 15px;
        }
        .table-title {
            width: 30%;
            font-weight: bold;
        }
        .tale-value {
            width: 70%;
        }
        .width-title {
            width: 40% !important;
        }
        .width-value {
            width: 60% !important;
        }
        img {
            width: 100%;
            text-align: center;
        }
        .description p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <div class="banner">
        <img src="{{public_path('images/logo.png')}}" alt="logo universidad popular del cesar" width="240" height="70">
        <p >Comité docencia - servicio</p>
    </div>
    <h1 class="center-align title">FACULTAD DE CIENCIAS DE LA SALUD</h1>
    <h2 class="center-align subtitle">programa de {{ $career->name }}</h2>
    <div class="container">
        <br />
        <h3 class="center-align title">{{$report->title}} {{ $report->metas['academic_period']->period }} semestre {{ $report->metas['academic_period']->year }}</h3>
        <table>
            <tbody>
                <tr class="table-row">
                    <td class="table-title">Asignatura</td>
                    <td class="table-value">{{ $report->metas['subject']->name }}</td>
                </tr>
                <tr class="table-row">
                    <td class="table-title">Módulo</td>
                    <td class="table-value">{{ $report->metas['module']->name }}</td>
                </tr>
                <tr>
                    <td class="table-title">grupos</td>
                    <td class="table-value">{{ count($groups) }}</td>
                </tr>
            </tbody>
        </table>
        <br />
        <div class="description">
            <h2 class="subtitle">Descripción</h2>
            {!! $report->description !!}
        </div>
        <br>
        @foreach($groups as $group)
        <h4 class="subtitle">{{$group->name}}</h4>
        @foreach($group->students as $student)
            <ul style="list-style:none">
                <li style="list-style:none">
                    Estudiante {{$student->person->fullName}}
                    <div>
                    <ul style="list-style:none">
                        <li style="list-style:none">Total Atenciones {{ $student->attentionsAll }}</li>
                        <li style="list-style:none">Total Atenciones aceptadas {{ $student->attentionsAccepted }}</li>
                        <li style="list-style:none">Total Atenciones encuestadas {{ $student->attentionsEvaluated }}</li>
                    </ul>
                    </div>
                </li>
            </ul>
        @endforeach
        @endforeach
    </div>
</body>
</html>

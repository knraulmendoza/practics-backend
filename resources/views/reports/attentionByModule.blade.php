<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Informe de rotaciones</title>
    <style type="text/scss">
        .banner img {
            display: inline;
            width: 300px;
        }
        .banner p {
            display: inline;
            position: absolute;
            right: 0;
            padding: 0;
        }
        .title {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        .subtitle {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        table, td {
            border-bottom: 1px solid black;
            padding: 5px;
        }

        table {
            empty-cells: hide;
            width: 100%;
        }
        .table-row {
            height: 10px;
        }
        .table-title {
            width: 30%;
            font-weight: bold;
        }
        .tale-value {
            width: 70%;
        }
        .width-title {
            width: 70% !important;
        }
        .width-value {
            width: 30% !important;
        }
        img {
            width: 100%;
            text-align: center;
        }
        .description p {
            text-align: justify;
        }
        .chart {
            max-height: 500px;
        }
    </style>
</head>

<body>
    <div class="banner">
        <img src="{{public_path('images/logo.png')}}" alt="logo universidad popular del cesar" width="240" height="70">
        <p>Comité docencia - servicio</p>
    </div>
    <h1 class="center-align title">FACULTAD DE CIENCIAS DE LA SALUD</h1>
    <h2 class="center-align subtitle">PROGRAMA DE {{ strtoupper($career->name) }}</h2>
    <div class="container">
        <br />
        <h3 class="center-align title">{{ $report->title ? strtoupper($report->title) : '' }} {{ $report->metas['academic_period']->period }} semestre {{ $report->metas['academic_period']->year }}</h3>
        <table>
            <tbody>
                <tr class="table-row">
                    <td class="table-title">Asignatura</td>
                    <td class="table-value">{{ $report->metas['subject']->name }}</td>
                </tr>
                <tr class="table-row">
                    <td class="table-title">Módulo</td>
                    <td class="table-value">{{ $report->metas['module']->name }}</td>
                </tr>
                <tr>
                    <td class="table-title">Usuarios atendidos</td>
                    <td class="table-value">{{ $patients }}</td>
                </tr>
                <tr>
                    <td class="table-title">Atenciones</td>
                    <td class="table-value">{{ $attentions }}</td>
                </tr>
                <tr>
                    <td class="table-title" rowspan="{{ count($report->scenarios) }}">Sitios de práctica</td>
                    <td class="table-value">{{ $report->scenarios[0]->abbreviation??$report->scenarios[0]->name }}</td>
                </tr>
                @foreach($report->scenarios as $key=>$scenario)
                @if($key > 0)
                <tr class="table-row">
                    <td class="table-value">{{ $scenario->abbreviation??$scenario->name }}</td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td class="table-title">Número de estudiantes</td>
                    <td class="table-value">{{ $students }}</td>
                </tr>
                <tr>
                    <td class="table-title">Duración</td>
                    <td class="table-value">{{ $report->duration->start_date }} a {{ $report->duration->end_date }}</td>
                </tr>
            </tbody>
        </table>
        <br />
        <img class="center-align chart" src="{{$report->image}}" alt="" srcset="">
        <table>
            <thead>
                <tr>
                    <th>Actividad</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($report->report_datas as $data)
                <tr class="table-row">
                    <td class="table-title width-title">{{$data->name}}</td>
                    <td class="table-value width-value">{{ $data->total }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <div class="description">
            <h2 class="subtitle">Descripción</h2>
            {!! $report->description ? $report->description : '' !!}
        </div>
    </div>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Informe de rotaciones</title>
    <style type="text/scss">
        .banner img {
            display: inline;
            width: 300px;
        }
        .banner p {
            display: inline;
            position: absolute;
            right: 0;
            padding: 0;
        }
        .title {
            font-size: 1.5rem;
            padding: 0;
            margin-top: 20px;
        }
        .subtitle {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        table, td {
            border-bottom: 1px solid black;
            padding: 5px;
        }

        table {
            empty-cells: hide;
            width: 100%;
        }
        .table-row {
            height: 10px;
        }
        .table-title {
            width: 30%;
            font-weight: bold;
        }
        .tale-value {
            width: 70%;
        }
        img {
            width: 100%;
            text-align: center;
        }
        .description p {
            text-align: justify;
        }
    </style>
</head>

<body>
    <div class="banner">
        <img src="{{public_path('images/logo.png')}}" alt="logo universidad popular del cesar" width="240" height="70">
        <p>Comité docencia - servicio</p>
    </div>
    <h1 class="center-align title">FACULTAD DE CIENCIAS DE LA SALUD</h1>
    <h2 class="center-align subtitle">programa de {{ $career->name }}</h2>
    <div class="container">
        <br />
        <h3 class="center-align title">{{$report->title}} {{ $report->metas['academic_period']->period }} semestre {{ $report->metas['academic_period']->year }}</h3>
        <table>
            <tbody>
                <tr class="table-row">
                    <td class="table-title">Asignatura</td>
                    <td class="table-value">{{ $report->metas['subject']->name }}</td>
                </tr>
                <tr class="table-row">
                    <td class="table-title" rowspan="{{ count($report->modules) }}">Módulos</td>
                    <td class="table-value">{{ $report->modules[0]->name }}</td>
                </tr>
                @foreach($report->modules as $key=> $module)
                @if($key > 0)
                <tr class="table-row">
                    <td class="table-value">{{ $module->name }}</td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td class="table-title">Número de estudiantes</td>
                    <td class="table-value">{{ $students }}</td>
                </tr>
                <tr>
                    <td class="table-title">Inicio de prácticas</td>
                    <td class="table-value">{{ $report->duration->start_date }}</td>
                </tr>
                <tr>
                    <td class="table-title">Fecha actual</td>
                    <td class="table-value">{{ date('Y-m-d') }}</td>
                </tr>
                <tr>
                    <td class="table-title">Días transcurridos</td>
                    <td class="table-value">{{ $days }}</td>
                </tr>
                <tr>
                    <td class="table-title">Asistencias Esperadas</td>
                    <td class="table-value">{{ $asistencesEsperadas }}</td>
                </tr>
                <tr>
                    <td class="table-title">Asistencias tomadas</td>
                    <td class="table-value">{{ $asistencesTomadas }}</td>
                </tr>
            </tbody>
        </table>
        <br />
        <div>
            <h3 class="title">Observación:</h3>
            @if ($asistencesEsperadas > $asistencesTomadas)
            <p>
                Se debe evaluar con el docente a cargo para verificar porque las asistencias esperadas no concuerdan con las tomadas
            </p>
            @endif
        </div>
        <table>
            <thead>
                <tr>
                    <th>Actividad</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($report->report_datas as $data)
                <tr class="table-row">
                    <td class="table-title width-title">{{$data->name}}</td>
                    <td class="table-value width-value">{{ $data->total }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <img width="600" class="center-align" src="{{$report->image}}" alt="" srcset="">

        <div class="description">
            <h2 class="subtitle">Descripción</h2>
            {!! $report->description !!}
        </div>
    </div>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Informe de rotaciones</title>
    <style type="text/scss">
        .banner img {
            display: inline;
            width: 300px;
        }
        .banner p {
            display: inline;
            position: absolute;
            right: 0;
            padding: 0;
        }
        .title {
            font-size: 1.5rem;
            padding: 0;
            margin-top: 20px;
        }
        .title span {
            text-transform: lowercase;
        }
        .subtitle {
            font-size: 1.5rem;
            padding: 0;
            margin: 0;
        }
        table, td {
            border-bottom: 1px solid grey;
            padding: 8px;
        }

        table {
            width: 100%;
        }
        .table-row {
            height: 10px;
        }

        .table-row td {
            font-size: 10.px;
        }
        .table-title {
            width: 40%;
            font-weight: bold;
        }
        .tale-value {
            width: 60%;
        }
        img {
            width: 100%;
            text-align: center;
        }
        .description p {
            text-align: justify;
        }
    </style>
</head>

<body>
    <div class="banner">
        <img src="{{public_path('images/logo.png')}}" alt="logo universidad popular del cesar" width="240" height="70">
        <p>Comité docencia - servicio</p>
    </div>
    <h1 class="center-align title">FACULTAD DE CIENCIAS DE LA SALUD</h1>
    <h2 class="center-align subtitle">Programa de {{ $career->name }}</h2>
    <div class="container-fluid">
        <br />
        <h3 class="center-align title">Cuadro de rotación <span>{{ count($rota) == 0 ? '' : $rota[0]->module }}</span> {{ $academicPeriod->period }} semestre {{ $academicPeriod->year }}</h3>
        @foreach($rota as $index=> $rotation)
        <h2 class="subtitle">Rotación {{ $index +1 }} ({{ $rotation->start_date }} / {{ $rotation->end_date  }})</h2>
        <table class="highlight">
            <thead>
                <tr>
                    <th>Estudiantes</th>
                    <th>Docente</th>
                    <th>Escenario</th>
                    <th>Area</th>
                    <th>Jornada</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rotation->groups as $group)
                <tr class="table-row">
                    <td>
                        @foreach($group->studentsGroup as $student)
                        <span>
                            {{ $student->person->first_name.' '.$student->person->first_last_name }}
                        </span> <br>
                        @endforeach
                    </td>
                    <td>
                        {{ $group->assignment->teacher->first_name.' '.($group->assignment->teacher->second_name==null?'':$group->assignment->teacher->second_name.' ').$group->assignment->teacher->first_last_name.' '.$group->assignment->teacher->second_last_name }}
                    </td>
                    <td>
                        {{ $group->assignment->scenario->name }}
                    </td>
                    <td>{{ $group->assignment->scenario->area->name }}</td>
                    <td>{{ $group->working_day }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br />
        @endforeach
    </div>
</body>

</html>
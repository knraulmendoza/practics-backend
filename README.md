# Practicts
## API

### Después de clonar este repositorio

1. Desde la carpeta raíz realizar en la consola de comandos ```composer install```
2. Para generar la llave secreta ```php artisan key:generate ```
3. Crear la base de datos mysql con el nombre de ```practics```
4. Ejecutar el comando ```php artisan migrate:refresh --seed```
5. Ejecutar el proyecto en local ```php artisan serve```

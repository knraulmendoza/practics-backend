<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'upc'], function () {
    Route::get('/subjectAll/{program}', 'UpcController@getSubjectAll');
    Route::get('/subject/{code}', 'UpcController@getSubject');
    Route::get('/period', 'UpcController@getPeriod');
    Route::get('/studentSubject/{subject}', 'UpcController@getStudentSubject');
    Route::get('/verifyUser/{email}', 'UpcController@verifyUser');
    Route::get('/faculties', 'UpcController@getFaculties');
    Route::get('/programs/{faculty}', 'UpcController@getPrograms');
    Route::get('/program/{code}', 'UpcController@getProgram');
});
Route::get('subject/asignatura/{program}', 'SubjectController@getSubjectUpc');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.auth']], function () {
    Route::get('/auth/getToken', 'AuthController@getAuthenticatedUser');
    Route::get('/auth/logout', 'AuthController@logout');
});
Route::group(['middleware' => []], function () {

    Route::group(['prefix' => 'coordinator'], function () {
        Route::get('/people', 'CoordinatorController@getPeople');
        Route::put('/{id}', 'CoordinatorController@update');
    });
    Route::get('/assignment/assignmentByModule/{module_id}', 'AssignmentController@assignmentByModule');
    Route::group(['prefix' => 'procedure'], function () {
        Route::post('/', 'ProcedureController@store');
        Route::put('/{id}', 'ProcedureController@update');
        Route::delete('/{id}', 'ProcedureController@destroy');
        Route::post('/teacher_title', 'TitleController@asignerTitle');
        Route::get('/getProdceduresByGroup/{id}', 'ProcedureController@getProdceduresByGroup');
    });
    Route::group(['prefix' => 'title'], function () {
        Route::post('/', 'AnswerController@store');
        Route::post('/teacher_title', 'TitleController@asignerTitle');
    });
    Route::group(['prefix' => 'answer'], function () {
        Route::post('/saveAnswerEncuesta', 'AnswerController@saveAnswerEncuesta');
    });
    Route::group(['prefix' => 'patient'], function () {
        Route::get('/document/{document}', 'PatientController@patientByDocument');
    });
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/authenticate', 'AuthController@authenticate');
        Route::post('/create', 'AuthController@register');
        Route::post('/email', 'AuthController@sendEmail');
        Route::put('/update/{id}', 'AuthController@update');
        Route::get('/last', 'AuthController@last');
        Route::get('/find/{user_name}', 'AuthController@find');
        Route::post('/verifyUser', 'AuthController@verifyUser');
    });
    Route::group(['prefix' => 'securityQuestion'], function () {
        Route::get('/getAll', 'AuthController@getSecurityQuestions');
        Route::put('/update/{id}', 'AuthController@updateSecurityQuestion');
        Route::post('/add', 'AuthController@addSecurityQuestion');
        Route::delete('/delete/{id}', 'AuthController@deleteSecurityQuestion');
    });
    Route::group(['prefix' => 'assistence'], function () {
        Route::post('/', 'AssistenceController@store');
        Route::post('/list', 'AssistenceController@manyAssistences');
        Route::post('/assistencesBySubject', 'AssistenceController@assistencesBySubject');
        Route::post('/assistencesByCareer', 'AssistenceController@assistencesByCareer');
        Route::post('/assistencesByScenario', 'AssistenceController@assistencesByScenario');
    });
    Route::group(['prefix' => 'student'], function () {
        Route::get('/assignment/{id}/{academicPeriod}', 'StudentController@getAssignment');
        Route::get('/attentions/{id}', 'StudentController@getAttentions');
        Route::get('/scenario/{scenarioId}/{subjectId?}', 'StudentController@studentsByScenario');
        Route::post('/patientPoll', 'StudentController@studentsByPatientPoll');
        Route::post('/studentsSave', 'StudentController@studentsSave');
    });
    Route::group(['prefix' => 'rotation'], function () {
        Route::get('/rotationsByGroup/{groupId}', 'RotationController@rotationsByGroup');
        Route::get('/rotationsWithGroups', 'RotationController@rotationsWithGroups');
        Route::get('/getRotationsByRol/{person}', 'RotationController@getRotationsByRol');
        Route::delete('/deletingRotations', 'RotationController@deletingRotations');
        Route::put('/', 'RotationController@update');
    });
    Route::group(['prefix' => 'question'], function () {
        Route::get('/questionByRol', 'QuestionController@questionByRol');
        Route::post('/questionByCheckType', 'QuestionController@getQuestionsByCheckType');
        Route::post('/questionsByCheckTypeAll', 'QuestionController@getQuestionsByCheckTypeAll');
        Route::post('/search', 'QuestionController@search');
    });
    Route::group(['prefix' => 'area'], function () {
        Route::post('/areaInScenario', 'AreaController@areaInScenario');
        Route::post('/search', 'AreaController@search');
        Route::post('/areaInModules', 'AreaController@areaInModules');
        Route::get('/areasWithoutScenario/{scenarioId}', 'AreaController@areasWithoutScenario');
        Route::get('/modules/{areaId}', 'AreaController@modules');
    });
    Route::group(['prefix' => 'check'], function () {
        Route::get('/', 'CheckController@index');
        Route::post('/', 'CheckController@store');
        Route::get('/checks/{id?}', 'CheckController@checksById');
        Route::get('/paginate', 'CheckController@paginate');
        Route::post('/checksByRotation', 'CheckController@checksByRotation');
        Route::post('/checksByStudent', 'CheckController@checksByStudent');
        Route::post('/checksByTeacher', 'CheckController@checksByTeacher');
        Route::post('/search', 'CheckController@search');
        Route::post('/saveEncuesta', 'CheckController@saveEncuesta');
        Route::post('/activeCheck', 'CheckController@activeCheck');
    });
    Route::group(['prefix' => 'teacher'], function () {
        Route::get('/titles/{id}', 'TeacherController@getTitles');
        Route::put('/{id}', 'TeacherController@update');
        Route::get('/document/{document}', 'TeacherController@teacherByDocument');
        Route::get('/people', 'TeacherController@getPeople');
        Route::get('/people-paginate', 'TeacherController@getPeoplePaginate');
        Route::get('/scenario/{scenarioId}', 'TeacherController@teachersByScenario');
    });
    Route::group(['prefix' => 'person'], function () {
        Route::post('/search', 'PersonController@getPersonSearch');
        Route::get('/getPerson', 'PersonController@getPerson');
    });
    Route::group(['prefix' => 'scenario'], function () {
        Route::get('/paginate', 'PracticeScenarioController@getAllPaginate');
        Route::get('/areas/{scenarioId}', 'PracticeScenarioController@getAreas');
        Route::get('/areasByModule/{scenarioId}/{moduleId}', 'PracticeScenarioController@getAreasByModule');
        Route::get('/modules/{moduleId}', 'PracticeScenarioController@scenariosByModule');
        Route::get('/by-type/{type}', 'PracticeScenarioController@scenariosByTypeScenario');
        Route::get('/search/{scenario}', 'PracticeScenarioController@search');
        Route::post('scenarioInCareer', 'PracticeScenarioController@scenarioInCareer');
        Route::post('assignQuotas', 'PracticeScenarioController@assignQuotas');
    });
    Route::group(['prefix' => 'faculty'], function () {
        Route::post('/', 'CareerController@saveFaculty');
        Route::get('/', 'CareerController@getFaculty');
    });
    Route::group(['prefix' => 'career'], function () {
        Route::get('/faculty/{id}', 'CareerController@getCareerByFaculty');
        Route::get('/questions', 'CareerController@getQuestions');
        Route::get('/questions-paginate', 'CareerController@getQuestionsPaginate');
        Route::get('/students', 'CareerController@getStudents');
        Route::get('/assignments/{subject_id}', 'CareerController@getAssignments');
        Route::get('/questionsCheck', 'CareerController@getQuestionsCheck');
        Route::get('/modules', 'CareerController@getModules');
        Route::get('/modules-paginate', 'CareerController@getModulesPaginate');
        Route::get('/subjects', 'CareerController@getSubjects');
        Route::get('/subjects-paginate', 'CareerController@getSubjectsPaginate');
    });
    Route::group(['prefix' => 'module'], function () {
        Route::get('/procedures/{id}', 'ModuleController@showProcedures');
        Route::get('/procedures-paginate/{id}', 'ModuleController@showProceduresPaginate');
        Route::get('/students/{id}', 'ModuleController@getStudents');
    });
    Route::group(['prefix' => 'groupStudent'], function () {
        Route::post('/saveStudentGroup', 'StudentGroupController@addStudentGroup');
        Route::get('/studentsGroups/{groupId}', 'StudentGroupController@studentGroup');
        Route::get('/studentsGroupsByModule/{moduleId}', 'StudentGroupController@studentGroupByModule');
        Route::get('/studentsGroupsByTeacher', 'StudentGroupController@studentGroupByTeacher');
        Route::post('/updateGroup', 'StudentGroupController@updateGroup');
    });
    Route::group(['prefix' => 'subject'], function () {
        Route::get('/modules/{code}', 'SubjectController@modules');
        Route::get('/modules/modulesArea/{id}/{areaId}/{practiceScenarioId}', 'SubjectController@modulesArea');
    });
    Route::group(['prefix' => 'academicPeriod'], function () {
        Route::get('/getAcademicPeriod', 'AcademicPeriodController@getObjectAcademicPeriod');
        Route::get('/getAcademicPeriodByDate', 'AcademicPeriodController@getAcademicPeriodByDate');
        Route::post('/activeAcademicPeriod', 'AcademicPeriodController@activeAcademicPeriod');
        Route::put('/updateAcademicPeriodCareer/{period}', 'AcademicPeriodController@updateAcademicPeriodCareer');
        Route::post('/saveAcademicPeriodCareer', 'AcademicPeriodController@saveAcademicPeriodCareer');
    });
    Route::group(['prefix' => 'attention'], function () {
        Route::post('/by-module', 'AttentionController@attentionByModule');
        Route::post('/by-scenario', 'AttentionController@attentionByScenario');
        Route::post('/by-activities', 'AttentionController@attentionByActivities');
        Route::get('/getAttentions/{id?}', 'AttentionController@getAttentions');
        Route::get('/groupByDate/{date}', 'AttentionController@attentionByDate');
        Route::get('/groupStudent', 'AttentionController@groupByDateStudent');
        Route::get('/groupDate', 'AttentionController@groupByDate');
        Route::get('/attentionsByStudent/{studentId}/{date?}', 'AttentionController@getattentionsByStudent');
        Route::get('/getAllAttentions', 'AttentionController@getAllAttentions');
        Route::post('/search', 'AttentionController@getAttentionSearch');
        Route::get('/encuesta/{id}', 'AttentionController@encuesta');
    });
    Route::group(['prefix' => 'report'], function () {
        Route::get('by-category/{reportCategory}/{academicPeriod}', 'ReportController@reportByCategory');
        Route::post('/', 'ReportController@store');
    });
    Route::group(['prefix' => 'page'], function () {
        Route::get('getAll', 'PageController@getAllPages');
    });
    Route::apiResources([
        'auth'               => 'AuthController',
        'academicPeriod'     => 'AcademicPeriodController',
        'person'             => 'PersonController',
        'career'             => 'CareerController',
        'subject'            => 'SubjectController',
        'module'             => 'ModuleController',
        'student'            => 'StudentController',
        'question'           => 'QuestionController',
        'patient'            => 'PatientController',
        'attention'          => 'AttentionController',
        'groupStudent'       => 'StudentGroupController',
        'scenario'           => 'PracticeScenarioController',
        'area'               => 'AreaController',
        'assignment'         => 'AssignmentController',
        'rotation'           => 'RotationController',
        'event'              => 'EventController',
        'answer'             => 'AnswerController',
        'assignStudentGroup' => 'AssignStudentGroupController',
        'page'               => 'PageController',
        'title'              => 'TitleController'
    ]);
});
Route::group(['prefix' => 'pdf'], function () {
    Route::post('/reportBySubject', 'PDFController@reportBySubject');
    Route::post('/reportByScenario', 'PDFController@reportByScenario');
    Route::post('/reportByModule', 'PDFController@reportByModule');
    Route::post('/reportAssistenceStudent', 'PDFController@reportAssistenceStudent');
    Route::post('/reportByPoll', 'PDFController@reportByPoll');
    Route::post('/reportByCheckStudent', 'PDFController@reportByCheckStudent');
    Route::post('/reportByCheckTeacher', 'PDFController@reportByCheckTeacher');
    Route::get('/boxRotations', 'PDFController@boxRotation');
});

Route::post('assignmentrotation', 'RotationController@AsignarRotacion');
Route::get('rotation/rotationsCode/{code}', 'RotationController@getRotationsByCode');


<?php

use App\SecurityQuestion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SecurityQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SecurityQuestion::create(['question' => '¿Cómo se llama su tío favorito?']);
        SecurityQuestion::create(['question' => '¿Cómo se llamó su primera mascota?']);
        SecurityQuestion::create(['question' => '¿Cuál es su profesor favorito?']);
        SecurityQuestion::create(['question' => '¿Cuál es su Hobby?']);
    }
}

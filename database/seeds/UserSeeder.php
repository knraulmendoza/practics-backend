<?php

use App\Enums\StateUser;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'user' => 'admin@unicesar.edu.co',
            'password' => Hash::make('admin'),
            'state' => StateUser::ACTIVOSINPERSON,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SecurityQuestionSeeder::class,
            RolesSeeder::class,
            UserSeeder::class,
            RolesUsersSeeder::class,
            PagesSeeder::class
        ]);
    }
}

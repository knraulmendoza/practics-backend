<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'description' => 'administrador',
            'visual_name' => 'administrador',
        ]);

        Role::create([
            'name' => 'coordinator',
            'description' => 'Coordinador de práctica',
            'visual_name' => 'coordinador',
        ]);

        Role::create([
            'name' => 'teacher practic',
            'description' => 'Docente de práctica',
            'visual_name' => 'docente de práctica',
        ]);

        Role::create([
            'name' => 'teacher',
            'description' => 'Docente',
            'visual_name' => 'docente',
        ]);

        Role::create([
            'name' => 'student',
            'description' => 'Estudiante',
            'visual_name' => 'estudiante',
        ]);
    }
}

<?php

use App\RolesUsers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RolesUsers::create(
            [
                'user_id' => 1,
                'role_id' => 1,
            ]
        );
    }
}

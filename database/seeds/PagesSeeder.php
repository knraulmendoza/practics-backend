<?php

use App\Page;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'title' => 'Configuración',
            'path' => '/configuracion',
            'icon' => 'ajuste',
        ]);
        Page::create([
            'title' => 'Práctica',
            'path' => '/modulo',
            'icon' => 'practicas',
        ]);
        $page = Page::firstWhere('path', '/modulo');
        $page->update(['parent_page' => $page->id]);
        Page::create([
            'title' => 'Asignaciones',
            'path' => '/asignacion',
            'icon' => 'asignacion_docente',
            'parent_page' => $page->id,
        ]);
        Page::create([
            'title' => 'Docente',
            'path' => '/teacher',
            'icon' => 'docente',
            'parent_page' => $page->id,
        ]);
        Page::create([
            'title' => 'Escenarios',
            'path' => '/escenarios',
            'icon' => 'escenarios',
            'parent_page' => $page->id,
        ]);
        $page = Page::firstWhere('path', '/escenarios');
        $page->update(['parent_page' => $page->id]);
        Page::create([
            'title' => 'Rotaciones',
            'path' => '/rotation',
            'icon' => 'rotaciones',
            'parent_page' => $page->id,
        ]);
        Page::create([
            'title' => 'Grupos de estudiantes',
            'path' => '/grupo-estudiante',
            'icon' => 'asignaciones'
        ]);
        Page::create([
            'title' => 'Informes',
            'path' => '/reports',
            'icon' => 'informes'
        ]);
        Page::create([
            'title' => 'Eventos',
            'path' => '/events',
            'icon' => 'eventos'
        ]);
        Page::create([
            'title' => 'Coordinadores',
            'path' => '/coordinador',
            'icon' => 'coordinadores'
        ]);
        Page::create([
            'title' => 'Carreras',
            'path' => '/career',
            'icon' => 'carreras'
        ]);
        Page::create([
            'title' => 'Estudiantes',
            'path' => '/estudiante',
            'icon' => 'estudiantes'
        ]);
        Page::create([
            'title' => 'Pacientes',
            'path' => '/atencion',
            'icon' => 'atenciones'
        ]);
        Page::create([
            'title' => 'Asignaturas',
            'path' => '/subject',
            'icon' => 'asignaturas'
        ]);
        Page::create([
            'title' => 'Preguntas',
            'path' => '/preguntas',
            'icon' => 'preguntas'
        ]);
        Page::create([
            'title' => 'Chequeos',
            'path' => '/chequeo',
            'icon' => 'chequeos'
        ]);
        Page::create([
            'title' => 'Páginas',
            'path' => '/page',
            'icon' => 'rotaciones'
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedures', function (Blueprint $table) {
            $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 45)->comment('Nombre del procedimiento');
            $table->unsignedSmallInteger('module_id')->comment('Corresponde a la llave foránea de la tabla modules');
            $table->text('description')->nullable()->comment('Descripción detallada sobre el procedimiento');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('module_id')->references('id')->on('modules')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedures');
    }
}

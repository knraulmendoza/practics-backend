<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titles', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 255)->comment('Nombre del título');
            $table->text('description')->nullable()->comment('Descripción del título');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('teachers_titles', function (Blueprint $table) {
            $table->unsignedInteger('teacher_id')->comment('Corresponde a la llave foránea de la tabla teachers');
            $table->unsignedTinyInteger('title_id')->comment('Corresponde a la llave foránea de la tabla titles');
            $table->primary(['teacher_id', 'title_id']);
            $table->foreign('teacher_id')->references('id')->on('teachers')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('title_id')->references('id')->on('titles')->onUpdate('restrict')->onDelete('restrict');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers_titles');
        Schema::dropIfExists('titles');
    }
}

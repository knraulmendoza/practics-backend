<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePracticeScenariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_scenarios', function (Blueprint $table) {
            $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 255)->comment('Nombre del escenario de práctica');
            $table->text('description')->nullable()->comment('Descripción detallada del escenario de práctica');
            $table->enum('complexity_level', [1, 2, 3, 4, 5])->nullable()->comment('Tipo de complejidad del escenario');
            $table->enum('entity', [1, 2])->comment('Tipo de entidad 1:  publica; 2: privada');
            $table->enum('scene_type', [1, 2, 3, 4, 5])->comment('Tipo de escenario de práctica: 1: clínica; 2: hospital; 3: Institución educativa; 4: barrios; 5: Otras entidades');
            $table->string('abbreviation', 30)->nullable()->comment('Abreviación del nombre del escenario de práctica');
            $table->tinyInteger('state')->default(1)->comment('Estado del escenario de práctica');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        DB::statement('ALTER TABLE `practice_scenarios` ADD FULLTEXT INDEX fulltext_scenario_name (`name`)');
        Schema::create('career_practice_scenario', function (Blueprint $table) {
            $table->mediumIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->unsignedSmallInteger('practice_scenario_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('career_id')->references('id')->on('careers')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('practice_scenario_id')->references('id')->on('practice_scenarios')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_practice_scenario');
        Schema::dropIfExists('practice_scenarios');
    }
}

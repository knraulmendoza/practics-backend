<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerPracticeScenarioAcademicPeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_practice_scenario_academic_period', function (Blueprint $table) {
            $table->unsignedMediumInteger('career_practice_scenario_id')->comment('Corresponde a la llave foránea de la tabla career_practice_scenario');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->tinyInteger('quotas')->comment('Corresponde a la llave principal de la tabla');
            $table->foreign('career_practice_scenario_id', 'fk_cps_career')->references('id')->on('career_practice_scenario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('academic_period_id', 'fk_cps_academic_period')->references('id')->on('careers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_practice_scenario_academic_period');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function(Blueprint $table) {
		    $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->enum('document_type', [1, 2, 3, 4])->comment('Tipo de documento que seleccione 1: cedula, 2: tarjeta de identidad, 3: extranjero, 4: pasaporte');
		    $table->string('document', 11)->comment('Número de documento de la persona');
		    $table->string('first_name', 25)->comment('Primer nombre de la persona');
		    $table->string('second_name', 25)->nullable()->comment('Segundo nombre de la persona');
		    $table->string('first_last_name', 30)->comment('Primer apellido de la persona');
		    $table->string('second_last_name', 30)->comment('Segundo apellido de la persona');
		    $table->string('phone', 10)->nullable()->comment('Número de teléfono de la persona');
		    $table->date('birthday')->nullable()->comment('Fecha de nacimiento de la persona');
		    $table->text('address')->nullable()->comment('Dirección de la persona');
		    $table->enum('gender', [1, 2, 3])->comment('Genero de la persona 1: masculino, 2: femenino, 3: otro');
		    $table->foreignId('user_id')->constrained('users')
            ->onDelete('restrict')
            ->onUpdate('restrict')
            ->comment('Corresponde a la llave foránea de la tabla users');
		    $table->timestamp('created_at');
            $table->timestamp('updated_at');
		});

        DB::statement('ALTER TABLE `people` ADD FULLTEXT INDEX FULL_name (first_name, second_name, first_last_name, second_last_name)');

		Schema::create('people_careers', function(Blueprint $table) {
		    $table->foreignId('person_id')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
		    $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');;
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
		    $table->primary(['person_id', 'career_id']);
		    $table->foreign('career_id')
		        ->references('id')->on('careers')
		        ->onDelete('restrict')
		        ->onUpdate('restrict');
		});
        Schema::create('students', function(Blueprint $table) {
		    $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->foreignId('person_id')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
		    $table->timestamp('created_at');
		    $table->timestamp('updated_at');
		});
        Schema::create('teachers', function(Blueprint $table) {
		    $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->foreignId('person_id')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
            $table->enum('state', [0, 1])->default(1)->comment('Estado del docente: 1 Activo, 0: Inactivo');
		    $table->timestamp('created_at');
		    $table->timestamp('updated_at');
		});
        Schema::create('coordinators', function(Blueprint $table) {
		    $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->foreignId('person_id')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
            $table->enum('state', [0, 1])->default(1)->comment('Estado del coordinadoor: 1 Activo, 0: Inactivo');
		    $table->timestamp('created_at');
		    $table->timestamp('updated_at');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinators');
        Schema::dropIfExists('teachers');
        Schema::dropIfExists('students');
        Schema::dropIfExists('people_careers');
        Schema::dropIfExists('people');
    }
}

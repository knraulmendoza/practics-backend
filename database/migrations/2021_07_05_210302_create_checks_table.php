<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checks', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('date')->comment('Fecha en la que se realiza el chequeo');
            $table->foreignId('evaluated')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
            $table->foreignId('evaluator')->constrained('people')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla people');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->enum('state', [1, 2, 3, 4])->default(1)->comment('Estado del chequeo, 1: Creada, 2: Realizada, 3 Cancelada, 4 chequeado');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('career_id')->references('id')->on('careers')->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('checks_rotations', function (Blueprint $table) {
            $table->foreignId('check_id')->constrained('checks')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla checks');
            $table->foreignId('rotation_id')->constrained('rotations')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla rotations');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->primary(['check_id', 'rotation_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checks_rotations');
        Schema::dropIfExists('checks');
    }
}
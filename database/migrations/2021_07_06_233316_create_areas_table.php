<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 255)->unique()->comment('El nombre del área será único y especificará una zona de escenario de práctica');
            $table->text('description')->nullable()->comment('Descripción detallada del área del escenario de práctica');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::create('practice_scenarios_areas', function (Blueprint $table) {
            $table->mediumIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->unsignedSmallInteger('practice_scenarios_id')->comment('Corresponde a la llave foránea de la tabla practice_scenarios');
            $table->unsignedSmallInteger('area_id')->comment('Corresponde a la llave foránea de la tabla areas');
            $table->tinyInteger('state');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->unique(['practice_scenarios_id', 'area_id']);
            $table->foreign('area_id')->references('id')->on('areas')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('practice_scenarios_id')->references('id')->on('practice_scenarios')->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('practice_scenarios_areas_modules', function (Blueprint $table) {
            $table->mediumIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->unsignedMediumInteger('practice_scenarios_areas_id')->comment('Corresponde a la llave foránea de la tabla practice_scenarios_areas');
            $table->unsignedSmallInteger('module_id')->comment('Corresponde a la llave foránea de la tabla modules');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->foreign('practice_scenarios_areas_id', 'psa_fk')->references('id')->on('practice_scenarios_areas')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('module_id')->references('id')->on('modules')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practice_scenarios_areas_modules');
        Schema::dropIfExists('practice_scenarios_areas');
        Schema::dropIfExists('areas');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rotations', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('start_date')->comment('Fecha en la que inicia la rotación');
            $table->date('end_date')->comment('Fecha finalización de la rotación');
            $table->enum('type', [1, 2, 3])->default(1)->comment('Tipo de rotación 1: Completa, 2: Doble, 3: Triple');
            $table->string('code', 8)->comment('Código de la rotación');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('career_id')->references('id')->on('careers')->onUpdate('restrict')->onDelete('restrict');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rotations');
    }
}

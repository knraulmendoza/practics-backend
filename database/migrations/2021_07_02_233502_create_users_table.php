<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('security_questions', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->text('question')->comment('Descripción de la pregunta');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('user', 60)->unique()->comment('Nombre del usuario, correo institucional');
            $table->text('password')->comment('Contraseña del usuario, encriptada');
            $table->enum('state', [1, 2, 3, 4])->default(3)->comment('Estado del usuario 1: activo, 2: inactivo, 3: sin persona, 4: activo sin persona');
            $table->enum('state_password', [0, 1])->default(0)->comment('Estado de la contraseña 0: cambio contraseña, 1: actualizada contraseña');
            $table->text('security_answer')->nullable()->comment('Respuesta a la pregunta de seguridad');
            $table->unsignedBigInteger('user_parent')->nullable()->comment('Respuesta a la pregunta de seguridad');
            $table->unsignedTinyInteger('security_question_id')->nullable()->comment('Corresponde a la llave foránea de la tabla security_questions');
            $table->foreign('security_question_id')
                ->references('id')->on('security_questions')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
            $table->foreign('user_parent')
                ->references('id')->on('users')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('security_questions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
		    $table->enum('report_category', ['atenciones_por_asignatura','atenciones_por_modulo','asistencias_por_asignatura', 'chequeos_por_rol', 'encuestas_a_pacientes', 'patients_poll'])
            ->comment('La categoría a la que pertenece el reporte');
		    $table->text('description')->comment('Es una descripción detallada del reporte');
		    $table->text('title')->comment('Título del reporte');
		    $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
		    $table->text('image')->nullable()->comment('Imagen asociada al reporte ejemplo gráficas');
            $table->foreign('career_id')
		        ->references('id')->on('careers')
		        ->onDelete('restrict')
		        ->onUpdate('restrict');
		    $table->foreign('academic_period_id')
		        ->references('id')->on('academic_periods')
		        ->onDelete('restrict')
		        ->onUpdate('restrict');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::create('report_datas', function(Blueprint $table) {
		    $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->string('name', 100)->comment('Este es el nombre del dato de la gráfica del reporte Ejemplo (asistencias)');
		    $table->smallInteger('total')->comment('Resultado del dato de la gráfica del reporte Ejemplo (Asistencia 100)');
		    $table->foreignId('report_id')->constrained('reports')
            ->onDelete('restrict')
            ->onUpdate('restrict')
            ->comment('Corresponde a la llave foránea de la tabla reports');
		    $table->timestamp('created_at');
            $table->timestamp('updated_at');
		});

		Schema::create('report_metas', function(Blueprint $table) {
		    $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->string('key', 50)->comment('Este campo se almacena las keys dinámicos que tiene el reporte');
		    $table->string('value', 100)->comment('Este campo es el valor del dato dinámico que tiene el reporte');
		    $table->string('tabla', 100)->nullable()->comment('Este campo indica el nombre de la tabla que hace referencia el reporte');
            $table->foreignId('report_id')->constrained('reports')
            ->onDelete('restrict')
            ->onUpdate('restrict')
            ->comment('Corresponde a la llave foránea de la tabla reports');
		    $table->timestamp('created_at');
            $table->timestamp('updated_at');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_datas');
        Schema::dropIfExists('report_metas');
        Schema::dropIfExists('reports');
    }
}

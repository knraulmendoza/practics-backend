<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssistenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistence', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('date')->comment('Fecha en que fue tomada la asistencia');
            $table->text('observation')->nullable()->comment('Observación que puede registrar el docente de la práctica ejemplo, excusando al estudiante');
            $table->enum('assist', [1, 2, 3])->default(1)->comment('Tipo de asistencia por defecto 1. 1: asistió, 2: no asistió, 3: presento excusa');
            $table->foreignId('student_id')->constrained('students')
            ->onUpdate('restrict')
            ->onDelete('restrict')
            ->comment('Corresponde a la llave foránea de la tabla students');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('restrict')->onDelete('restrict');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistence');
    }
}

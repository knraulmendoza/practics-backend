<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicPeriodsCareers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_periods_careers', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->date('start_date')->comment('Fecha de inicio de cada periodo académico por carrera');
            $table->date('end_date')->comment('Fecha de finalización de cada periodo académico por carrera');
            $table->enum('state', [0, 1])->default(1)->comment('Estado del periodo académico 0: INACTIVO, 1: ACTIVO');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('career_id')->references('id')->on('careers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_periods_careers');
    }
}

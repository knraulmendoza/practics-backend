<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 255)->unique()->comment('Nombre de la asignatura este es único');
            $table->string('code', 7)->unique()->comment('Identificador de la asignatura');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::create('careers_subjects', function (Blueprint $table) {
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->unsignedSmallInteger('subject_id')->comment('Corresponde a la llave foránea de la tabla subjects');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->primary(['career_id', 'subject_id']);
            $table->foreign('career_id')->references('id')->on('careers')
            ->onUpdate('restrict')
            ->onDelete('restrict');
            $table->foreign('subject_id')->references('id')->on('subjects')
            ->onUpdate('restrict')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers_subjects');
        Schema::dropIfExists('subjects');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->text('observation')->comment('Descripción de la observación')->nullable();
            $table->bigInteger('observationable_id')->comment('Este campo contiene la relación polimórfca que identifica el tipo de reporte a la que pertenece esta');
            $table->text('observationable_type')->comment('Nombre del modelo a la que pertenece la observación');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 25)->unique()->comment('Nombre del rol');
            $table->text('description')->nullable()->comment('Descripción del rol');
            $table->string('visual_name', 25)->comment('Nombre de visualización en pantalla del rol');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('roles_users', function (Blueprint $table) {
            $table->unsignedTinyInteger('role_id')->comment('Corresponde a la llave foránea de la tabla roles');
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreignId('user_id')->constrained('users')
            ->comment('Corresponde a la llave foránea de la tabla users')
            ->onUpdate('RESTRICT')
            ->onDelete('RESTRICT');
            $table->enum('state', [0,1])->default(1)->comment('Estado de la asignación del rol al usuario, 0: inhabilitado, 1: habilitado');
            $table->primary(['role_id', 'user_id']);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_users');
        Schema::dropIfExists('roles');
    }
}

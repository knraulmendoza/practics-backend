<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attentions', function (Blueprint $table) {
            $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('date')->comment('Fecha en que fue tomada la asistencia');
            $table->enum('state', [0, 1, 2])->comment('Estado de la atención, 0: se encuentra sin confirmar, 1: confirmado, 2: evaluado');
            $table->unsignedBigInteger('student_id')->comment('Corresponde a la llave foránea de la tabla students');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->unsignedBigInteger('patient_id')->comment('Corresponde a la llave foránea de la tabla patients');
            $table->unsignedInteger('assignment_id')->comment('Corresponde a la llave foránea de la tabla assignments');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('student_id')->references('id')->on('students')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('patient_id')->references('id')->on('patients')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('assignment_id')->references('id')->on('assignments')->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('procedures_attentions', function (Blueprint $table) {
            $table->unsignedInteger('procedure_id')->comment('Corresponde a la llave foránea de la tabla procedures');
            $table->unsignedInteger('attention_id')->comment('Corresponde a la llave foránea de la tabla attentions');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->primary(['procedure_id', 'attention_id']);

            $table->foreign('procedure_id')->references('id')->on('procedures')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('attention_id')->references('id')->on('attentions')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedures_attentions');
        Schema::dropIfExists('attentions');
    }
}

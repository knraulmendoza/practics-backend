<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_groups', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->foreignId('student_id')
            ->comment('Corresponde a la llave foránea de la tabla students')
            ->constrained('students')->onUpdate('restrict')->onDelete('restrict');
            $table->unsignedMediumInteger('group_module_id')->comment('Corresponde a la llave foránea de la tabla group_modules');
            $table->foreign('group_module_id')->references('id')->on('groups_modules')->onUpdate('restrict')->onDelete('restrict');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_groups');
    }
}

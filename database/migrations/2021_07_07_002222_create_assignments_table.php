<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('date')->comment('Fecha en que se realiza la asignación');
            $table->unsignedMediumInteger('module_area_id')->comment('Corresponde a la llave foránea de la tabla practice_scenarios_areas_modules');
            $table->unsignedInteger('teacher_id')->comment('Corresponde a la llave foránea de la tabla teachers');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->unique(['module_area_id', 'teacher_id', 'academic_period_id']);
            $table->foreign('module_area_id', 'fk_assignments_module_area')->references('id')->on('practice_scenarios_areas_modules')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('career_id')->references('id')->on('careers')->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('assign_student_groups', function (Blueprint $table) {
            $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
            $table->enum('working_day', ['mañana', 'tarde', 'noche'])->comment('Este campo es para registrar la jornada en la que se encontrara el grupo: “Mañana”,” Tarde”, noche');
            $table->unsignedMediumInteger('group_module_id')->comment('Corresponde a la llave foránea de la tabla groups_modules');
            $table->unsignedBigInteger('rotation_id')->comment('Corresponde a la llave foránea de la tabla rotations');
            $table->unsignedInteger('assignment_id')->comment('Corresponde a la llave foránea de la tabla assignments');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->unique(['group_module_id', 'rotation_id', 'assignment_id'], 'UN_group_assignm_rotation');
            $table->foreign('group_module_id')->references('id')->on('groups_modules')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('rotation_id')->references('id')->on('rotations')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('assignment_id')->references('id')->on('assignments')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_student_groups');
        Schema::dropIfExists('assignments');
    }
}

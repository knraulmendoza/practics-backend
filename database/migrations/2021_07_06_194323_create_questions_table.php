<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->enum('type', [1 ,2])->comment('Tipo de pregunta (1: Chequeo, 2: encuesta)');
            $table->text('question')->comment('Este es el campo pregunta ya sea para chequeo o encuesta');
            $table->set('check_type', ['students', 'teachers'])->nullable()->comment('Tipo de chequeo: estudiantes, docentes, estudiantes y docentes, null significa encuesta');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('answers', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->tinyInteger('answer')->comment('El valor de la respuesta cuantitativa');
            $table->unsignedSmallInteger('question_id')->comment('Corresponde a la llave foránea de la tabla questions');
            $table->bigInteger('answerable_id')->comment('Este campo contiene la relación polimórfica que identifica de donde viene la respuesta dada');
            $table->text('answerable_type')->comment('En este campo se almacena el nombre del modelo al cuál está relacionado con la respuesta');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        DB::statement('ALTER TABLE `answers` MODIFY COLUMN `answer` tinyint(1) NOT NULL;');
        Schema::create('careers_questions', function (Blueprint $table) {
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->unsignedSmallInteger('question_id')->comment('Corresponde a la llave foránea de la tabla questions');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->primary(['career_id', 'question_id']);
            $table->foreign('career_id')->references('id')->on('careers')
            ->onUpdate('restrict')
            ->onDelete('restrict');
            $table->foreign('question_id')->references('id')->on('questions')
            ->onUpdate('restrict')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
        Schema::dropIfExists('careers_questions');
        Schema::dropIfExists('questions');
    }
}

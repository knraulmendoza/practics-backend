<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('code', 50)->unique()->comment('En este campo se almacena el código de la Facultad');
            $table->string('name', 255)->unique()->comment('En este campo se almacena el nombre de la facultad');
            $table->enum('state', [1,2])->default(1)->comment('Estado de la facultad, 1: Activo, 2: Inactivo');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('careers', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('code', 7)->unique()->comment('Nombre del programa, el cual debe ser único');
            $table->string('name', 40)->unique()->comment('Código o acrónimo que identifica a cada programa');
            $table->unsignedTinyInteger('faculty_id')->comment('Corresponde a la llave foránea de la tabla faculties');
            $table->foreign('faculty_id')->references('id')->on('faculties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
        Schema::dropIfExists('faculties');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('title', 100)->comment('Título que aparece en el menú de opciones');
            $table->string('path', 100)->comment('URL de acceso');
            $table->string('icon', 60)->comment('Nombre del icono que se mostrara en el menú');
            $table->enum('state', [0, 1])->default(1)->comment('Estado de la página 0: No disponible, 1: disponible');
            $table->unsignedTinyInteger('parent_page')->nullable()->comment('Corresponde a la llave foránea de la tabla pages');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('parent_page')->references('id')->on('pages')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}

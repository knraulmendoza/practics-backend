<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsPractictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_practicts', function (Blueprint $table) {
            $table->mediumIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->text('title')->comment('Título del evento');
            $table->text('description')->comment('Descripción del evento');
            $table->date('date')->comment('Fecha en la que se realizara el evento');
            $table->time('hour')->comment('Hora en la que el evento empezara');
            $table->string('address', 100)->comment('Lugar en el cual se llevará acabo el evento');
            $table->enum('state', [1, 2, 3])->default(1)->comment('Estado del evento, 1: pendiente, 2: realizado, 3: pospuesto o cancelado');
            $table->text('image')->nullable()->comment('Imagen referente al evento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_practicts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->smallIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('name', 255)->comment('En este campo se almacena el nombre de él módulo');
            $table->text('description')->nullable()->comment('En este campo se almacena una descripción detallada del módulo');
            $table->string('code', 7)->unique()->comment('En este campo se almacena el código de cada módulo');
            $table->unsignedTinyInteger('career_id')->comment('Corresponde a la llave foránea de la tabla careers');
            $table->unsignedSmallInteger('subject_id')->comment('Corresponde a la llave foránea de la tabla subjects');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign(['career_id', 'subject_id'])->references(['career_id', 'subject_id'])->on('careers_subjects')
            ->onUpdate('restrict')
            ->onDelete('restrict');
        });
        Schema::create('groups_modules', function (Blueprint $table) {
            $table->mediumIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->string('code', 12)->unique()->comment('Se almacena el código de cada grupo de práctica, este será único');
            $table->text('name')->nullable()->comment('Se almacena el nombre del grupo este es aleatorio ejemplo grupo 1');
            $table->unsignedSmallInteger('module_id')->comment('Corresponde a la llave foránea de la tabla modules');
            $table->unsignedTinyInteger('academic_period_id')->comment('Corresponde a la llave foránea de la tabla academic_periods');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            
            $table->foreign('module_id')->references('id')->on('modules')
            ->onUpdate('restrict')
            ->onDelete('restrict');
            $table->foreign('academic_period_id')->references('id')->on('academic_periods')
            ->onUpdate('restrict')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_modules');
        Schema::dropIfExists('modules');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAcademicPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_periods', function (Blueprint $table) {
            $table->tinyIncrements('id')->comment('Corresponde a la llave principal de la tabla');
		    $table->smallInteger('year')->comment('Este campo contendrá el registro de los años, cada registro será único');
		    $table->enum('period', ['1', '2'])->comment('El periodo académico de cada año 1 o 2');
		    $table->date('start_date')->comment('Fecha de inicio de cada periodo académico');
		    $table->date('end_date')->comment('Fecha de finalización de cada periodo académico');
            $table->unique(['year', 'period']);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_periods');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Corresponde a la llave principal de la tabla');
            $table->enum('document_type', [1, 2, 3, 4])->comment('Tipo de documento que seleccione 1: cedula, 2: tarjeta de identidad, 3: extranjero, 4: pasaporte');
		    $table->string('document', 11)->unique()->comment('Número de documento del paciente');
		    $table->string('name', 255 )->comment('Nombre del paciente');
		    $table->unsignedTinyInteger('age')->nullable();
		    $table->enum('gender', [1, 2, 3])->nullable()->comment('Genero del paciente');
		    $table->string('phone', 11)->nullable()->comment('Número de teléfono del paciente');
		    $table->enum('status', [1, 2, 3, 4])->nullable()->comment('Estado económico del paciente');
		    $table->enum('academic_level', [1, 2, 3, 4])->nullable()->comment('Nivel académico alcanzado por el paciente. 1: primaria, 2: secundaria, 3: técnico, 4: profesional');
		    $table->enum('employment_situation', [1, 2, 3])->nullable()->comment('Situación laboral del paciente. 1: desempleado, 2: empleado, 3: estudiante');
		    $table->string('origin', 45)->nullable()->comment('Ciudad origen del paciente');
		    $table->timestamp('created_at');
		    $table->timestamp('updated_at');
        });

        DB::statement('ALTER TABLE `patients` ADD FULLTEXT INDEX ft_name (name)');
        DB::statement('ALTER TABLE `patients` MODIFY COLUMN `age` tinyint(1) UNSIGNED COMMENT "Edad del paciente";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}

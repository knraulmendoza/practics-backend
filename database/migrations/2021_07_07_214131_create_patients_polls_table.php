<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsPollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients_polls', function (Blueprint $table) {
            $table->increments('id')->comment('Corresponde a la llave principal de la tabla');
            $table->date('date')->comment('Fecha en la que se asigna la encuesta');
            $table->unsignedInteger('attention_id')->comment('Corresponde a la llave foránea de la tabla attentions');
            $table->unsignedBigInteger('evaluated')->comment('Corresponde a la llave foránea de la tabla people');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->unique(['attention_id', 'evaluated']);
            $table->foreign('attention_id')->references('id')->on('attentions')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('evaluated')->references('id')->on('people')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients_polls');
    }
}
